<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Role;
use App\Permission;
use Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::all();
        return view('admin.role.viewrole',compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
     {
        if($request->getMethod() == "GET")
        {
            $permission = Permission::all();
            $data =[
                'id' => null,
                'name' =>null,
            ];
            return view('admin.role.addrole',compact('data','permission'));
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'name' =>'required|unique:roles',
            ]);

            if ($validator->fails()) {
                return redirect('role/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();          
            $role = Role::create($data);

            \LogActivity::addToLog('Role Added'); 
            return redirect('/role/view')->with('success','Role Added');            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
     {
        if($request->getMethod() == 'GET')
        {
            $role = Role::find($id);

            $permission = Permission::all();

            return view('admin.role.updaterole',compact('permission','role'));                 
        }   
        else
        {
           $data = $request->all();
           try 
           {
             DB::beginTransaction();

             $role = Role::find($id);
             $role['name'] = $data['name'];
             $role->save();
             $role->permission()->sync($request->permission);
             \LogActivity::addToLog('Role Updated');
             DB::commit();

             return redirect('/role/view')->with('success','Role Updated Successfully!!');
           } 
            catch (Exception $e) 
            {
             throw $e;
             DB::rollback();
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $role = Role::find($id);
        $role->delete();
        \LogActivity::addToLog('Role Deleted');
        return redirect('/role/view')->with('infor','Role Deleted');
    }
}
