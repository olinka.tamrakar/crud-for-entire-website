<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Hash;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('course');
    }

    public function view()
    {
    	return view('settings.view');
    }
    public function password(Request $request)
    {
    	if($request->getMethod() == 'GET')
    	{  		
            return view('settings.password');
        }
        else
        {
            $id = Auth::user()->id;
            $role = Auth::user()->role;

    		$oldpassword = $request->oldpassword;

    		$confirm = $request->confirmpassword;
    		$confirmpassword = Hash::make($confirm);

    		$password1 = User::select('users.password')
    							->where('id',$id)
    							->get();
    		$password = $password1->all()[0]->password;

    		if (Hash::check($oldpassword, $password)) 
    		{
    			 $user = User::where('id',$id)->update(
                [
                    'password' => $confirmpassword,
                ]);
    			 \LogActivity::addToLog($role.' Password Changed');
    			return redirect('/')->with('success','Password Changed!!');

			}
    		else
    		{
    			return back()->with('error','Wrong Password!!');
    		}
    	}
    }

    public function name(Request $request)
    {
    	if($request->getMethod() == 'GET')
    	{  		
    		return view('settings.name');
    	}
    	else
    	{
            $newname = $request->name;
            dd($request);
    		$id = Auth::user()->id;
            $role = Auth::user()->role;

    		$name = User::select('users.name')
    						->where('users.id',$id)
    						->get();
    		User::where('id',$id)->update([
    			'name' => $newname,
    		]);
    		\LogActivity::addToLog($role.' Name Changed');
    		if($role=='institute')
            {
            return redirect('/')->with('success','Name changed Successfully!!');
            }
    	}
    }

    public function address(Request $request)
    {
    	if($request->getMethod() == 'GET')
    	{  		
    		return view('settings.address');
    	}
    	else
    	{
    		$newaddress = $request->address;
    		$id = Auth::user()->id;
            $role = Auth::user()->role;

    		$address = User::select('users.address')
    						->where('users.id',$id)
    						->get();
    		User::where('id',$id)->update([
    			'address' => $newaddress,
    		]);
    		\LogActivity::addToLog($role.' Address Changed');
    		redirect('/')->with('success','Address changed Successfully!!');
    	}
    }
    public function phone(Request $request)
    {
        if($request->getMethod() == 'GET')
        {       
            return view('settings.phone');
        }
        else
        {
            $newphone = $request->phone;
            $id = Auth::user()->id;
            $role = Auth::user()->role;

            $phone = User::select('users.phone')
                            ->where('users.id',$id)
                            ->get();
            User::where('id',$id)->update([
                'phone' => $newphone,
            ]);
            \LogActivity::addToLog($role.' Admin Phone Changed');
            redirect('/')->with('success','Phone changed Successfully!!');
        }
    }
    public function emails(Request $request)
    {
        if($request->getMethod() == 'GET')
        {       
            return view('settings.email');
        }
        else
        {
            $newemail = $request->email;
            $id = Auth::user()->id;

            $email = User::select('users.email')
                            ->where('users.id',$id)
                            ->get();
            User::where('id',$id)->update([
                'email' => $newemail,
            ]);
            \LogActivity::addToLog($role.' Email Changed');
            redirect('/')->with('success','Email changed Successfully!!');
        }
    }
}
