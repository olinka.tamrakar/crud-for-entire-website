<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Whyus;
use Validator;

class WhyusController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'title' =>null,
                'description' =>null,
                'photo' =>null,
            ];
            return view('admin.whyus.addwhyus',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                   'title' =>'required',
                    'description' =>'required',
                    'photo' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/whyus/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();
                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('whyus', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['photo'] = $file;         
                $whyus = Whyus::create($data);
                \LogActivity::addToLog('Why Us Added');
                return redirect('/whyus/view')->with('success','Why Us Added!!');    
            }

            else
            {
                $validator = Validator::make($request->all(), [
                   'title' =>'required',
                    'description' =>'required',
               ]);
                $data = $request->all();
                whyus::where('id',$data['id'])->update(
                    [
                        'title' => $data['title'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('Why Us Updated');
                return redirect('/whyus/view')->with('success','Why Us Updated!!');
            }      
        }
    }

    public function view()
    {
        $whyus1 = Whyus::all();
        $whyus = $whyus1->all();

        return view('admin.whyus.viewwhyus',compact('whyus'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Whyus::select('*')
            ->where('whyuses.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.whyus.addwhyus',$data);        
        }

    }
    public function delete($id)
    {
        $whyus = Whyus::find($id);
        $whyus->delete();
        \LogActivity::addToLog('Why Us Deleted');
        return redirect('/whyus/view')->with('warning','Why Us Deleted');
    }
}
