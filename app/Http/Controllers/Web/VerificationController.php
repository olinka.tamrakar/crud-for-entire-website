<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Auth;

class VerificationController extends Controller
{
    public function verify($token)
    {
    	$verifyUser = User::where('verification', $token)->first();
    	if(isset($verifyUser) )
    	{
    		$user = $verifyUser->user;
    		if(!$user->verified) 
    		{
    			$verifyUser->user->verified = 1;
    			$verifyUser->user->save();
    			Auth::logout();
    			return redirect('/login')->with('message', 'Your e-mail is verified. You can now login.');
    		}
    		else
    		{

    			Auth::logout();
    			return redirect('/login')->with('message', 'Your e-mail is already verified. You can now login.');
    		}
    	}
    	else
    	{
    		return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
    	} 	 
    }
}
