<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Category;
use App\Gallery;
use App\Gallerycategory;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function add(Request $request)
    {
    	$category = Gallerycategory::all();
        if($request->getMethod() == "GET")
        {
            $data =[
            	'id' => null,
                'photos' =>null,
                'category' =>$category,
            ];
            return view('admin.gallery.addgallery',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'photos' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/gallery/form')
                ->withErrors($validator)
                ->withInput();
            }
            $data = $request->all();
            if(!$request->id)
            {    
                try 
                {
                    DB::beginTransaction();   
                    if($request->file('photos'))
                    {
                        foreach ($request->file('photos') as $file)
                        {
                            $filename = $file;
                            $filename->move('gallerygallery', $filename->getClientOriginalName());
                            $file1 = $filename->getClientOriginalName();        
                            $data['photos'] = $file1;           
                            $gallery = Gallery::create($data);
                        }
                    }
                    \LogActivity::addToLog('Gallery Added'); 
                    DB::commit();
            return redirect('/gallery/view');            
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
            }
        }
    }

    public function view()
    {
        $gallery1 = Gallery::select('*')
                    ->join('gallerycategories','gallerycategories.id','=','galleries.gallerycategory_id')
                    ->get();
        $gallery = $gallery1->all();

        return view('admin.gallery.viewgallery',compact('gallery'));
    }

    public function update(Request $request,$id)
    {  
    if($request->getMethod() == 'GET')
    {
        $datas1 = Gallery::select('*')
        ->where('id',$id)
        ->get();


        $category = Gallerycategory::all();

        $datas =$datas1[0];
        return view('admin.gallery.updategallery',compact('datas','category'));      
    } 
    else
    {
        try 
        {
            DB::beginTransaction();
            $data = $request->all();
            Gallery::where('id',$data['id'])->update(
                [
                    'name' => $data['name'],
                ]
            );
            \LogActivity::addToLog('Gallery Updated'); 
            DB::commit();
            return redirect('/gallery/view');
        } 
        catch (Exception $e) 
        {
            throw $e;
            DB::rollback();   
        }
    }  
    }
    public function delete($id)
    {
        $gallery = Gallery::find($id);
        $photo = public_path().'/gallerygallery'.$gallery->photos;
        File::delete($filename);
        $gallery->delete();
        \LogActivity::addToLog('Gallery Deleted'); 
        return redirect('/gallery/view');
    }
}
