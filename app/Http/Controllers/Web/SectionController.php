<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Section;
use Validator;
use App\Syllabus;

class SectionController extends Controller
{
	public function add(Request $request,$id)
	{
		if($request->getMethod() == "GET")
		{
			$data =[
				'id' =>null,
				'sectionname' =>null,
				'description' =>null,
				'syllabus_id' =>$id,
			];
			return view('admin.section.addsection',$data);
		}
		else
		{
			$validator = Validator::make($request->all(), [
				'sectionname' =>'required',
				'description' =>'required',
			]);

			if ($validator->fails()) {
				return redirect('/section/form')
				->withErrors($validator)
				->withInput();
			}
			$data = $request->all();
			$data['syllabus_id'] = $id;
			try 
			{
				DB::beginTransaction();
				$section = Section::create($data);   
				\LogActivity::addToLog('Section Added');               
				DB::commit();
				return redirect('/course/view')->with('success','Section Added!!');
			} 
			catch (Exception $e) 
			{
				throw $e;
				DB::rollback();
			}
		}
	}

	public function update(Request $request,$id)
	{  
		if($request->getMethod() == 'GET')
		{
			$datas1 = Section::where('id',$id)
						->get();
			$datas =$datas1[0];
			$data =[
				'id'                =>  $id,
				'sectionname'       =>  $datas->sectionname,
				'description'       =>  $datas->description,
				'syllabus_id'       =>  $datas->syllabus_id,
			];
			return view('admin.section.addsection',$data);        
		}
		else
		{
			$validator = Validator::make($request->all(), [
				'sectionname' =>'required|max:50',
				'description' =>'required',
			]);

			if ($validator->fails()) {
				return redirect('/section/form')
				->withErrors($validator)
				->withInput();
			}
			try 
			{
				$data = $request->all();

				DB::beginTransaction();
				Section::where('id', $data['id'])->update(
					[
						'sectionname' => $data['sectionname'],
						'description' => $data['description'],
					]
				);

				\LogActivity::addToLog('Section Updated');               
				DB::commit();
				return redirect('/section/view')->with('success','Section Updated!!');
			} 
			catch (Exception $e) 
			{
				throw $e;
				DB::rollback();
			}
		}
	}
	public function view($id)
	{
		$section1 = Section::select('sections.*')
						->join('syllabi','sections.syllabus_id','=','syllabi.id')
						->where('syllabi.id',$id)
						->get();
		$section = $section1[0];
		return view('admin.section.viewsection',compact('section'));
	}

	public function delete($id)
	{
		$syllabus = Syllabus::find($id);
		$syllabus->delete();
		LogActivity::addToLog('Syllabus Deleted');               
		return redirect('/syllabus/view')->with('info','Syllabus Deleted!!');
	}
}
