<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use App\Blog;
use App\Blogcategory;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('blog');
    }

    public function add(Request $request)
    {
        $blogcategory = Blogcategory::all();
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'title' =>null,
                'description' =>null,
                'photos' =>null,
                'blogcategory'=> $blogcategory,
            ];
            return view('admin.blog.addblog',$data);
        }
        else 
        {
            $validator = Validator::make($request->all(), [
                'title' =>'required',
                'blogcategory_id' =>'required',
                'description' =>'required|max:500',
            ]);

            if ($validator->fails()) {
                return redirect('/blog/form')
                ->withErrors($validator)
                ->withInput()
                ->with('warning','Validation failed');
            }
            $data = $request->all();
            
            if($request->file('photos'))
            {
                $filename = $request->file('photos');
                $filename->move('bloggallery', $filename->getClientOriginalName());
                $file = $filename->getClientOriginalName();
            }
            $data['photos'] = $file;              
            $blog = Blog::create($data);
            \LogActivity::addToLog('Blog Added');
            return redirect('/blog/view')->with('success','Blog Added!!');        
        }
    }

    public function view()
    {
        $blog1 = Blog::all();
        $blog = $blog1->all();

        return view('admin.blog.viewblog',compact('blog'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Blog::select('*')
            ->where('blogs.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];
            // $data =[
            //     'id'               =>  $datas->id,
            //     'title'            =>  $datas->title,
            //     'category'         =>  $datas->category,
            //     'photos'           =>  $datas->photos,
            //     'description'      =>  $datas->description,
            // ];

            $category = Gallerycategory::all();

            return view('admin.blog.updateblog',compact('data','category'));        
        }
        else
        {
            $data = $request->all();
            Blog::where('id',$data['id'])->update(
                [
                    'title' => $data['title'],
                    'category' => $data['category'],
                    'description' => $data['description'],
                ]
            );
            \LogActivity::addToLog('Blog Updated');
            return redirect('/blog/view')->with('success','Blog Updated');
        }  

    }
    public function delete($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        \LogActivity::addToLog('Blog Deleted');
        return redirect('/blog/view')->with('warning','Blog Deleted');
    }
}
