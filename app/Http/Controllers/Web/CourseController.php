<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Validator;

use App\Course;
use App\Syllabus;
use App\Coursecategory;
use Auth;


class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('course');
    }
    
    public function assigncourse(Request $req)
    {
    	$data = $req->all();
    	$course = $data['course'];
        \LogActivity::addToLog('Course Assigned');
    	return redirect('/course/view');
    }

    public function add(Request $request)
    {
        $coursecategory = Coursecategory::all();
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'name' =>null,
                'price' =>null,
                'features' =>null,
                'photos' =>null,
                'coursecategory' => $coursecategory,
                'description' =>null,

            ];
            return view('admin.course.addcourse',$data);
        }
        else
        {
                $validator = Validator::make($request->all(), [
                    'name' =>'required|max:50',
                    'price' =>'required|numeric',
                    'features' =>'required',
                    'photos' => 'required',
                    'description' =>'required',
                ]);

                if ($validator->fails()) {
                    return redirect('/course/form')
                    ->withErrors($validator)
                    ->withInput();
                }
                $data = $request->all();
                try 
                {
                    DB::beginTransaction();
                    $features = implode(',', $request->features);
                    $data['features'] = $features;

                    if($request->file('photos'))
                    {
                        $filename = $request->file('photos');
                        $filename->move('coursegallery', $filename->getClientOriginalName());
                        $file = $filename->getClientOriginalName();
                    }

                    $coursecategory_id = $request->coursecategory_id;
                    $data['photos'] = $file;
                    $data['coursecategory_id'] = $coursecategory_id;

                    $data['user_id'] = Auth::user()->id;

                    $courses = Course::create($data);   
                    \LogActivity::addToLog('Course Added');               
                    DB::commit();
                    return redirect('/course/view')->with('success','Course Added!!');
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
        }
    }

    public function update(Request $request,$id)
    {  
        if($request->getMethod() == 'GET')
        {
            $datas1 = Course::select('courses.*','coursecategories.*')
            ->join('coursecategories','coursecategories.id','=','courses.coursecategory_id')
            ->where('courses.id',$id)
            ->get();
            $datas =$datas1[0];
            $features = explode(',', $datas->features);

            $courses = Coursecategory::all();
            $data =[
                'id'                =>  $id,
                'name'              =>  $datas->name,
                'price'             =>  $datas->price,
                'features'          =>  $features,
                'coursecategory'    =>  $courses,
                'description'       =>  $datas->description,
                'photos'            =>  $datas->photos,
                'user_id'           =>  $datas->user_id,
            ];
            return view('admin.course.updatecourse',$data);        
        }
        else
            {
                $validator = Validator::make($request->all(), [
                    'name' =>'required|max:50',
                    'price' =>'required|numeric',
                    'features' =>'required',
                    'description' => 'required',
                ]);

                if ($validator->fails()) {
                    return redirect('/course/form')
                    ->withErrors($validator)
                    ->withInput();
                }
                try 
                {
                    $data = $request->all();

                    $features = implode(',', $request->features);
                    $data['features'] = $features;

                    DB::beginTransaction();
                    Course::where('id', $data['id'])->update(
                     [
                         'name' => $data['name'],
                         'price' => $data['price'],
                         'features' => $data['features'],
                         'description' => $data['description'],
                     ]
                    );

                    \LogActivity::addToLog('Course Updated');               
                    DB::commit();
                    return redirect('/course/view')->with('success','Course Updated!!');
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
            }
    }
    public function view()
    {
        if(Auth::user()->role[0]->name == 'Admin')
        {
            $course1 = Course::all();   
            $course = $course1->all();
        }
        else
        {
            $user_id = Auth::User()->id;
            $course1 = Course::where('user_id',$user_id)
            ->get();
            $course = $course1->all();
        }  
        return view('admin.course.viewcourse',compact('course'));
    }

    public function delete($id)
    {
        $course = Course::find($id);
        $course->delete();
        \LogActivity::addToLog('Course Deleted');               
        return redirect('/course/view')->with('info','Course Deleted!!');
    }
}
