<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Notifications\Verifyuser;

use Validator;
use Auth;

use App\Institute;
use App\User;
use App\RoleUser;
use App\Role;
use App\Course;
use App\CourseInstitute;


class InstituteController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('institute');
    // }
    public function assigncourse(Request $request)
    {
        if($request->getMethod() == 'GET')
        {
            $course = Course::all();
            $courses = $course->all(); 
            return view ('institute.assigncourse',compact('courses'));           
        }
        else
        {
            $id = Auth::user()->id;
            $institute1 = Institute::where('user_id',$id)
            ->get();
            $institute2 = $institute1->all();
            $institute = $institute2[0];

            $data = $request->all();
            
            foreach ($data['courses'] as $course) {
                $institute->courses()->attach($course);
            }
            \LogActivity::addToLog('Course Assigned');
            return view('institute.institutedashboard');
        }
    }

    public function assignteacher(Request $request)
    {
        if($request->getMethod() == 'GET')
        {
            $teacher = User::select('*')
            ->join('teachers','teachers.user_id','=','users.id')
            ->get();
            $teachers = $teacher->all();

            return view ('institute.assignteacher',compact('teachers'));
        }
        else
        {
            $id = Auth::user()->id;
            $institute1 = Institute::where('user_id',$id)
            ->get();
            $institute2 = $institute1->all();
            $institute = $institute2[0];

            $data = $request->all();
            
            foreach ($data['teachers'] as $teacher) {
                $institute->teachers()->attach($teacher);
            }
            \LogActivity::addToLog('Teacher Assigned');
            return view('institute.institutedashboard');
        }
    }

    public function add(Request $request)
    {
        $course = Course::all();
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'name' =>null,
                'password' =>null,
                'address' =>null,
                'phone' =>null,
                'email' =>null,
                'logo' =>null,
                'establisheddate' =>null,
                'website' =>null,
                'user_id'=>null,
                'courses'=>$course,

            ];
            return view('admin.institute.addinstitute',$data);
        }
        else
        {
            $validator = Validator::make($request->all(), [
                'name' =>'required',
                'address' =>'required',
                'phone' =>'required',
                'email' =>'required|email',
                'logo' =>'required|image',
                'website' =>'required',
                'establisheddate' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/institute/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();
            // dd($data);
            
            try 
            {
                DB::beginTransaction();


                $password = Hash::make($request->password);
                $data['password'] = $password;

                $data['verification'] = Str::random(15);

                $user = User::create($data);

                $user->notify(new Verifyuser($user));

                if($request->file('logo'))
                {
                    $filename = $request->file('logo');
                    $filename->move('institutes', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['logo'] = $file;
                $user_id = $user->id;
                $data['user_id'] = $user_id;

                $institutes = Institute::create($data);
                $data['institutes_id'] = $institutes['id'];

                foreach ($data['courses'] as $courses) 
                {
                    $institutes->courses()->attach($courses);
                }

                $rolename = $data['role'];
                $roleid = Role::where('roles.name','=',$rolename)
                ->get();
                $roleid1 = $roleid[0]->id;

                $data['role_id'] = $roleid1;

                $role = RoleUser::create($data);
                \LogActivity::addToLog('Institute Added');
                DB::commit();
                return redirect('/institute/view');
            } 
            catch (Exception $e) 
            {
                throw $e;
                DB::rollback();
            }
            
        }
    }

    public function update(Request $request,$id)
    {     
        if($request->getMethod()=='GET')
        {
            $datas1 = Institute::select('*')
            ->join('users','users.id','=','institutes.user_id')
            ->join('course_institutes','course_institutes.institute_id','=','institutes.id')
            ->where('institutes.id',$id)
            ->get();
            $datas = $datas1[0];

            $course1 = CourseInstitute::select('courses.id','courses.name')
            ->join('courses','courses.id','=','course_institutes.course_id')
            ->get();
            $course = $course1->all();

            $allcourse1 = Course::all(); 
            $allcourse = $allcourse1->all();        

            return view('admin.institute.updateinstitute',compact('datas','course','allcourse'));          
        }  
        else
        {
            try 
            {
                DB::beginTransaction();

                User::where('id',$data['user_id'])->update(
                    [
                        'name' => $data['name'],
                        'address' => $data['address'],
                        'password' => $data['password'],
                        'phone' => $data['phone'],
                        'email' => $data['email'],
                    ]
                );

                Institute::where('id', $data['id'])->update(
                   [
                       'website' => $data['website'],
                       'establisheddate' => $data['establisheddate'],
                   ]
               );
                \LogActivity::addToLog('Institute Updated');
                DB::commit();
                return redirect('/institute/view');
            } 
            catch (Exception $e) 
            {
                throw $e;
                DB::rollback();
            }
        }

    }
    public function view()
    {
        $user1 = User::all();   
        $user = $user1->all();

        $institute1 = Institute::all();
        $institute = $institute1->all();

        return view('admin.institute.viewinstitute',compact('institute','user'));
    }

    public function viewcourse()
    {
        $id = Auth::user()->id;
        $course = Institute::select('courses.*')
        ->join('course_institutes','course_institutes.institute_id','=','institutes.id')
        ->join('courses','courses.id','course_institutes.course_id')
        ->where('user_id',$id)
        ->get();
        return view('institute.viewcourse',compact('course'));
    }

    public function viewteacher()
    {
        $id = Auth::user()->id;
        $teacher = User::select('users.*')
        ->join('teachers','users.id','=','teachers.user_id')
        ->join('institute_teachers','institute_teachers.teacher_id','=','teachers.id')
        ->get();
        return view('institute.viewteacher',compact('teacher'));
    }

    public function delete($id)
    {
        $institute = Institute::find($id);
        $institute->delete();
        \LogActivity::addToLog('Institute Deleted');
        return redirect('/institute/view');
    }
}
