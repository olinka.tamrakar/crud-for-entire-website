<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Teacher;
use PDF;

class PdfController extends Controller
{
    public function generate()
    {
    	$teacher = $this->get_data();
        // dd($teacher);
    	$pdf = PDF::loadView('pdf', compact('teacher'));
		return $pdf->download('Teacherslist.pdf');
    }
    public function get_data()
    {
        $teacher = User::select('users.*')
                    ->join('teachers','users.id','=','teachers.user_id')
                    ->get();
        // $teacher = $teacher1->all();
                    return $teacher;

        // return view('pdf',compact('teacher'));
    }
}
