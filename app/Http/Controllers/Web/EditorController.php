<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Notifications\Verifyuser;

use App\Editor;
use App\User;
use App\Role;
use App\RoleUser;
use Validator;

class EditorController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'name' =>null,
                'address' =>null,
                'phone' =>null,
                'email' =>null,
                'password' =>null,
                'status' =>null,
                'file' =>null,
                'user_id' =>null,
            ];
            return view('admin.editor.addeditor',$data);
        }
        else 
        {
            
           $validator = Validator::make($request->all(), [
              'name' =>'required',
              'email' =>'required',
              'address' =>'required',
              'phone' =>'required',
              'email' =>'required',
              'password' =>'required',
              'file' =>'required|image',
          ]);

           if ($validator->fails()) {
              return redirect('/editor/form')
              ->withErrors($validator)
              ->withInput();
          }
          $data = $request->all();
          if($request->file('file'))
          {
              $filename = $request->file('file');
              $filename->move('image\editors', $filename->getClientOriginalName());
              $file = $filename->getClientOriginalName();
          }
          try 
          {
              DB::beginTransaction();
              $password = Hash::make($request->password);
              $data['password'] = $password;

              $data['verification'] = Str::random(15);

              $user = User::create($data);

              $user->notify(new Verifyuser($user));
              $user_id =$user->id;

              $rolename = $data['role'];
              $roleid = Role::where('roles.name','=',$rolename)
              ->get();
              $roleid1 = $roleid[0]->id;

              $data['user_id'] = $user_id;
              $data['role_id'] = $roleid1;
              $role = RoleUser::create($data);

              $data['file'] = $file;                                 
              $editor = Editor::create($data);
              \LogActivity::addToLog('Editor Added');               
              DB::commit();
              return redirect('/editor/view')->with('success','Editor Added!!');
          } 
          catch (Exception $e) 
          {
              throw $e;
              DB::rollback();
          }
        }
    }

    public function view()
    {
    	$user1 = User::all();
        $user = $user1->all();

        $editor1 = Editor::all();
        $editor = $editor1->all();

        return view('admin.editor.vieweditor',compact('editor','user'));
    }

    public function update(Request $request,$id)
    {   
        if($request->getMethod() == 'GET')
        {
            $datas1 = User::select('*')
            ->join('editors','editors.user_id','=','users.id')
            ->where('editors.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $datas =$datas2[0];
            $data =[
                'id'               =>  $datas->id,
                'name'            =>  $datas->name,
                'phone'			=> $datas->phone,
                'email'         =>  $datas->email,
                'address'		=> $datas->address,
                'password'		=> $datas->password,
                'file'           =>  $datas->file,
                'status'      =>  $datas->status,
                'user_id' 		=> $datas->user_id,
            ];
            return view('admin.editor.updateeditor',$data);    
        }
        else
        {   
            $data = $request->all();

            User::where('id',$data['user_id'])->update(
                [
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'address' => $data['address'],
                    'phone' => $data['phone']
                ]
            );
            \LogActivity::addToLog('Editor Updated');  
            return redirect('/editor/view')->with('success','Editor Updated!!');  
        }  
    }
    public function delete($id)
    {
        $editor = Editor::find($id);
        $editor->delete();
        \LogActivity::addToLog('Editor Deleted');  
        return redirect('/editor/view')->with('error','Editor Deleted!!');
    }
}
