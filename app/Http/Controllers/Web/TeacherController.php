<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Str;

use App\Role;
use App\User;
use App\Status;
use App\RoleUser;
use App\Teacher;
use App\Course;
use App\CourseTeacher;
use App\Notifications\Verifyuser;



class TeacherController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

     public function add(Request $request)
    {
        $course = Course::all();

        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'name' =>null,
                'password' =>null,
                'address' =>null,
                'phone' =>null,
                'email' =>null,
                'specialization' =>null,
                'experiences' =>null,
                'subjects' =>null,
                'followers' =>null,
                'classes' =>null,
                'file' =>null,
                'status' =>null,
                'user_id'=>null,
                'courses' =>$course,
                'description' =>null,
            ];
            return view('admin.teacher.addteacher',$data);
        }
        else 
        {
            $data = $request->all();
            if(!$request->id)
            {
               $validator = Validator::make($request->all(), [
                'name' =>'required',
                'password' =>'required',
                'address' =>'required',
                'phone' =>'required',
                'email' =>'required|email|unique:users',
                'specialization' =>'required',
                'experiences' =>'required',
                'subjects' =>'required',
                'followers' =>'required',
                'classes' =>'required',
                'file' =>'required|image',
                'description' =>'required',
            ]);

            if ($validator->fails()) 
            {
                return redirect('teacher/form')
                ->withErrors($validator)
                ->withInput();
            }
                try 
                {
                    DB::beginTransaction();

                    $password = Hash::make($request->password);
                    $data['password'] = $password;

                    $data['verification'] = Str::random(15);

                    $user = User::create($data);

                    $user->notify(new Verifyuser($user));

                    $user_id = $user->id;
                    $rolename = $data['role'];
                    $roleid = Role::where('roles.name','=',$rolename)
                    ->get();
                    $roleid1 = $roleid[0]->id;

                    $data['user_id'] = $user_id;
                    $data['role_id'] = $roleid1;

                    if($request->file('file'))
                    {
                        $filename = $request->file('file');
                        $filename->move('teachergallery', $filename->getClientOriginalName());
                        $file = $filename->getClientOriginalName();
                    }
                    $data['file'] = $file;

                    $status = $request->status;

                    $data['status'] = $status;
                    $teacher = Teacher::create($data);
                    $role = RoleUser::create($data);
                    
                    foreach ($data['courses'] as $courses) {
                        $teacher->courses()->attach($courses);
                    }
                    \LogActivity::addToLog('Teacher Added');
                    DB::commit();
                    return redirect('/teacher/view')->with('success','Teacher Added Successfully!!');
                } 
                catch (Exception $e) 
                {
                 throw $e;
                 DB::rollback();
             }
            }
        }
           
    }

    public function update(Request $req,$id)
    {
        if($req->getMethod() == 'GET')
        {
            try 
            {
                DB::beginTransaction();   
                $data = Teacher::select('*')
                            ->join('users','users.id','=','teachers.user_id')
                            ->where('teachers.id',$id)
                            ->get();
                $datas = $data[0];

                $selectedcourses1 = CourseTeacher::select('*')
                                    ->join('teachers','course_teachers.teacher_id','=','teachers.id')
                                    ->get();
                $selectedcourses = $selectedcourses1->all();
                    
                $course1 = Course::all();
                $course = $course1->all();

                DB::commit();
                return view('admin.teacher.updateteacher',compact('datas','course','selectedcourses'));
            } 
            catch (Exception $e) 
            {
                throw $e;
                DB::rollback();
            }          
        }   
        else
        {
           $data = $req->all();
           try 
           {
             DB::beginTransaction();

             $user = User::where('id',$data['user_id'])->update(
                [
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'phone' => $data['phone'],
                    'email' => $data['email'],
                ]
            );

            $teacher1 = Teacher::where('user_id',$data['user_id'])
                            ->get();
            $teacher = $teacher1[0];

            $teacher->specialization = $req->specialization;
            $teacher->experiences    = $req->experiences;  
            $teacher->subjects       = $req->subjects;
            $teacher->followers      = $req->followers;
            $teacher->classes        = $req->classes;  
            $teacher->description    = $req->description;
            $teacher->save();

            $teacher->courses()->sync($req->courses);

             \LogActivity::addToLog('Teacher Updated');
             DB::commit();

             return redirect('/teacher/view')->with('success','Teacher Updated Successfully!!');
           } 
            catch (Exception $e) 
            {
             throw $e;
             DB::rollback();
            }
        }

    }

     public function show()
    {
        $user1 = User::all();
        $user = $user1->all();

        $teacher1 = Teacher::all();   
        $teacher = $teacher1->all();

        $description = explode(' ', $teacher[0]->description);
        for ($i=0; $i < 3 ; $i++) { 
            print_r($description[$i].'&nbsp');
         } 

        $course1 = Course::all();
        $course = $course1->all();

        return view('admin.teacher.viewteacher',compact('user','teacher','course'));
    }

    public function delete($id)
    {
        $teacher1 = Teacher::find($id);

        if($teacher1->status == 'Active')
        {
            $teacher1->status = 'Deactive';
        }
        else if($teacher1->status == 'Deactive')
        {
            $teacher1->status = 'Active';
        }

        $teacher1->save();
        \LogActivity::addToLog('Teacher Deleted');

      return redirect('/teacher/view')->with('success','Teacher Deleted Successfully!!');
    }
}
