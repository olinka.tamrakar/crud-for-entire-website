<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Contact;

use Validator;

class ContactController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'location' =>null,
                'number' =>null,
                'email' =>null,
                'videocall'=> null,
            ];
            return view('admin.contact.addcontact',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                   'location' =>'required',
                   'number' =>'required',
                   'email' =>'required',
                   'videocall'=> 'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/contact/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();         
                $contact = Contact::create($data);
                \LogActivity::addToLog('Contact Added');
                return redirect('/contact/view')->with('success','Contact Added!!');    
            }

            else
            {
                $data = $request->all();
                Contact::where('id',$data['id'])->update(
                    [
                        'location' => $data['location'],
                        'number' => $data['number'],
                        'email' => $data['email'],
                        'videocall' => $data['videocall'],
                    ]
                );
                \LogActivity::addToLog('Contact Updated');
                return redirect('/contact/view')->with('success','Contact Updated');
            }      
        }
    }

    public function view()
    {
        $contact1 = Contact::all();
        $contact = $contact1->all();

        return view('admin.contact.viewcontact',compact('contact'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Contact::select('*')
            ->where('contacts.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.contact.addcontact',$data);        
        }

    }
    public function delete($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        \LogActivity::addToLog('Contact Deleted');
        return redirect('/contact/view')->with('warning','Contact Deleted');
    }
}
