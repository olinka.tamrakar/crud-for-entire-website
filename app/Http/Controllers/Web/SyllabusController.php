<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Syllabus;
use Validator;

class SyllabusController extends Controller
{
	public function add(Request $request,$id)
	{
		if($request->getMethod() == "GET")
		{
			$data =[
				'id' =>null,
				'syllabusname' =>null,
				'classtime' =>null,
				'estimatetime' =>null,
				'courseduration' =>null,
				'photos' =>null,
				'course_id' =>$id,

			];
			return view('admin.syllabus.addsyllabus',$data);
		}
		else
		{
			$validator = Validator::make($request->all(), [
				'syllabusname' =>'required|max:50',
				'classtime' =>'required',
				'estimatetime' =>'required',
				'courseduration' =>'required',
			]);

			if ($validator->fails()) {
				return redirect('/syllabus/form')
				->withErrors($validator)
				->withInput();
			}
			$data = $request->all();
			$data['course_id'] = $id;
			try 
			{
				DB::beginTransaction();
				$syllabus = Syllabus::create($data);   
				\LogActivity::addToLog('Syllabus Added');               
				DB::commit();
				return redirect('/course/view')->with('success','Syllabus Added!!');
			} 
			catch (Exception $e) 
			{
				throw $e;
				DB::rollback();
			}
		}
	}

	public function update(Request $request,$id)
	{  
		if($request->getMethod() == 'GET')
		{
			$datas1 = Syllabus::select('*')
			->join('courses','courses.id','=','syllabi.course_id')
			->where('syllabi.id',$id)
			->get();
			$datas =$datas1[0];
			$data =[
				'id'                =>  $id,
				'syllabusname'      =>  $datas->syllabusname,
				'classtime'         =>  $datas->classtime,
				'estimatetime'      =>  $datas->estimatetime,
				'courseduration'    =>  $datas->courseduration,
				'course_id'         =>  $datas->course_id,
			];
			return view('admin.syllabus.addsyllabus',$data);        
		}
		else
		{
			$validator = Validator::make($request->all(), [
				'syllabusname' =>'required|max:50',
				'classtime' =>'required',
				'estimatetime' =>'required',
				'courseduration' => 'required',
			]);

			if ($validator->fails()) {
				return redirect('/syllabus/form')
				->withErrors($validator)
				->withInput();
			}
			try 
			{
				$data = $request->all();

				DB::beginTransaction();
				Syllabus::where('id', $data['id'])->update(
					[
						'name' => $data['name'],
						'classtime' => $data['classtime'],
						'estimatetime' => $data['estimatetime'],
						'courseduration' => $data['courseduration'],
					]
				);

				\LogActivity::addToLog('Syllabus Updated');               
				DB::commit();
				return redirect('/syllabus/view')->with('success','Syllabus Updated!!');
			} 
			catch (Exception $e) 
			{
				throw $e;
				DB::rollback();
			}
		}
	}
	public function view($id)
	{
		$syllabus1 = Syllabus::select('syllabi.*')
		->join('courses','syllabi.course_id','=','courses.id')
		->where('courses.id',$id)
		->get();
		$syllabus = $syllabus1[0];
		return view('admin.syllabus.viewsyllabus',compact('syllabus'));
	}

	public function delete($id)
	{
		dd($id);
		$syllabus = Syllabus::find($id);
		$syllabus->delete();
		LogActivity::addToLog('Syllabus Deleted');               
		return redirect('/syllabus/view')->with('info','Syllabus Deleted!!');
	}
}
