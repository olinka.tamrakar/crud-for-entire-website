<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Workflow;
use Validator;

class WorkflowController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'title' =>null,
                'description' =>null,
                'photo' =>null,
            ];
            return view('admin.workflow.addworkflow',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                    'title' =>'required',
                    'description' =>'required',
                    'photo' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/workflow/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all(); 
                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('workflowgallery', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['photo'] = $file;         
                $workflow = Workflow::create($data);
                \LogActivity::addToLog('Workflow Added');
                return redirect('/workflow/view')->with('success','Workflow Added!!');    
            }

            else
            {
                $data = $request->all();
                workflow::where('id',$data['id'])->update(
                    [
                        'title' => $data['title'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('Workflow Updated');
                return redirect('/workflow/view')->with('success','Workflow Updated!!');
            }      
        }
    }

    public function view()
    {
        $workflow1 = Workflow::all();
        $workflow = $workflow1->all();

        return view('admin.workflow.viewworkflow',compact('workflow'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Workflow::select('*')
            ->where('workflows.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.workflow.addworkflow',$data);        
        }

    }
    public function delete($id)
    {
        $workflow = Workflow::find($id);
        $workflow->delete();
        \LogActivity::addToLog('Work Flow Deleted');
        return redirect('/workflow/view')->with('warning','Work Flow Deleted');
    }
}
