<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Blogcategory;
use Validator;

class BlogcategoryController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
            	'id' => null,
                'name' =>null,
            ];
            return view('admin.blogcategory.addblogcategory',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'name' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/blogcategory/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();

            if(!$request->id)
            {         
                $data['parent_id'] = empty($data['parent_id']) ? 0 : $data['parent_id'];    
                $data['status'] ='Active';
                $blogcategory = Blogcategory::create($data);
                \LogActivity::addToLog('Category Added');
                return redirect('/blogcategory/view')->with('success','Blog Category Added!!');
            }
            else
            {
                $data = $request->all();
                Blogcategory::where('id',$data['id'])->update(
                    [
                        'name' => $data['name'],
                    ]
                );
                \LogActivity::addToLog('Blog Category Updated');
                return redirect('/blogcategory/view')->with('success','Blog Category Updated!!');
            }  
        }
    }

    public function view()
    {
        $blogcategory1 = Blogcategory::all();
        $blogcategory = $blogcategory1->all();

        return view('admin.blogcategory.viewblogcategory',compact('blogcategory'));
    }

    public function update($id)
    {   
        $datas1 = Blogcategory::select('*')
        ->where('blogcategories.id',$id)
        ->get();

        $datas2 = $datas1->all();
        $datas =$datas2[0];
        $data =[
            'id'               =>  $datas->id,
            'name'            =>  $datas->name,
        ];
        return view('admin.blogcategory.addblogcategory',$data);
    }
    public function delete($id)
    {
        $blogcategory = Blogcategory::find($id);
        if($blogcategory->status == 'Active')
        {
            $blogcategory->status = 'Deactive';
            \LogActivity::addToLog('Category Deactivated');
        }
        else if($blogcategory->status == 'Deactive')
        {
            $blogcategory->status = 'Active';
            \LogActivity::addToLog('Category Activated');
        }

        $blogcategory->save();
        return redirect('/blogcategory/view')->with('info','Blog Category Status Changed!!');
    }

}