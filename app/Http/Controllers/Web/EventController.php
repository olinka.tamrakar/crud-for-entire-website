<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Event;
use Validator;

class EventController extends Controller
{
    public function __construct()
    {
        // $this->middleware('admin');
    }
    
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {   
            $data=[
                'id' =>null,
                'topics' =>null,
                'host' =>null,
                'location' =>null,
                'starttime' =>null,
                'endtime'  =>null,
                'startdate' =>null,
                'enddate' =>null,
                'website' =>null,
                'keynotespeakers' =>null,
                'description' =>null,
                'photos' =>null,
            ];
            return view('admin.event.addevent',$data);
        }
        else
        {
            $validator = Validator::make($request->all(), [
                'topics' =>'required',
                'host' =>'required|alpha',
                'location' =>'required',
                'starttime' =>'required',
                'endtime' =>'required',
                'startdate' =>'required',
                'enddate' =>'required|date|after:startdate',
                'website' =>'required|url',
                'keynotespeakers' =>'required|alpha',
                'description' =>'required|max:500',
                'photos' =>'required|image',
            ]);

            if ($validator->fails()) {
                return redirect('/event/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();
            if(!$request->id)
            {
                try 
                {
                    DB::beginTransaction();
                    if($request->file('photos'))
                    {
                        $filename = $request->file('photos');
                        $filename->move('eventgallery', $filename->getClientOriginalName());
                        $file = $filename->getClientOriginalName();
                    }
                    $data['photos'] = $file;

                    $events = Event::create($data); 
                    \LogActivity::addToLog('Event Added');      
                    DB::commit();
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
            }
            return redirect('/event/view');           
        }

    }
    public function view()
    {
    	$event1 = Event::all();   
        $event = $event1->all();

        return view('admin.event.viewevent',compact('event'));
    }
    public function delete($id)
    {
        $event = Event::find($id);
        $event->delete();
        \LogActivity::addToLog('Event Deleted'); 
        return redirect('/event/view');
    }
    public function update(Request $request, $id)
    {
        if($request->getMethod()=='GET')
        {
            $datas1 = Event::select('events.*')
            ->where('events.id','=',$id)
            ->get();
            $datas2 = $datas1->all();
            $datas =$datas2[0];
            $data =[
                'id'                =>  $datas->id,
                'topics'            =>  $datas->topics,
                'host'              =>  $datas->host,
                'location'          =>  $datas->location,
                'starttime'         =>  $datas->starttime,
                'endtime'         =>  $datas->endtime,
                'startdate'         =>  $datas->startdate,
                'enddate'           =>  $datas->enddate,
                'website'           =>  $datas->website,
                'keynotespeakers'   =>  $datas->keynotespeakers,
                'description'       =>  $datas->description,
            ];
            return view('admin.event.updateevent',$data);            
        }
        else
            {
                try 
                {
                    $data = $request->all();
                    DB::beginTransaction();
                    Event::where('id',$data['id'])->update(
                        [
                            'topics' => $data['topics'],
                            'host' => $data['host'],
                            'location' => $data['location'],
                            'starttime' => $data['starttime'],
                            'endtime' => $data['endtime'],
                            'startdate' => $data['startdate'],
                            'enddate' => $data['enddate'],
                            'website' => $data['website'],
                            'keynotespeakers' => $data['keynotespeakers'],
                            'description' => $data['description'],

                        ]
                    );
                    \LogActivity::addToLog('Event Updated'); 
                    DB::commit();
                    return redirect('/event/view'); 
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
            }
    }
}
