<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Allaboutorg;
use Validator;

class AllaboutorgController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'organization' =>null,
                'description' =>null,
            ];
            return view('admin.allaboutorg.addallaboutorg',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                    'organization' =>'required',
                	'description' =>'required',
                    'photo' => 'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/allaboutorg/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all(); 
                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('allaboutorggallery', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['photo'] = $file;         
                $allaboutorg = Allaboutorg::create($data);
                \LogActivity::addToLog('All About Organization Added');
                return redirect('/allaboutorg/view')->with('success','All About Organization Added!!');    
            }

            else
            {
                $data = $request->all();
                Allaboutorg::where('id',$data['id'])->update(
                    [
                        'organization' => $data['organization'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('All About Organization Updated');
                return redirect('/allaboutorg/view')->with('success','All About Organization Updated');
            }      
        }
    }

    public function view()
    {
        $allaboutorg1 = Allaboutorg::all();
        $allaboutorg = $allaboutorg1->all();

        return view('admin.allaboutorg.viewallaboutorg',compact('allaboutorg'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = allaboutorg::select('*')
            ->where('allaboutorgs.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.allaboutorg.addallaboutorg',$data);        
        }

    }
    public function delete($id)
    {
        $allaboutorg = allaboutorg::find($id);
        $allaboutorg->delete();
        \LogActivity::addToLog('allaboutorgAll About Organization Deleted');
        return redirect('/allaboutorg/view')->with('warning','All About Organization Deleted');
    }
}
