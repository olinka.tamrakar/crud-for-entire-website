<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Ourteam;

class OurteamController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'name' =>null,
                'position' =>null,
                'description' =>null,
            ];
            return view('admin.ourteam.addourteam',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                    'name' =>'required',
                	'position' =>'required',
                	'description' =>'required',
                    'photo' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/ourteam/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();   

                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('ourteamgallery', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['photo'] = $file;      
                $ourteam = Ourteam::create($data);
                \LogActivity::addToLog('Ourteam Added');
                return redirect('/ourteam/view')->with('success','Ourteam Added!!');    
            }

            else
            {
                $validator = Validator::make($request->all(), [
                    'name' =>'required',
                    'position' =>'required',
                    'description' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/ourteam/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();
                Ourteam::where('id',$data['id'])->update(
                    [
                        'name' => $data['name'],
                        'position' => $data['position'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('Ourteam Updated');
                return redirect('/ourteam/view')->with('success','Ourteam Updated!!');
            }      
        }
    }

    public function view()
    {
        $ourteam1 = Ourteam::all();
        $ourteam = $ourteam1->all();

        return view('admin.ourteam.viewourteam',compact('ourteam'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Ourteam::select('*')
            ->where('ourteams.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.ourteam.addourteam',$data);        
        }

    }
    public function delete($id)
    {
        $ourteam = Ourteam::find($id);
        $ourteam->delete();
        \LogActivity::addToLog('Ourteam Deleted');
        return redirect('/ourteam/view')->with('warning','Ourteam Deleted');
    }
}
