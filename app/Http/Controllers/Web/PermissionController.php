<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Permission;
use Validator;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permission = Permission::all();
        return view('admin.Permission.viewPermission',compact('permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
     {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' => null,
                'name' =>null,
            ];
            return view('admin.permission.addpermission',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'name' =>'required|unique:permissions',
                'permissionfor' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('permission/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();          
            $permission = Permission::create($data);
            \LogActivity::addToLog('Permission Added'); 
            return redirect('/permission/view')->with('success','Permission Added!!');            
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
     {
        if($request->getMethod() == 'GET')
        {
            try 
            {
                DB::beginTransaction();   
                $data = Permission::select('*')
                ->where('permissions.id',$id)
                ->get();

                $datas = $data[0];

                DB::commit();
                return view('admin.permission.updatepermission',compact('datas'));
            } 
            catch (Exception $e) 
            {
                throw $e;
                DB::rollback();
            }          
        }   
        else
        {
           $data = $request->all();
           try 
           {
             DB::beginTransaction();

             $user = Permission::where('id',$data['id'])->update(
                [
                    'name' => $data['name'],
                    'permissionfor' => $data['permissionfor'],
                ]
            );

             \LogActivity::addToLog('Permission Updated');
             DB::commit();

             return redirect('/permission/view')->with('success','Permission Updated Successfully!!');
           } 
            catch (Exception $e) 
            {
             throw $e;
             DB::rollback();
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $permission = Permission::find($id);
        $permission->delete();
        \LogActivity::addToLog('Permission Deleted');
        return redirect('/permission/view')->with('infor','Permission Deleted');
    }
}
