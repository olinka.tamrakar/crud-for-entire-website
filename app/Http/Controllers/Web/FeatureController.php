<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Feature;
use Validator;

class FeatureController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'heading' =>null,
                'description' =>null,
                'image' =>null,
            ];
            return view('admin.feature.addfeature',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                   'heading' =>'required',
                   'description' =>'required',
                   'photo' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/feature/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();  
                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('blogs', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data['photo'] = $file;       
                $feature = Feature::create($data);
                \LogActivity::addToLog('feature Added');
                return redirect('/feature/view')->with('success','feature Added!!');    
            }

            else
            {
                $data = $request->all();
                Feature::where('id',$data['id'])->update(
                    [
                        'heading' => $data['heading'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('feature Updated');
                return redirect('/feature/view')->with('success','feature Updated');
            }      
        }
    }

    public function view()
    {
        $feature1 = Feature::all();
        $feature = $feature1->all();

        return view('admin.feature.viewfeature',compact('feature'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Feature::select('*')
            ->where('features.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.feature.addfeature',$data);        
        }

    }
    public function delete($id)
    {
        $feature = Feature::find($id);
        $feature->delete();
        \LogActivity::addToLog('feature Deleted');
        return redirect('/feature/view')->with('warning','feature Deleted');
    }
}
