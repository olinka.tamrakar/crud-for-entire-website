<?php

namespace App\Http\Controllers\Web\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Allaboutorg;
use App\Whyus;
use App\Mission;
use App\Workflow;
use App\Ourteam;
use App\Course;
use App\Coursecategory;
use App\Teacher;
use App\Openinghour;
use App\Syllabus;
use App\Event;
use App\Faq;
use App\Contact;
use App\Gallerycategory;
use App\Gallery;
use App\Blog;
use App\Blogcategory;
use App\Slider;
use App\Pageimage;
use App\Feature;
use App\Whychooseus;

class ViewController extends Controller
{
	public function about()
    {
        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $allaboutorg1 = Allaboutorg::all();
        $allaboutorg = $allaboutorg1[0];

        $description = explode(' ', $allaboutorg1[0]->description);

        $whyus1 = Whyus::all();
        $whyus = $whyus1->all();

        $mission1 = Mission::all();
        $mission = $mission1->all();

        $workflow1 = Workflow::all();
        $workflow = $workflow1->all();

        $pageimage1 = Pageimage::where('pageimagefor','Aboutus')
        ->get();
        $pageimage = $pageimage1[0];
        
        return view('frontend.about',compact('allaboutorg','whyus','mission','workflow','openinghour','contact','pageimage','description'));
    }
    public function blogdetail($id)
    {
        $blog1 = Blog::where('id',$id)
                        ->get();
        $blog = $blog1[0];

        $latestblog1 = blog::orderBy('blogs.created_at', 'DESC')
                                ->limit(3)
                                ->get();
        $latestblog = $latestblog1->all(); 

        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $pageimage1 = Pageimage::where('pageimagefor','Blog')
        ->get();
        $pageimage = $pageimage1[0];

        return view('frontend.blog-detail',compact('openinghour','contact','pageimage','blog','latestblog'));
    }
    public function blogs(Request $request)
    {
        if($request->keyword)
        {
            $keyword = $request->keyword;
            $blog1 = Blog::where('title','like','%'.$keyword.'%')
            ->get();
            $blog = $blog1->all();
        }
        else
        {
            $blog1 = Blog::all();
            $blog = $blog1->all();
        }
        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $latestblog1 = blog::orderBy('blogs.created_at', 'DESC')
                                ->limit(3)
                                ->get();
        $latestblog = $latestblog1->all(); 

        $blogcategory1 = Blogcategory::all();
        $blogcategory = $blogcategory1->all();

        $pageimage1 = Pageimage::where('pageimagefor','Blog')
        ->get();
        $pageimage = $pageimage1[0];

        return view('frontend.blogs',compact('openinghour','contact','blog','blogcategory','pageimage','latestblog'));
    }
    public function contact()
    {
        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $pageimage1 = Pageimage::where('pageimagefor','Contact')
        ->get();
        $pageimage = $pageimage1[0];

        return view('frontend.contact',compact('openinghour','contact','pageimage'));
    }
    public function coursedetail($id)
    {
        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $course1 = Course::where('id',$id)
        ->get();
        $course = $course1[0];

        $courseall1 = Course::all();
        $courseall = $courseall1->all();

        $teacher1 = Teacher::select('teachers.*','users.*')
        ->join('courses','courses.user_id','teachers.user_id')
        ->join('users','users.id','teachers.user_id')
        ->where('courses.id',$course->id)
        ->get();
        $teacher = $teacher1->all();

        $pageimage1 = Pageimage::where('pageimagefor','Coursedetail')
        ->get();
        $pageimage = $pageimage1[0];

        $relatedcourse1 = Course::where('courses.coursecategory_id',$course->coursecategory_id)
        ->get();
        $relatedcourse = $relatedcourse1->all();

        $syllabus1 = Syllabus::where('course_id',$course->id)
        ->get();
        $syllabus = $syllabus1[0];

        return view('frontend.course-detail',compact('openinghour','contact','course','syllabus','courseall','teacher','relatedcourse','pageimage'));
    }
    public function course(Request $request,$id=0)
    {
        if($request->keyword)
        {
            $keyword = $request->keyword;
            $course1 = Course::where('name','LIKE','%'.$keyword.'%')
            ->get();
            $course = $course1->all();
        }
        else if($id)
        {
            $course1 = Course::where('coursecategory_id',$id)
                    ->get();
            $course =$course1->all();
        }
        else
        {
            $course1 = Course::all();
            $course = $course1->all();          
        }

        $latestcourse1 = Course::orderBy('courses.created_at', 'DESC')
                                ->limit(3)
                                ->get();
        $latestcourse = $latestcourse1->all();
        
        $coursecategories1 = Coursecategory::all();
        $coursecategories = $coursecategories1->all();

        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $pageimage1 = Pageimage::where('pageimagefor','Courses')
        ->get();
        $pageimage = $pageimage1[0];

        return view('frontend.course',compact('course','coursecategories','openinghour','contact','courses','pageimage','latestcourse'));
    }

    public function eventdetail($id)
    {
        $openinghour1 = Openinghour::all();
        $openinghour = $openinghour1->all();

        $contact1 = Contact::all();
        $contact = $contact1[0];

        $event1 = Event::where('id',$id)
        ->get();
        $event = $event1[0];

        $pageimage1 = Pageimage::where('pageimagefor','Courses')
        ->get();
        $pageimage = $pageimage1[0];

        return view('frontend.event-detail',compact('openinghour','contact','event','pageimage'));
    }
    public function events(Request $request)
    {
        if($request->keyword)
        {
           $keyword = $request->keyword;

           $event1 = Event::where('topics','like','%'.$keyword.'%')
           ->get();
           $event = $event1->all();
       }
       else
       {
        $event1 = Event::all();
        $event = $event1->all();
    }
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.events',compact('openinghour','contact','event','pageimage'));
}
public function faq()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $faq1 = Faq::all();
    $faq = $faq1->all();

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.faq',compact('openinghour','contact','faq','pageimage'));
}
public function gallerydetail($id)
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $gallerydetail1 = Gallery::select('galleries.photos','gallerycategories.name')
    ->join('gallerycategories','galleries.gallerycategory_id','gallerycategories.id')
    ->where('gallerycategory_id',$id)
    ->get();
    $gallerydetail = $gallerydetail1->all();

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.gallery-detail',compact('openinghour','contact','gallerydetail','pageimage'));
}
public function gallery()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $gallerycategory1 = Gallerycategory::all();
    $gallerycategory = $gallerycategory1->all();

    $gallery1 = Gallery::select('*')
    ->join('gallerycategories','galleries.gallerycategory_id','gallerycategories.id')
    ->get();
    $gallery = $gallery1->all();

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.gallery',compact('openinghour','contact','gallerycategory','gallery','pageimage'));
}
public function index()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $slider1 = Slider::all();
    $slider = $slider1->all();

    $course1 = Course::select('courses.name as courses',
                                'courses.description',
                                'courses.photos',
                                'courses.price',
                                'coursecategories.name as categories',
                                'coursecategories.id as id')
                ->join('coursecategories','courses.coursecategory_id','coursecategories.id')
                ->get();
    $course = $course1->all();

    $allaboutorg1 = Allaboutorg::all();
    $allaboutorg  = $allaboutorg1[0];

    $feature1 = Feature::all();
    $feature  = $feature1->all();

    $latestcourse1 = Course::orderBy('courses.created_at', 'DESC')
                                ->limit(4)
                                ->get();
    $latestcourse = $latestcourse1->all();

    $latestevent1 = Event::orderBy('events.created_at', 'DESC')
                                ->limit(3)
                                ->get();
    $latestevent = $latestevent1->all();

    $whychooseus1 = Whychooseus::all();
    $whychooseus = $whychooseus1->all();

    $blog1 = Blog::orderBy('blogs.created_at', 'DESC')
                    ->limit(4)
                    ->get();
    $blog = $blog1->all();

    return view('frontend.index',compact('openinghour','contact','slider','course','allaboutorg','feature','latestcourse','latestevent','whychooseus','blog'));
}
public function teachersdetail()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.teachers-detail',compact('openinghour','contact','pageimage'));
}
public function teachers(Request $request)
{
    if($request->keyword)
    {
        $keyword = $request->keyword;
        $teacher1 = Teacher::select('*')
        ->join('users','users.id','teachers.user_id')
        ->where('name','like','%'.$keyword.'%')
        ->get();
        $teacher = $teacher1->all();      
    }
    else
    {
        $teacher1 = Teacher::select('teachers.*','users.*')
        ->join('users','users.id','teachers.user_id')
        ->get();
        $teacher = $teacher1->all();
    }
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $coursecategory1 = Coursecategory::all();
    $coursecategory = $coursecategory1->all();

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
                    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.teachers',compact('openinghour','contact','teacher','coursecategory','pageimage','latestcourse'));
}

public function team()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    $ourteam1 = Ourteam::all();
    $ourteam = $ourteam1->all();

    $pageimage1 = Pageimage::where('pageimagefor','Courses')
    ->get();
    $pageimage = $pageimage1[0];

    return view('frontend.team',compact('ourteam','openinghour','contact','pageimage'));
}

public function register()
{
    $openinghour1 = Openinghour::all();
    $openinghour = $openinghour1->all();

    $contact1 = Contact::all();
    $contact = $contact1[0];

    return view('frontend.register',compact('openinghour','contact'));
}
}
