<?php

namespace App\Http\Controllers\Web\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;

use App\Notifications\Verifyuser;
use App\User;
use App\Role;
use App\Status;
use App\RoleUser;
use App\Student;
use App\Openinghour;
use App\Contact;

class StudentController extends Controller
{
    public function register(Request $request)
    {
       if($request->getMethod() == "GET")
       {
           return view('frontend.register');
       }
       else
       {
        $data = $request->all();
            if(!$request->id)
            {
                $validator = Validator::make($request->all(), [
                    'name' =>'required',
                    'password' =>'required',
                    'address' =>'required',
                    'phone' =>'required',
                    'email' =>'required|email|unique:users',
                    'gender' =>'required',
                    'dateofbirth' =>'required',
                    'photo' =>'required|image',
                ]);

                if ($validator->fails()) 
                {
                    return redirect('student/register')
                        ->withErrors($validator)
                        ->withInput();
                }
                try 
                {
                    DB::beginTransaction();

                    $password = Hash::make($request->password);
                    $data['password'] = $password;

                    $data['verification'] = Str::random(15);

                    $user = User::create($data);

                    $user->notify(new Verifyuser($user));

                    $user_id = $user->id;
                    $rolename = $data['role'];
                    $roleid = Role::where('roles.name','=',$rolename)
                            ->get();
                    $roleid1 = $roleid[0]->id;

                    $data['user_id'] = $user_id;
                    $data['role_id'] = $roleid1;

                    if($request->file('photo'))
                    {
                        $filename = $request->file('photo');
                        $filename->move('teachergallery', $filename->getClientOriginalName());
                        $file = $filename->getClientOriginalName();
                    }
                    $data['file'] = $file;

                    $status = $request->status;

                    $data['status'] = $status;
                    $teacher = Student::create($data);
                    $role = RoleUser::create($data);

                    \LogActivity::addToLog('Student Added');
                    DB::commit();
                    dd("done");
                    return redirect('/student/view')->with('success','Student Added Successfully!!');
                } 
                catch (Exception $e) 
                {
                    throw $e;
                    DB::rollback();
                }
            }
       }
   }
   public function login(Request $request)
   {
        if($request->getMethod() == "GET")
        {
            $openinghour1 = Openinghour::all();
            $openinghour = $openinghour1->all();

            $contact1 = Contact::all();
            $contact = $contact1[0];

            return view ('frontend.login',compact('openinghour','contact'));
        }
        else
        {
            $student1 = Student::select('*')
                            ->join('users','students.user_id','users.id')
                            ->where('users.email',$request->email)
                            ->get();
            $student = $student1->all();
            if($student == null)
            {
                return redirect('/index')->with('danger','Email not found!');
            }
            else
            {
                $openinghour1 = Openinghour::all();
                $openinghour = $openinghour1->all();

                $contact1 = Contact::all();
                $contact = $contact1[0];
                return view('student.studentdashboard',compact('contact','openinghour'));
            }
        }
    }
    public function enroll(Request $request)
    {
        dd($request);
    }
}
