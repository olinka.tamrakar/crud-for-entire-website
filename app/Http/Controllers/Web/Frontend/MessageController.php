<?php

namespace App\Http\Controllers\Web\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Message;

class MessageController extends Controller
{
    public function send(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'form_name' =>'required',
            'form_email' =>'required|unique:messages',
            'form_subject' =>'required',
            'form_phone' =>'required',
            'form_message' =>'required|max:200',
        ]);

        if ($validator->fails()) {
            return redirect('/message')
            ->withErrors($validator)
            ->withInput()
            ->with('warning','Validation failed');
        }
        else
        {
           $data = $request->all();  
           $message = Message::create($data);

           return redirect('/contact');  
       }
   }
}
