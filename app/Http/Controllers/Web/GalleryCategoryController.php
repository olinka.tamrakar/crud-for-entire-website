<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Gallerycategory;

use Validator;

class GallerycategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
            	'id' => null,
                'name' =>null,
            ];
            return view('admin.gallerycategory.addgallerycategory',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'name' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/gallerycategoryform')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();

            if(!$request->id)
            {        
                $data['parent_id'] = empty($data['parent_id']) ? 0 : $data['parent_id'];     
                $gallerycategory = GalleryCategory::create($data);
                \LogActivity::addToLog('GalleryCategory Added'); 
            }
            else
            {
                $data = $request->all();
                GalleryCategory::where('id',$data['id'])->update(
                    [
                        'name' => $data['name'],
                    ]
                );
                \LogActivity::addToLog('GalleryCategory Updated');
            } 
            return redirect('/gallerycategory/view')->with('success','GalleryCategory Updated');            
        }
    }

    public function view()
    {
        $gallerycategory1 = Gallerycategory::all();
        $gallerycategory = $gallerycategory1->all();

        return view('admin.gallerycategory.viewgallerycategory',compact('gallerycategory'));
    }

    public function update($id)
    {   
        $datas1 = Gallerycategory::select('*')
        ->where('gallerycategories.id',$id)
        ->get();

        $datas2 = $datas1->all();
        $datas =$datas2[0];
        $data =[
            'id'               =>  $datas->id,
            'name'            =>  $datas->name,
        ];
        return view('admin.gallerycategory.addgallerycategory',$data);
    }
    public function delete($id)
    {
        $gallerycategory = Gallerycategory::find($id);
        $gallerycategory->delete();
        \LogActivity::addToLog('Gallery Category Deleted');
        return redirect('/gallerycategory/view')->with('infor','Gallery Category Deleted');
    }

    public function display($id)
    {
        $totalgallery1 = Gallerycategory::all();
        $totalgallery = $totalgallery1->all();

        if($id!='null')
        {
            $gallery1 = GalleryCategory::select('*')
            ->join('galleries','galleries.gallerycategory_id','=','gallerycategories.id')
            ->where('gallerycategories.id',$id)
            ->get();
            $gallery = $gallery1->all();


            return view('admin.gallerycategory.displayalbum',compact('gallery','totalgallery'));
        }
        else
        {
            $gallery1 = GalleryCategory::select('*')
            ->join('galleries','galleries.gallerycategory_id','=','gallerycategories.id')
            ->get();
            $gallery = $gallery1->all();          
            return view('admin.gallerycategory.displayalbum',compact('gallery','totalgallery'));
        }
    }
}

