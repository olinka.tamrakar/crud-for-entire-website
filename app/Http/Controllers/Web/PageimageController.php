<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pageimage;
use Validator;

class PageimageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'pageimagefor' =>null,
                'photo' =>null,
            ];
            return view('admin.pageimage.addpageimage',$data);
        }
        else 
        {
            if(!$request->id)
            {
            $validator = Validator::make($request->all(), [
                'pageimagefor' =>'required',
                'photo' =>'required|image'
            ]);

            if ($validator->fails()) {
                return redirect('/pageimage/form')
                ->withErrors($validator)
                ->withInput()
                ->with('warning','Validation failed');
            }
            $data = $request->all();
            if($request->file('photo'))
            {
                $filename = $request->file('photo');
                $filename->move('pageimagegallery', $filename->getClientOriginalName());
                $file = $filename->getClientOriginalName();
            }
            $data['photo'] = $file;              
            $pageimage = Pageimage::create($data);
            \LogActivity::addToLog('Pageimage Added');
            return redirect('/pageimage/view')->with('success','Pageimage Added!!');              
            }
             else
        {
            $validator = Validator::make($request->all(), [
                'pageimagefor' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/pageimage/form')
                ->withErrors($validator)
                ->withInput()
                ->with('warning','Validation failed');
            }
            $data = $request->all();
            Pageimage::where('id',$data['id'])->update(
                [
                    'pageimagefor' => $data['pageimagefor'],
                ]
            );
            \LogActivity::addToLog('Page Image Updated');
            return redirect('/pageimage/view')->with('success','Page Image Updated');
        }  
        }
    }

    public function view()
    {
        $pageimage1 = Pageimage::all();
        $pageimage = $pageimage1->all();

        return view('admin.pageimage.viewpageimage',compact('pageimage'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Pageimage::select('*')
            ->where('pageimages.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $datas =$datas2[0];

            $data =[
                'id'               =>  $datas->id,
                'pageimagefor'     =>  $datas->pageimagefor,
            ];

            return view('admin.pageimage.addpageimage',$data);       
        }

    }
    public function delete($id)
    {
        $pageimage = Pageimage::find($id);
        $pageimage->delete();
        \LogActivity::addToLog('Pageimage Deleted');
        return redirect('/pageimage/view')->with('warning','Pageimage Deleted');
    }
}
