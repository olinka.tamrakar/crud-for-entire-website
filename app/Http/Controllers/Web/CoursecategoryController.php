<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Coursecategory;
use Validator;

class CoursecategoryController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
            	'id' => null,
                'name' =>null,
            ];
            return view('admin.coursecategory.addcoursecategory',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'name' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/coursecategory/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();

            if(!$request->id)
            {         
                $data['parent_id'] = empty($data['parent_id']) ? 0 : $data['parent_id'];    
                $coursecategory = Coursecategory::create($data);
                \LogActivity::addToLog('Category Added');
                return redirect('/coursecategory/view')->with('success','Course Category Added!!');
            }
            else
            {
                $data = $request->all();
                Coursecategory::where('id',$data['id'])->update(
                    [
                        'name' => $data['name'],
                    ]
                );
                \LogActivity::addToLog('Course Category Updated');
                return redirect('/coursecategory/view')->with('success','Course Category Updated!!');
            }  
        }
    }

    public function view()
    {
        $coursecategory1 = Coursecategory::all();
        $coursecategory = $coursecategory1->all();

        return view('admin.coursecategory.viewcoursecategory',compact('coursecategory'));
    }

    public function update($id)
    {   
        $datas1 = Coursecategory::select('*')
        ->where('coursecategories.id',$id)
        ->get();

        $datas2 = $datas1->all();
        $datas =$datas2[0];
        $data =[
            'id'               =>  $datas->id,
            'name'            =>  $datas->name,
        ];
        return view('admin.coursecategory.addcoursecategory',$data);
    }
    public function delete($id)
    {
        $coursecategory = Coursecategory::find($id);
        if($coursecategory->status == 'Active')
        {
            $coursecategory->status = 'Deactive';
            \LogActivity::addToLog('Category Deactivated');
        }
        else if($coursecategory->status == 'Deactive')
        {
            $coursecategory->status = 'Active';
            \LogActivity::addToLog('Category Activated');
        }

        $coursecategory->save();
        return redirect('/coursecategory/view')->with('info','Course Category Status Changed!!');
    }

}
