<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mission;
use Validator;

class MissionController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'title' =>null,
                'description' =>null,
                'photo' =>null,
            ];
            return view('admin.mission.addmission',$data);
        }
        else 
        {
            if(!$request->id)
            {
                $validator = Validator::make($request->all(), [
                    'title' =>'required',
                    'description' =>'required',
                    'photo' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/mission/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                if($request->file('photo'))
                {
                    $filename = $request->file('photo');
                    $filename->move('missiongallery', $filename->getClientOriginalName());
                    $file = $filename->getClientOriginalName();
                }
                $data = $request->all();   
                $data['photo'] = $file;      
                $mission = Mission::create($data);
                \LogActivity::addToLog('Mission Added');
                return redirect('/mission/view')->with('success','Mission Added!!');    
            }

            else
            {
                $data = $request->all();
                mission::where('id',$data['id'])->update(
                    [
                        'title' => $data['title'],
                        'description' => $data['description'],                    ]
                );
                \LogActivity::addToLog('Mission Updated');
                return redirect('/mission/view')->with('success','Mission Updated');
            }      
        }
    }

    public function view()
    {
        $mission1 = Mission::all();
        $mission = $mission1->all();

        return view('admin.mission.viewmission',compact('mission'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = mission::select('*')
            ->where('missions.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.mission.addmission',$data);        
        }

    }
    public function delete($id)
    {
        $mission = mission::find($id);
        $mission->delete();
        \LogActivity::addToLog('Mission Deleted');
        return redirect('/mission/view')->with('warning','Mission Deleted');
    }
}
