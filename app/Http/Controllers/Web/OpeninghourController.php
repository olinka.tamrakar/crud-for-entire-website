<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Openinghour;
use Validator;

class OpeninghourController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'day' =>null,
                'from' =>null,
                'to' =>null,
            ];
            return view('admin.openinghour.addopeninghour',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                    'day' =>'required',
                    'from' =>'required',
                    'to' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/openinghour/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();         
                $openinghour = Openinghour::create($data);
                \LogActivity::addToLog('All About Organization Added');
                return redirect('/openinghour/view')->with('success','All About Organization Added!!');    
            }

            else
            {
                $data = $request->all();
                Openinghour::where('id',$data['id'])->update(
                    [
                        'day' => $data['day'],
                        'from' => $data['from'],
                        'to' => $data['to'],
                    ]
                );
                \LogActivity::addToLog('All About Organization Updated');
                return redirect('/openinghour/view')->with('success','All About Organization Updated');
            }      
        }
    }

    public function view()
    {
        $openinghour1 = openinghour::all();
        $openinghour = $openinghour1->all();

        return view('admin.openinghour.viewopeninghour',compact('openinghour'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = openinghour::select('*')
            ->where('openinghours.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.openinghour.addopeninghour',$data);        
        }

    }
    public function delete($id)
    {
        $openinghour = openinghour::find($id);
        $openinghour->delete();
        \LogActivity::addToLog('openinghourAll About Organization Deleted');
        return redirect('/openinghour/view')->with('warning','All About Organization Deleted');
    }
}

