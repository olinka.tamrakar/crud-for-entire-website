<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Faq;
use Validator;

class FaqController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
            	'id' => null,
                'question' =>null,
                'answer' =>null,
            ];
            return view('admin.faq.addfaq',$data);
        }
        else 
        {
             $validator = Validator::make($request->all(), [
                'question' =>'required',
                'answer' =>'required',
            ]);

            if ($validator->fails()) {
                return redirect('/faq/form')
                ->withErrors($validator)
                ->withInput();
            }

            $data = $request->all();

            if(!$request->id)
            {             
                $faq = Faq::create($data);
                \LogActivity::addToLog('Category Added');
                return redirect('/faq/view')->with('success','FAQs Added!!');
            }
            else
            {
                $data = $request->all();
                Faq::where('id',$data['id'])->update(
                    [
                        'question' => $data['question'],
                        'answer' => $data['answer'],
                    ]
                );
                \LogActivity::addToLog('FAQs Updated');
                return redirect('/faq/view')->with('success','FAQs Updated!!');
            }  
        }
    }

    public function view()
    {
        $faq1 = Faq::all();
        $faq = $faq1->all();

        return view('admin.faq.viewfaq',compact('faq'));
    }

    public function update($id)
    {   
        $datas1 = Faq::select('*')
        ->where('faqs.id',$id)
        ->get();

        $datas2 = $datas1->all();
        $datas =$datas2[0];
        $data =[
            'id'               =>  $datas->id,
            'question'            =>  $datas->question,
            'answer'            =>  $datas->answer,
        ];
        return view('admin.faq.addfaq',$data);
    }
    public function delete($id)
    {
        $faq = Faq::find($id);
        if($faq->status == 'Active')
        {
            $faq->status = 'Deactive';
            \LogActivity::addToLog('Category Deactivated');
        }
        else if($faq->status == 'Deactive')
        {
            $faq->status = 'Active';
            \LogActivity::addToLog('Category Activated');
        }

        $faq->save();
        return redirect('/faq/view')->with('info','FAQs Status Changed!!');
    }

}
