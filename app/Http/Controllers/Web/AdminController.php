<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\RoleUser;
use App\Role;
use App\Teacher;
use App\Course;
use App\Institute;
use App\Blog;
use App\Permission;
use App\Coursecategory;
use App\Editor;
use App\Event;
use App\Gallery;
use App\Gallerycategory;
use App\LogActivity;

use Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index()
    {
        $id = Auth::user()->id;
        $data = User::where('id',$id)
                    ->get();
        $datas = $data[0];

        $user = count(User::all());
        $role = count(Role::all());
        $permission = count(Permission::all());
        $course = count(Course::all());
        $coursecategory = count(Coursecategory::all());
        $teacher = count(Teacher::all());
        $editor = count(Editor::all());
        $institute = count(Institute::all());
        $gallery = count(Gallery::all());
        $gallerycategory = count(Gallerycategory::all());
        $event = count(Event::all());
        $blog = count(Blog::all());
        $logactivity = count(LogActivity::all());

        return view('admin.admindashboard',compact('datas','user','role',
                                                    'permission','course','coursecategory',
                                                    'teacher','editor','institute',
                                                    'gallery','gallerycategory','event',
                                                    'blog','logactivity'));
    }

    public function check(Request $req)
    {
        $email = $req->email;
        $password = md5($req->password);
        $user = User::where('users.email', $email)->first();
        if (!$user)
        {
           return ("User doesnot exist");
        }
        else
        {
            $emailcheck = User::where('users.password',$password)->first();
            if(!$emailcheck)
            {
                \LogActivity::addToLog('Admin Login Attempt Failed');
                return ("Invalid Password")->with('error','Invalid Password');
            }
            else
            {
                $userid = $user->id;

                $roleinfo = RoleUser::select('role_users.role_id')
                    ->where('role_users.user_id','=',$userid)->get();


                $roleinfo1 = $roleinfo[0]->role_id;

                $roleid = Role::find($roleinfo1)->all();

                $role = $roleid[0]->name;

                $data = null;

                if($role == "admin")
                {
                    \LogActivity::addToLog('Admin Login Attempt Successful');
                    return view('admin.admindashboard');
                }               
            }
        }     
    }
}
