<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Slider;
use Validator;


class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'title' =>null,
                'subtitle' =>null,
                'description'=> null,
                'photos' =>null,
            ];
            return view('admin.slider.addslider',$data);
        }
        else 
        {
            if(!$request->id)
            {
            $validator = Validator::make($request->all(), [
                'title' =>'required',
                'subtitle' =>'required',
                'description' =>'required|max:100',
                'photo' =>'required|image'
            ]);

            if ($validator->fails()) {
                return redirect('/slider/form')
                ->withErrors($validator)
                ->withInput()
                ->with('warning','Validation failed');
            }
            $data = $request->all();
            
            if($request->file('photo'))
            {
                $filename = $request->file('photo');
                $filename->move('slidergallery', $filename->getClientOriginalName());
                $file = $filename->getClientOriginalName();
            }
            $data['photo'] = $file;              
            $slider = Slider::create($data);
            \LogActivity::addToLog('Slider Added');
            return redirect('/slider/view')->with('success','Slider Added!!');              
            }
             else
        {
            $validator = Validator::make($request->all(), [
                'title' =>'required',
                'subtitle' =>'required',
                'description' =>'required|max:100',
            ]);

            if ($validator->fails()) {
                return redirect('/slider/form')
                ->withErrors($validator)
                ->withInput()
                ->with('warning','Validation failed');
            }
            $data = $request->all();
            Slider::where('id',$data['id'])->update(
                [
                    'title' => $data['title'],
                    'subtitle' => $data['subtitle'],
                    'description' => $data['description'],
                ]
            );
            \LogActivity::addToLog('Slider Updated');
            return redirect('/slider/view')->with('success','Slider Updated');
        }  
        }
    }

    public function view()
    {
        $slider1 = Slider::all();
        $slider = $slider1->all();

        return view('admin.slider.viewslider',compact('slider'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Slider::select('*')
            ->where('sliders.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $datas =$datas2[0];

            $data =[
                'id'               =>  $datas->id,
                'title'            =>  $datas->title,
                'subtitle'         =>  $datas->subtitle,
                'description'      =>  $datas->description,
            ];

            return view('admin.slider.addslider',$data);       
        }

    }
    public function delete($id)
    {
        $slider = Slider::find($id);
        $slider->delete();
        \LogActivity::addToLog('Slider Deleted');
        return redirect('/slider/view')->with('warning','Slider Deleted');
    }
}