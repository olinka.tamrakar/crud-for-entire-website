<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Link;

class LinkController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'socialmedia' =>null,
                'link' =>null,
                'user_id' =>null,
            ];
            return view('admin.link.addlink',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                   'socialmedia' =>'required',
                'link' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/link/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();         
                $link = Link::create($data);
                \LogActivity::addToLog('Link Added');
                return redirect('/link/view')->with('success','Link Added!!');    
            }

            else
            {
                $data = $request->all();
                Link::where('id',$data['id'])->update(
                    [
                        'socialmedia' => $data['socialmedia'],
                        'link' => $data['link'],
                    ]
                );
                \LogActivity::addToLog('Link Updated');
                return redirect('/link/view')->with('success','Link Updated');
            }      
        }
    }

    public function view()
    {
        $link1 = Link::all();
        $link = $link1->all();

        return view('admin.link.viewlink',compact('link'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Link::select('*')
            ->where('links.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.link.addlink',$data);        
        }

    }
    public function delete($id)
    {
        $link = Link::find($id);
        $link->delete();
        \LogActivity::addToLog('Link Deleted');
        return redirect('/link/view')->with('warning','Link Deleted');
    }
}
