<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;
use App\Whychooseus;

class WhychooseusController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'introduction' =>null,
                'question' =>null,
                'answer' =>null,
            ];
            return view('admin.whychooseus.addwhychooseus',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                   'introduction' =>'required',
                   'question' =>'required',
                   'answer' =>'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/whychooseus/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();         
                $whychooseus = Whychooseus::create($data);
                \LogActivity::addToLog('Why Choose Us Added');
                return redirect('/whychooseus/view')->with('success','Why Choose Us Added!!');    
            }

            else
            {
                $data = $request->all();
                Whychooseus::where('id',$data['id'])->update(
                    [
                        'introduction' => $data['introduction'],
                        'question' => $data['question'],
                        'answer' => $data['answer'],
                    ]
                );
                \LogActivity::addToLog('Why Choose Us Updated');
                return redirect('/whychooseus/view')->with('success','Why Choose Us Updated');
            }      
        }
    }

    public function view()
    {
        $whychooseus1 = Whychooseus::all();
        $whychooseus = $whychooseus1->all();

        return view('admin.whychooseus.viewwhychooseus',compact('whychooseus'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Whychooseus::select('*')
            ->where('whychooseuses.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.whychooseus.addwhychooseus',$data);        
        }

    }
    public function delete($id)
    {
        $whychooseus = Whychooseus::find($id);
        $whychooseus->delete();
        \LogActivity::addToLog('Why Choose Us Deleted');
        return redirect('/whychooseus/view')->with('warning','Why Choose Us Deleted');
    }
}