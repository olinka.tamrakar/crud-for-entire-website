<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\LogActivity;

class FilterController extends Controller
{
    public function filter(Request $request)
    {
        $date = $request->date;
    	$title = $request->title;
        $action = $request->action;
        $check = $title.' '.$action;

        $log = new LogActivity();

    $logs = LogActivity::where('subject',$check)
                            ->get();
    	
    	for ($i=0; $i < count($logs); $i++) 
        { 
            $date[$i] = $logs[$i]->created_at->toDateString();
            $time[$i] = $logs[$i]->created_at->toTimeString();
        }	
    	return view('logActivity',compact('logs','date','time'));
    }
}
