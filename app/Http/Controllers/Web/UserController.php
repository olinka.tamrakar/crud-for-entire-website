<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;

class UserController extends Controller
{
    public function index()
    {
    	$users = User::all();

    	$roles = Role::select('*')
    				->join('role_users','role_users.role_id','=','roles.id')
    				->get();

    	return view('admin.user.viewuser',compact('users','roles'));
    }
}
