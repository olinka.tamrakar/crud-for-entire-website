<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Indexpage;
use Validator;

class IndexpageController extends Controller
{
    public function add(Request $request)
    {
        if($request->getMethod() == "GET")
        {
            $data =[
                'id' =>null,
                'heading' =>null,
                'quote' =>null,
                'description' =>null,
                'photo'=> null,
            ];
            return view('admin.indexpage.addindexpage',$data);
        }
        else 
        {
            if(!$request->id)
            {

                $validator = Validator::make($request->all(), [
                    'heading' =>'required',
                	'quote' =>'required',
                	'description' =>'required',
                	'photo'=> 'required',
               ]);

                if ($validator->fails()) {
                    return redirect('/indexpage/form')
                    ->withErrors($validator)
                    ->withInput()
                    ->with('warning','Validation failed');
                }
                $data = $request->all();         
                $indexpage = Indexpage::create($data);
                \LogActivity::addToLog('indexpage Added');
                return redirect('/indexpage/view')->with('success','indexpage Added!!');    
            }

            else
            {
                $data = $request->all();
                Indexpage::where('id',$data['id'])->update(
                    [
                        'heading' => $data['heading'],
                        'quote' => $data['quote'],
                        'description' => $data['description'],
                    ]
                );
                \LogActivity::addToLog('Indexpage Updated');
                return redirect('/indexpage/view')->with('success','Indexpage Updated');
            }      
        }
    }

    public function view()
    {
        $indexpage1 = Indexpage::all();
        $indexpage = $indexpage1->all();

        return view('admin.indexpage.viewindexpage',compact('indexpage'));
    }

    public function update(Request $request,$id)
    {
        if($request->getMethod() == 'GET')
        {
            $datas1 = Indexpage::select('*')
            ->where('indexpages.id',$id)
            ->get();

            $datas2 = $datas1->all();
            $data =$datas2[0];                         

            return view('admin.indexpage.addindexpage',$data);        
        }

    }
    public function delete($id)
    {
        $indexpage = Indexpage::find($id);
        $indexpage->delete();
        \LogActivity::addToLog('indexpage Deleted');
        return redirect('/indexpage/view')->with('warning','indexpage Deleted');
    }
}
