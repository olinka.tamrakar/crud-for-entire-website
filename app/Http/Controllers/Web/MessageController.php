<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Message;

class MessageController extends Controller
{
	public function view()
	{
		$message = Message::all();
		return view('admin.message.viewmessage',compact('message'));
	}
}
