<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Blog;
use App\User;
use App\CourseTeacher;
use App\Contact;
use App\Openinghour;
use App\Teacher;
use App\Course;
use App\InstituteTeacher;
use App\Institute;
use App\CourseInstitute;
use App\Role;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        if(Auth::user())
        {
            $id = Auth::user()->id;

            $role1 = Role::select('*')
            ->join('role_users','roles.id','=','role_users.role_id')
            ->where('role_users.user_id',$id)
            ->get();
            $role = $role1[0]->name;
            if($role == 'Admin')
            {
                return redirect('/admin');
            }
            else if($role == 'Teacher')
            {
                return redirect('teacherdashboard');
            }
            else if($role == 'Editor')
            {
                return redirect('editordashboard');
            }
            else if($role == 'Institute')
            {
                return redirect('institutedashboard');
            }
            else
            {
                return view('auth.login');
            }
        }
        else
        {
            return view('auth.login');
        }

    }
    public function teacher()
    {
    	$id = Auth::user()->id;
        $data = User::where('id',$id)
        ->get();
        $datas = $data[0];

        $course1 = Course::where('user_id',$id)
        ->get();

        $teacher1 = Teacher::where('user_id',$id)
        ->get();
        $teacher = $teacher1[0];

        $course = count($course1);

        return view('teacher.teacherdashboard',compact('datas','course','teacher'));
    }

    public function institute()
    {
    	$id = Auth::user()->id;
        $data = User::where('id',$id)
        ->get();
        $datas = $data[0];

        $institute_id1 = Institute::select('institutes.id')
        ->where('user_id',$id)
        ->get();
        $institute_id = $institute_id1[0]->id;

        $teacher1 = InstituteTeacher::where('institute_teachers.institute_id','=',$institute_id)
        ->get();
        $teacher = count($teacher1);

        $course1 = CourseInstitute::where('course_institutes.institute_id','=',$institute_id)
        ->get();
        $course = count($course1);

        return view('institute.institutedashboard',compact('datas','teacher','course'));
    }
    public function editor()
    {
    	$id = Auth::user()->id;
        $data = User::where('id',$id)
        ->get();
        $datas = $data[0];

        $blog = count(Blog::all());

        return view('editor.editordashboard',compact('datas','blog'));
    }
}
