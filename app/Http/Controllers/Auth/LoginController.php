<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;
use App\Teacher;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        foreach ($this->guard()->user()->role as $role) 
        {
        	$user = $this->guard()->user();
        	$id = $this->guard()->user()->id;
            
            if ($role->name == 'Admin') 
            {
                return redirect('/admin');
            }
            else if($role->name == 'Teacher')
            {
            	$teacher1 = Teacher::select('teachers.status')
            						->where('user_id',$id)
            						->get();
            	$teacher = $teacher1->all();
            	if($teacher[0]->status == 'Active')
            	{
                	return redirect('/teacherdashboard');	
                }
                else
                {
                    return view('auth.login')->with('error','Sorry!You cannot login!!');
                }
            }
            else if($role->name == 'Institute')
            {
                return redirect('/institutedashboard');
            }
            else
            {
                return redirect('/editordashboard');
            }
        }
    }
}
