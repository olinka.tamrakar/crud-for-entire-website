<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indexpage extends Model
{
   protected $fillable =['heading','quote','description','photo'];
}
