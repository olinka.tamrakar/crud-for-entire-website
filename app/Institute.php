<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $fillable = ['website','logo','establisheddate','status','user_id'];

    public function courses(){
       return $this->belongsToMany('App\Course','course_institutes');
   }

    public function teachers(){
       return $this->belongsToMany('App\Teacher','institute_teachers');
   }
}
