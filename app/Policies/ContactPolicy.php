<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function view(User $user)
    {
        return $this->getpermission($user,55);
    }

    public function create(User $user)
    {
        return $this->getpermission($user,53);
    }

    public function update(User $user)
    {
        return $this->getpermission($user,54);
    }

    public function getpermission(User $user,$permission_id)
    {
        foreach ($user->role as $role) 
        {
            foreach ($role->permission as $permission) {
                if($permission->id == $permission_id)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
