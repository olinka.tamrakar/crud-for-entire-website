<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MissionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function view(User $user)
    {
        return $this->getpermission($user,61);
    }

    public function create(User $user)
    {
        return $this->getpermission($user,59);
    }

    public function update(User $user)
    {
        return $this->getpermission($user,60);
    }
    public function delete(User $user)
    {
        return $this->getpermission($user,70);
    }

    public function getpermission(User $user,$permission_id)
    {
        foreach ($user->role as $role) 
        {
            foreach ($role->permission as $permission) {
                if($permission->id == $permission_id)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
