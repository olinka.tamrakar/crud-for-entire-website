<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OurteamPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
     public function view(User $user)
    {
        return $this->getpermission($user,74);
    }

    public function create(User $user)
    {
        return $this->getpermission($user,71);
    }

    public function update(User $user)
    {
        return $this->getpermission($user,72);
    }
    public function delete(User $user)
    {
        return $this->getpermission($user,73);
    }

    public function getpermission(User $user,$permission_id)
    {
        foreach ($user->role as $role) 
        {
            foreach ($role->permission as $permission) {
                if($permission->id == $permission_id)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
