<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OpeninghourPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function view(User $user)
    {
        return $this->getpermission($user,78);
    }

    public function create(User $user)
    {
        return $this->getpermission($user,75);
    }

    public function update(User $user)
    {
        return $this->getpermission($user,76);
    }

    public function delete(User $user)
    {
        return $this->getpermission($user,77);
    }

    public function getpermission(User $user,$permission_id)
    {
        foreach ($user->role as $role) 
        {
            foreach ($role->permission as $permission) {
                if($permission->id == $permission_id)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
