<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteTeacher extends Model
{
    protected $fillable = ['institute_id','teacher_id',];
}
