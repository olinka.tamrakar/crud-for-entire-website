<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coursecategory extends Model
{
    protected $fillable = ['name','status','parent_id'];

    public function childs() {
        return $this->hasMany('App\Category','parent_id','id') ;
    }
}
