<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['topics','host','location','starttime','endtime','startdate','enddate','website','photos','keynotespeakers','description',];
}
