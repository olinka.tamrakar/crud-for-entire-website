<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['gender','dateofbirth','photo','status','user_id'];
}
