<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Openinghour extends Model
{
    protected $fillable = ['day','from','to'];
}
