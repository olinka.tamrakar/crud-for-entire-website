<?php


namespace App\Helpers;
use Request;
use Auth;
use App\LogActivity as LogActivityModel;


class LogActivity
{

    public static function addToLog($subject)
    {
        $log = [];
        $log['subject'] = $subject;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        if(Auth::user())
        {
            $log['user_id'] = Auth::user()->id;
        }
        else
        {
            $log['user_id'] = 0;   
        }
        LogActivityModel::create($log);
    }


    public static function logActivityLists()
    {
    	return LogActivityModel::latest()->get();
    }


}