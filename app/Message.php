<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['form_name','form_email','form_subject','form_phone','form_message'];
}
