<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
    protected $fillable = ['syllabusname','classtime','estimatetime','courseduration','course_id'];
}
