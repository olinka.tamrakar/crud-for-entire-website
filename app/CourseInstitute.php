<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseInstitute extends Model
{
     protected $fillable = ['course_id','institute_id',];
}
