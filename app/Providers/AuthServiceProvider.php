<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\RoleUser;
use App\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-only', function ($user) 
        {
            $user_id = $user->id;
            $role_id = RoleUser::select('roles.*')
                        ->join('roles','role_users.role_id','=','roles.id')
                        ->where('role_users.user_id',$user_id)
                        ->get();
            $role = $role_id[0]->name;
           if ($role == 'Admin') 
           {
                return true;
            } 
            return false;
        });

        Gate::define('teacheronly', function ($user) 
        {
            $user_id = $user->id;
            $role_id = RoleUser::select('roles.*')
                        ->join('roles','role_users.role_id','=','roles.id')
                        ->where('role_users.user_id',$user_id)
                        ->get();
            $role = $role_id[0]->name;
           if ($role == 'Teacher') 
           {
                return true;
            } 
            return false;
        });

        Gate::define('instituteonly', function ($user) 
        {
            $user_id = $user->id;
            $role_id = RoleUser::select('roles.*')
                        ->join('roles','role_users.role_id','=','roles.id')
                        ->where('role_users.user_id',$user_id)
                        ->get();
            $role = $role_id[0]->name;
           if ($role == 'Institute') 
           {
                return true;
            } 
            return false;
        });

        Gate::define('editoronly', function ($user) 
        {
            $user_id = $user->id;
            $role_id = RoleUser::select('roles.*')
                        ->join('roles','role_users.role_id','=','roles.id')
                        ->where('role_users.user_id',$user_id)
                        ->get();
            $role = $role_id[0]->name;
           if ($role == 'Editor') 
           {
                return true;
            } 
            return false;
        });

        Gate::resource('blogs','App\Policies\BlogPolicy');
        Gate::resource('courses','App\Policies\CoursePolicy');
        Gate::resource('editors','App\Policies\EditorPolicy');
        Gate::resource('events','App\Policies\EventPolicy');
        Gate::resource('gallerys','App\Policies\GalleryPolicy');
        Gate::resource('institutes','App\Policies\InstitutePolicy');
        Gate::resource('teachers','App\Policies\TeacherPolicy');
        Gate::resource('coursecategorys','App\Policies\CourseCategoryPolicy');
        Gate::resource('permissions','App\Policies\PermissionPolicy');
        Gate::resource('users','App\Policies\UserPolicy');
        Gate::resource('roles','App\Policies\RolePolicy');
        Gate::resource('gallerycategorys','App\Policies\GalleryCategoryPolicy');
        Gate::resource('blogcategorys','App\Policies\BlogCategoryPolicy');
        Gate::resource('faqs','App\Policies\FaqPolicy');
        Gate::resource('contacts','App\Policies\ContactPolicy');
        Gate::resource('allaboutorgs','App\Policies\AllaboutorgPolicy');
        Gate::resource('missions','App\Policies\MissionPolicy');
        Gate::resource('whyuss','App\Policies\WhyusPolicy');
        Gate::resource('workflows','App\Policies\WorkflowPolicy');
        Gate::resource('ourteams','App\Policies\OurteamPolicy');
        Gate::resource('indexpages','App\Policies\IndexpagePolicy');
        Gate::resource('links','App\Policies\LinkPolicy');
        Gate::resource('features','App\Policies\FeaturesPolicy');
        Gate::resource('whychooseuss','App\Policies\WhychooseusPolicy');
        Gate::resource('openinghours','App\Policies\OpeninghourPolicy');
        Gate::resource('sections','App\Policies\SectionPolicy');
        Gate::resource('syllabuss','App\Policies\SyllabusPolicy');
        Gate::resource('sliders','App\Policies\SliderPolicy');
        Gate::resource('pageimages','App\Policies\PageimagePolicy');
    }
}
