<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allaboutorg extends Model
{
    protected $fillable =['organization','description','photo'];
}
