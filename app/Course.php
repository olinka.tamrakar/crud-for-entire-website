<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable =['name','price','description','features','photos','user_id','coursecategory_id'];

    public function institutes(){
       return $this->belongsToMany('App\Institute','institutes');
   }
}
