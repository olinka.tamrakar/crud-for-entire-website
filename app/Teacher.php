<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['specialization','experiences','subjects','followers','classes','user_id','file','status','description'];

    public function courses(){
       return $this->belongsToMany('App\Course','course_teachers');
   }

   public function institutes(){
       return $this->belongsToMany('App\Institute','institute_teachers');
   }
}
