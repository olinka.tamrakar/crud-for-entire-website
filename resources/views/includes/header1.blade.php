<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>CRUD</title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

  <!-- Bootstrap 3.3.2 -->
  <link href="{{ asset('css/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

  <!-- Ionicons -->
  <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />

  <!-- DATA TABLES -->
  <link href="{{ asset('datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />

  <!-- bootstrap wysihtml5 - text editor -->
  <link href="{{ asset('css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />

  <!-- iCheck for checkboxes and radio inputs -->
  <link href="{{ asset('css/plugins/iCheck/minimal/blue.css') }}" rel="stylesheet" type="text/css" />

  <!-- Theme style -->
  <link href="{{ asset('css/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins 
     folder instead of downloading all of them to reduce the load. -->
     <link href="{{ asset('css/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="text/css" />

     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
    </head>
    <body class="skin-blue">
      <div class="wrapper">

        <header class="main-header">
          <a href="/" class="logo"><b>CRUD</b></a>
          <!-- Header Navbar: style can be found in header.less -->
          <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">4</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 4 messages</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- start message -->
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('css/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image"/>
                            </div>
                            <h4>
                              Support Team
                              <small><i class="fa fa-clock-o"></i> 5 mins</small>
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                          </a>
                        </li><!-- end message -->
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('css/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="user image"/>
                            </div>
                            <h4>
                              AdminLTE Design Team
                              <small><i class="fa fa-clock-o"></i> 2 hours</small>
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('css/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="user image"/>
                            </div>
                            <h4>
                              Developers
                              <small><i class="fa fa-clock-o"></i> Today</small>
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('css/dist/img/user3-128x128.jpg') }}" class="img-circle" alt="user image"/>
                            </div>
                            <h4>
                              Sales Department
                              <small><i class="fa fa-clock-o"></i> Yesterday</small>
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <div class="pull-left">
                              <img src="{{ asset('css/dist/img/user4-128x128.jpg') }}" class="img-circle" alt="user image"/>
                            </div>
                            <h4>
                              Reviewers
                              <small><i class="fa fa-clock-o"></i> 2 days</small>
                            </h4>
                            <p>Why not buy a new awesome theme?</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">See All Messages</a></li>
                  </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
                <li class="dropdown notifications-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">10</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 10 notifications</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li>
                          <a href="#">
                            <i class="fa fa-users text-aqua"></i> 5 new members joined today
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-users text-red"></i> 5 new members joined
                          </a>
                        </li>

                        <li>
                          <a href="#">
                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-user text-red"></i> You changed your username
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="footer"><a href="#">View all</a></li>
                  </ul>
                </li>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger">9</span>
                  </a>
                  <ul class="dropdown-menu">
                    <li class="header">You have 9 tasks</li>
                    <li>
                      <!-- inner menu: contains the actual data -->
                      <ul class="menu">
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              Design some buttons
                              <small class="pull-right">20%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">20% Complete</span>
                              </div>
                            </div>
                          </a>
                        </li><!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              Create a nice theme
                              <small class="pull-right">40%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">40% Complete</span>
                              </div>
                            </div>
                          </a>
                        </li><!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              Some task I need to do
                              <small class="pull-right">60%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </a>
                        </li><!-- end task item -->
                        <li><!-- Task item -->
                          <a href="#">
                            <h3>
                              Make beautiful transitions
                              <small class="pull-right">80%</small>
                            </h3>
                            <div class="progress xs">
                              <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">80% Complete</span>
                              </div>
                            </div>
                          </a>
                        </li><!-- end task item -->
                      </ul>
                    </li>
                    <li class="footer">
                      <a href="#">View all tasks</a>
                    </li>
                  </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('css/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"/>
                    <span class="hidden-xs">{{Auth::user()->name}}</span>
                  </a>
                  <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                      <img src="{{ asset('css/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />
                      <p>
                        {{Auth::user()->name}} <!-- - Web Developer -->
                        <!-- <small>Member since Nov. 2012</small> -->
                      </p>
                    </li>
                    <!-- Menu Body -->
                  <!-- <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li> -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{Route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a class="btn btn-default btn-flat href="{{ route('logout') }}" onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Sign out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ asset('css/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />
          </div>
          <div class="pull-left info">
            <h4><p>{{Auth::user()->name}}</p></h4 >
          </div>
        </div>
        <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            @canany(['teachers.view','editors.view','institutes.view'],Auth::user())
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                @can('teachers.view',Auth::user())
                <li>
                  <a href="{{Route('teacher.view')}}">
                    <i class="fa fa-user"></i>Teacher
                  </a>
                </li>
                @endcan

                @can('editors.view',Auth::user())
                <li>
                  <a href="{{Route('editor.view')}}">
                    <i class="fa fa-user"></i>Editor
                  </a></li>
                  @endcan

                  @can('institutes.view',Auth::user())
                  <li>
                    <a href="{{Route('institute.view')}}">
                      <i class="fa fa-user"></i>Institute
                    </a>
                  </li>
                  @endcan
                </ul>
              </li>
            @endcanany

            @canany(['roles.view','permissions.view'],Auth::user())
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-group"></i>
                  <span>Roles and Permissions</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  @can('roles.view',Auth::user())
                  <li>
                    <a href="{{Route('role.view')}}">
                      <i class="fa fa-group"></i>Roles
                    </a>
                  </li>
                  @endcan
                  @can('permissions.view',Auth::user())
                  <li>
                    <a href="{{Route('permission.view')}}">
                      <i class="fa fa-lock"></i>Permissions
                    </a>
                  </li>
                  @endcan
                </ul>
              </li>
            @endcanany

            @canany(['gallerys.view','sliders.view','pageimages.view'],Auth::user())
             <li class="treeview">
                <a href="#">
                  <i class="fa fa-image"></i>
                  <span>Gallery Settings</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
              @can('gallerys.view',Auth::user())
              <li>
                <a href="{{Route('gallery.view')}}">
                  <i class="fa fa-image"></i>Gallery
                </a>
              </li>
              @endcan

              @can('sliders.view',Auth::user())
              <li>
                <a href="{{Route('slider.view')}}">
                  <i class="fa fa-image"></i>Slider
                </a>
              </li>
              @endcan

              @can('pageimages.view',Auth::user())
              <li>
                <a href="{{Route('pageimage.view')}}">
                  <i class="fa fa-image"></i>Page Image
                </a>
              </li>
              @endcan
              </ul>
              </li>
            @endcanany

            @canany(['gallerycategorys.view','blogcategorys.view','coursecategorys.view'],Auth::user())
             <li class="treeview">
                <a href="#">
                  <i class="fa fa-user"></i>
                  <span>Category Settings</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
              @can('gallerycategorys.view',Auth::user())
              <li>
                <a href="{{Route('gallerycategory.view')}}">
                  <i class="fa fa-image"></i>Gallery Category
                </a>
              </li>
              @endcan

              @can('blogcategorys.view',Auth::user())
              <li>
                <a href="{{Route('blogcategory.view')}}">
                  <i class="fa fa-newspaper-o"></i>Blog Category
                </a>
              </li>
              @endcan

              @can('coursecategorys.view',Auth::user())
              <li>
                <a href="{{Route('coursecategory.view')}}">
                  <i class="fa fa-book"></i>Course Category
                </a>
              </li>
              @endcan
             </ul>
             </li>
            @endcanany
              

            @canany(['ourteams.view','allaboutorgs.view','missions.view','whyuss.view','workflow.view','faqs.view','indexpages.view','features.view','whychooseuss.view'],Auth::user())    
              <li>
                <a href="#">
                  <i class="fa fa-newspaper-o"></i>
                 Page Settings
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">         
              @can('ourteams.view',Auth::user())  
              <li>
                <a href="{{Route('ourteam.view')}}">
                  <i class="fa fa-group"></i>Our Team
                </a>
              </li>
              @endcan

              @can('allaboutorgs.view',Auth::user())  
              <li>
                <a href="{{Route('allaboutorg.view')}}">
                  <i class="fa fa-cloud"></i>All About Org
                </a>
              </li>
              @endcan

              @can('missions.view',Auth::user())
              <li>
                <a href="{{Route('mission.view')}}">
                  <i class="fa fa-cloud"></i>Mission
                </a>
              </li>
              @endcan

              @can('whyuss.view',Auth::user())
              <li>
                <a href="{{Route('whyus.view')}}">
                  <i class="fa fa-cloud"></i>Why Us
                </a>
              </li>
              @endcan

              @can('workflow.view',Auth::user())
              <li>
                <a href="{{Route('workflow.view')}}">
                  <i class="fa fa-cloud"></i>Work Flow
                </a>
              </li>
              @endcan

              @can('faqs.view',Auth::user())  
              <li>
                <a href="{{Route('faq.view')}}">
                  <i class="fa fa-question"></i>FAQs
                </a>
              </li>
              @endcan

              @can('indexpages.view',Auth::user())
              <li>
                <a href="{{Route('indexpage.view')}}">
                  <i class="fa fa-cloud"></i>Index Page
                </a>
              </li>
              @endcan

              @can('features.view',Auth::user())
              <li>
                <a href="{{Route('feature.view')}}">
                  <i class="fa fa-cloud"></i>Feature
                </a>
              </li>
              @endcan

              @can('whychooseuss.view',Auth::user())
              <li>
                <a href="{{Route('whychooseus.view')}}">
                  <i class="fa fa-cloud"></i>Why Choose Us
                </a>
              </li>
              @endcan

              </ul>
              </li>
            @endcanany
            
            @canany(['contacts.view','openinghours.view','links.view'],Auth::user())
             <li>
                <a href="#">
                  <i class="fa fa-newspaper-o"></i>
                 Contact Settings
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">   
              @can('contacts.view',Auth::user())
              <li>
                <a href="{{Route('contact.view')}}">
                  <i class="fa fa-history"></i>Contact
                </a>
              </li>
              @endcan

              @can('openinghours.view',Auth::user())
              <li>
                <a href="{{Route('openinghour.view')}}">
                  <i class="fa fa-history"></i>Opening Hour
                </a>
              </li>
              @endcan

              @can('links.view',Auth::user())
              <li>
                <a href="{{Route('link.view')}}">
                  <i class="fa fa-cloud"></i>Link
                </a>
              </li>
              @endcan
              </ul>
              </li>
            @endcanany

            <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i> <span>Account Settings</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                    <li><a href="/settings/name"><i class="fa fa-user"></i> Name</a></li>
                    <li><a href="/settings/address"><i class="fa fa-home"></i> Address</a></li>
                    <li><a href="/settings/phone"><i class="fa fa-phone"></i> Phone</a></li>
                    <li><a href="/settings/password"><i class="fa fa-lock"></i> Password</a></li>
                    <li><a href="/settings/email"><i class="fa fa-envelope-o"></i> Email</a></li>
              </ul>
            </li>

              @can('courses.view',Auth::user())
              <li class="treeview">
                <a href="{{Route('course.view')}}">
                  <i class="fa fa-book"></i> <span>Course</span>
                </a>
              </li>
              @endcan
              
              @can('events.view',Auth::user())
              <li class="treeview">
                <a href="{{Route('event.view')}}">
                  <i class="fa fa-calendar"></i> <span>Events</span> 
                </a>
              </li>
              @endcan


              @can('blogs.view',Auth::user())
              <li class="treeview">
                <a href="{{Route('blog.view')}}">
                  <i class="fa fa-newspaper-o"></i> <span>Blog</span>
                </a>
              </li>
              @endcan

              @can('logactivitys.view',Auth::user())
              <li class="treeview">
                <a href="{{Route('logactivity')}}">
                  <i class="fa fa-history"></i> <span>Log Activity</span>
                </a>
              </li>
              @endcan

              @can('messages.view',Auth::user())  
              <li class="treeview">
                <a href="{{Route('message.view')}}">
                  <i class="fa fa-history"></i> <span>Message</span>
                </a>
              </li>
              @endcan

              @can('teachers.assign|teachers.viewassign',Auth::user())
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-user"></i> <span>Teacher</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                 <li><a href="{{Route('getassignteacher')}}"><i class="fa fa-circle-o"></i> Add My Teacher</a></li>
                 <li><a href="{{Route('viewteacher')}}"><i class="fa fa-circle-o"></i> View My Teacher</a></li>
               </ul>
             </li>
             @endcan

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper" style="min-height: 800px;">
        @include('flashmessage')
        @yield('content-wrapper')

      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> New 
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="/">Code Gamut</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->

    <script src="{{ asset('css/plugins/jQuery/jQuery-2.1.3.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('css/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- DATA TABES SCRIPT -->
    <script src="{{ asset('datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="{{ asset('css/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{ asset('css/plugins/fastclick/fastclick.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('css/dist/js/app.min.js') }}" type="text/javascript"></script>    
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('css/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="{{ asset('css/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
    <!-- Page script -->
    <script type="text/javascript">
      $(function () {

        "use strict";

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"]').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });

        //When unchecking the checkbox
        $("#check-all").on('ifUnchecked', function (event) {
          //Uncheck all checkboxes
          $("input[type='checkbox']", ".table-mailbox").iCheck("uncheck");
        });
        //When checking the checkbox
        $("#check-all").on('ifChecked', function (event) {
          //Check all checkboxes
          $("input[type='checkbox']", ".table-mailbox").iCheck("check");
        });
        //Handle starring for glyphicon and font awesome
        $(".fa-star, .fa-star-o, .glyphicon-star, .glyphicon-star-empty").click(function (e) {
          e.preventDefault();
          //detect type
          var glyph = $(this).hasClass("glyphicon");
          var fa = $(this).hasClass("fa");

          //Switch states
          if (glyph) {
            $(this).toggleClass("glyphicon-star");
            $(this).toggleClass("glyphicon-star-empty");
          }

          if (fa) {
            $(this).toggleClass("fa-star");
            $(this).toggleClass("fa-star-o");
          }
        });

        //Initialize WYSIHTML5 - text editor
        $("#email_message").wysihtml5();
      });
    </script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('css/dist/js/demo.js') }}" type="text/javascript"></script>

    <!-- JQuery Validation -->
    <script src="{{ asset('js/validation/jquery_validate.js') }}"></script>
    <script src="{{ asset('js/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('js/validation/validation.js') }}"></script>

    <!-- page script -->
    <script type="text/javascript">
      $(function () {
        $('#example1').dataTable({
          "bPaginate": true,
          "bLengthChange": true,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>

    <script type="text/javascript">
      $('#addforms').submit(function() {
        $(this).find("button[type='submit']").prop('disabled',true);
      });
    </script>
    
    <script>
      $(document).ready(function(){
        var next = 1;
        $(".add-more").click(function(e){
          e.preventDefault();
          var addto = "#field" + next;
          var addRemove = "#field" + (next);
          next = next + 1;
          var newIn = '<input autocomplete="off" class="input form-control" id="field' + next + '" name="features[]' + next + '" type="text">';
          var newInput = $(newIn);
          var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
          var removeButton = $(removeBtn);
          $(addto).after(newInput);
          $(addRemove).after(removeButton);
          $("#field" + next).attr('data-source',$(addto).attr('data-source'));
          $("#count").val(next);  

          $('.remove-me').click(function(e){
            e.preventDefault();
            var fieldNum = this.id.charAt(this.id.length-1);
            var fieldID = "#field" + fieldNum;
            $(this).remove();
            $(fieldID).remove();
          });
        });   
      });

    </script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript">
    </script>
    <script>
      $(function () {
        //Add text editor
        $("#description").wysihtml5();
      });
    </script>
  </body>
  </html>
