@extends('includes.header1')

@section('content-wrapper')
	
	<center><h1>Hello {{Auth::user()->name}}</h1></center>
	<div class="box">
	<div class="box-header">
		<h3 class="box-title">Your information</h3>
	</div><!-- /.box-header -->

	<div class="box-body no-padding">
		<table class="table table-condensed">
			<tr>
				<th>ID</th>
				<td>{{Auth::user()->id}}</td>
				<th>Action</th>
			</tr>

			<tr title="Change Name">
				<th>Name</th>
				<td>{{Auth::user()->name}}</td>
				<td><a href="settings/name"><i class="fa fa-edit"></i></a></td>
			</tr>

			<tr title="Change Email">	
				<th>Email</th>
				<td>{{Auth::user()->email}}</td>
				<td><a href="settings/email"><i class="fa fa-edit"></i></a></td>

			</tr>

			<tr title="Change Address">
				<th>Address</th>
				<td>{{Auth::user()->address}}</td>
				<td><a href="settings/address"><i class="fa fa-edit"></i></a></td>

			</tr>
			
			<tr title="Change Phone">
				<th>Phone</th>
				<td>{{Auth::user()->phone}}</td>
				<td><a href="settings/phone"><i class="fa fa-edit"></i></a></td>

			</tr>

			<tr title="Change Password">
				<th>Password</th>
				<td>*****************</td>
				<td><a href="settings/password"><i class="fa fa-edit"></i></a></td>

			</tr>
		</table>
	</div>
	</div>
@endsection