@extends('includes.header1')
@section('content-wrapper')

<body class="register-page"> 
<div class="register-box">
<div class="register-logo">
	<a href="../../index2.html"><b>Change Address</b></a>
</div>

<div class="register-box-body">
	<form action="/settings/address" method="POST" enctype="multipart/form-data" id="addforms">
		@csrf
		<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-home form-control-feedback"></span>
				<input id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="New address" required>

				@if ($errors->has('address'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('address') }}</strong>
				</span>
				@endif
			</div>

	<div class="row">
		<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px;">
				{{ __('Confirm New Address') }}
			</button>

			<button type="reset" class="btn btn-danger btn-block btn-flat" style="width: 150px;">
				{{ __('Cancel') }}
			</button>
		</div>
	</div>
	</form>
</div>
</div>
</body>
@endsection