@extends('includes.header1')
@section('content-wrapper')

<body class="register-page"> 
<div class="register-box">
<div class="register-logo">
	<a href="../../index2.html"><b>Change Phone</b></a>
</div>

<div class="register-box-body">
	<form action="/settings/phone" method="POST" enctype="multipart/form-data" id="addforms">
		@csrf
		<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-phone form-control-feedback"></span>
				<input id="phone" type="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="New Phone" required>

				@if ($errors->has('phone'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('phone') }}</strong>
				</span>
				@endif
			</div>

	<div class="row">
		<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px;">
				{{ __('Confirm New Phone') }}
			</button>

			<button type="reset" class="btn btn-danger btn-block btn-flat" style="width: 150px;">
				{{ __('Cancel') }}
			</button>
		</div>
	</div>
	</form>
</div>
</div>
</body>
@endsection