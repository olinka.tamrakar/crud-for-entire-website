@extends('includes.header1')
@section('content-wrapper')

<body class="register-page"> 
<div class="register-box">
<div class="register-logo">
	<a href="../../index2.html"><b>Change Password</b></a>
</div>

<div class="register-box-body">
	<form action="/settings/password" method="POST" enctype="multipart/form-data" id="addforms">
		@csrf

		<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<input id="oldpassword" type="password" class="form-control{{ $errors->has('oldpassword') ? ' is-invalid' : '' }}" name="oldpassword" placeholder="Old Password" required autofocus>

				@if ($errors->has('oldpassword'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('oldpassword') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<input id="newpassword" type="password" class="form-control{{ $errors->has('newpassword') ? ' is-invalid' : '' }}" name="newpassword" placeholder="New Password" required autofocus>

				@if ($errors->has('newpassword'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('newpassword') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<input id="confirmpassword" type="password" class="form-control{{ $errors->has('confirmpassword') ? ' is-invalid' : '' }}" name="confirmpassword" placeholder="Re-Enter Password" required autofocus>

				@if ($errors->has('confirmpassword'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('confirmpassword') }}</strong>
				</span>
				@endif
			</div>

	<div class="row">
		<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px;">
				{{ __('Confirm Password') }}
			</button>

			<button type="reset" class="btn btn-danger btn-block btn-flat" style="width: 150px;">
				{{ __('Cancel') }}
			</button>
		</div>
	</div>
	</form>
</div>
</div>
</body>
@endsection