@extends('includes.header1')
@section('content-wrapper')

<body class="register-page"> 
<div class="register-box">
<div class="register-logo">
	<a href="../../index2.html"><b>Change Name</b></a>
</div>

<div class="register-box-body">
	<form action="/settings/name" method="POST" enctype="multipart/form-data" id="addforms">
		@csrf
	<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="New Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

	<div class="row">
		<div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px;">
				{{ __('Confirm New Name') }}
			</button>

			<button type="reset" class="btn btn-danger btn-block btn-flat" style="width: 150px;">
				{{ __('Cancel') }}
			</button>
		</div>
	</div>
	</form>
</div>
</div>
</body>
@endsection