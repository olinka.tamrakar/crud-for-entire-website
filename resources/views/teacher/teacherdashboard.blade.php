@extends('includes.header1')

@section('content-wrapper')
	<center><h1>Hello {{Auth::user()->name}}</h1></center>
<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-blue">
		<div class="inner">
			<h3>
				{{$course}}
			</h3>
			<p>
				My Courses
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-red">
		<div class="inner">
			<h3>
				{{$teacher->specialization}}
			</h3>
			<p>
				Specialization
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-green">
		<div class="inner">
			<h3>
				{{$teacher->experiences}}
			</h3>
			<p>
				Experiences
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-maroon">
		<div class="inner">
			<h3>
				{{$teacher->subjects}}
			</h3>
			<p>
				Subjects
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-yellow">
		<div class="inner">
			<h3>
				{{$teacher->followers}}
			</h3>
			<p>
				Followers
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-aqua">
		<div class="inner">
			<h3>
				{{$teacher->classes}}
			</h3>
			<p>
				Classes
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

@endsection
