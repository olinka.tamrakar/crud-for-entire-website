@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->    
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="font-28 text-white">About Us</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li class="active text-white">Our Team</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>
    
    <!-- Section: Team -->
    <section id="team" class="bg-lighter">
      <div class="container">
        <div class="section-title">
          <div class="row mtli-row-clearfix">
            @foreach($ourteam as $teams)
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                <p class="pull-left flip mb-0 mr-15 mt-5">
                  <img src="/ourteamgallery/{{$teams->photo}}" alt="" height="250px" width="250px">
                </p>
                <div class="team_detail">
                  <h2 class="icon-box-title m-0 mb-5">{{$teams->name}}</h2>
                  <h3 class="m-0 pb-5 mb-5">{{$teams->position}}</h3>
                  <p class="text-gray font-13 mb-0">{{$teams->description}}</p>
                  <!-- <p class="text-gray font-13 mb-10 mt-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequuntur, modi quasi voluptas tempore itaque, error necessitatibus! Ab omnis aperiam dolorum reiciendis voluptas eius, porro aliquam officia distinctio voluptatibus consectetur. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab voluptatem et aut recusandae impedit similique.</p> -->
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  </ul>
                </div>
              </div>    
            </div>
           @endforeach
          </div>
        </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')