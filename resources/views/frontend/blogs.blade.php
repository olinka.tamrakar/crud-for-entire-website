@include('frontend.includes.header') 
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="font-28 text-white">Blogs</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li class="active text-white">Blogs</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-md-9 pull-right flip sm-pull-none">
            <div class="blog-posts">
              <div class="col-md-12 list-dashed">
                <div class="row">
                  @foreach($blog as $blogs)
                    <div class="col-md-6">
                    <article class="post clearfix mb-30 pb-30">
                      <div class="entry-header">
                        <div class="post-thumb thumb"> 
                          <img src="/bloggallery/{{$blogs->photos}}" alt="" class="img-responsive img-fullwidth" height="265px" width="395px"> 
                        </div>
                      </div>
                      <div class="entry-content border-1px p-20 pr-10">
                        <div class="entry-meta media no-bg no-border mt-15 pb-20">
                          <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                            <ul>
                              <li class="font-16 text-white font-weight-600">{{$blogs->created_at->toDateString()}}</li>
                            </ul>
                          </div>
                          <div class="media-body pl-15">
                            <div class="event-content pull-left flip">
                              <h4 class="entry-title text-white text-uppercase m-0"><a href="{{Route('frontend.blogs-detail',$blogs->id)}}">{{$blogs->title}}</a></h4>
                              <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                              <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>
                            </div>
                          </div>
                        </div>
                        <p class="mt-10">{!! $blogs->description !!}</p>
                        <a href="{{Route('frontend.blogs-detail',$blogs->id)}}" class="btn-read-more">Read more</a>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                    </div>
                  @endforeach
                </div>
              </div>
              <div class="col-md-12">
                <nav>
                  <ul class="pagination theme-colored">
                    <li> <a aria-label="Previous" href="#"> <span aria-hidden="true">«</span> </a> </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li> <a aria-label="Next" href="#"> <span aria-hidden="true">»</span> </a> </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div class="col-md-3">
           <div class="sidebar sidebar-right mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Search box</h5>
                <div class="search-form">
                  <form method="POST" action="{{Route('blog.search',null)}}" enctype="multipart/form-data">
                  @csrf
                    <div class="input-group">
                      <input placeholder="Click to Search" class="form-control search-input" type="text" name="keyword" id="keyword">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Categories</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                    @foreach($blogcategory as $blogcategorys)
                      <li><a href="#">{{$blogcategorys->name}}<span>(19)</span></a></li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Archives</h5>
                <ul class="list-divider list-border list check">
                  <li><a href="#">Vehicle Accidents</a></li>
                  <li><a href="#">Family Law</a></li>
                  <li><a href="#">Personal Injury</a></li>
                  <li><a href="#">Personal Injury</a></li>
                  <li><a href="#">Case Investigation</a></li>
                  <li><a href="#">Business Taxation</a></li>
                </ul>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Latest News</h5>
                <div class="latest-posts">
                  @foreach($latestblog as $blogs)
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="{{Route('frontend.blogs-detail',$blogs->id)}}"><img src="https://placehold.it/75x75" alt=""></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="{{Route('frontend.blogs-detail',$blogs->id)}}">{{$blogs->title}}</a></h5>
                      <p>{{$blogs->description}}</p>
                    </div>
                  </article>
                  @endforeach
                </div>
              </div>              
              <div class="widget">
                <h5 class="widget-title line-bottom">Tags</h5>
                <div class="tags">
                  <a href="#">travel</a>
                  <a href="#">blog</a>
                  <a href="#">lifestyle</a>
                  <a href="#">feature</a>
                  <a href="#">mountain</a>
                  <a href="#">design</a>
                  <a href="#">restaurant</a>
                  <a href="#">journey</a>
                  <a href="#">classic</a>
                  <a href="#">sunset</a>
                </div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </section> 
  </div>  
  <!-- end main-content -->

  <!-- Footer -->
@include('frontend.includes.footer')