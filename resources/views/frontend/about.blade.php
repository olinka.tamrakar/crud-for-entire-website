@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->    
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="font-28 text-white">About Us</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li class="active text-white">About Us</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: About -->
    <section class="">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6">
              <h6 class="letter-space-4 text-gray-darkgray text-uppercase mt-0 mb-0">All About</h6>
              <h2 class="text-uppercase font-weight-600 mt-0 font-28 line-bottom">{{$allaboutorg->organization}}</h2>
              <!-- <h4 class="text-theme-colored">Lorem ipsum dolor sit amet soluta saepe odit error, maxime praesentium sunt udiandae!</h4> -->
              <p>
                @for($i=0; $i<80; $i++)
                    {{$description[$i]}}&nbsp
                @endfor
              ....</p>
            </div>
            <div class="col-md-6">
              <img src="/allaboutorggallery/{{$allaboutorg->photo}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: Services -->
    <section id="services" class="bg-lighter">
      <div class="container">
        <div class="section-title">
          <div class="about_tabs">
              <ul id="myTab" class="nav nav-tabs boot-tabs">
                <li class="active"><a class="text-uppercase title" href="#why_us" data-toggle="tab">Why <span class="text-theme-color-2 font-weight-400">Us?</span></a></li>
                <li><a class="text-uppercase title" href="#mission_vision" data-toggle="tab">Mission <span class="text-theme-color-2 font-weight-400">Vision</span></a></li>
                <li><a class="text-uppercase title" href="#work_flow" data-toggle="tab">Work <span class="text-theme-color-2 font-weight-400">Flow</span></a></li>
              </ul>
              <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="why_us">
                @for($i=0; $i<count($whyus) ; $i++)
                @if($i%2 == 0)
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-left flip mb-0 mr-0 mt-5">
                          <img src="/aboutgallery/{{$whyus[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$whyus[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$whyus[$i]->description}}</p>
                        </div>
                      </div>    
                    </div>    
                  </div>
                @else
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-right flip mb-0 mr-0 mt-5">
                          <img src="/aboutgallery/{{$whyus[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$whyus[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$whyus[$i]->description}}</p>
                        </div>
                      </div>    
                    </div>    
                  </div>
                @endif
                @endfor
                </div>

                <div class="tab-pane fade" id="mission_vision">
                @for($i=0; $i<count($mission) ; $i++)
                @if($i%2 == 0)
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-left flip mb-0 mr-0 mt-5">
                          <img src="/missiongallery/{{$mission[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$mission[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$mission[$i]->description}}</p>
                        </div>
                      </div> 
                    </div>
                  </div>
                    @else   
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-right flip mb-0 mr-0 mt-5">
                          <img src="/missiongallery/{{$mission[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$mission[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$whyus->description}}</p>
                        </div>
                      </div>    
                    </div>
                  </div>
                @endif
                @endfor
                </div>

                <div class="tab-pane fade" id="work_flow">
                  @for($i=0; $i<count($workflow) ; $i++)
                  @if($i%2 == 0)
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-left flip mb-0 mr-0 mt-5">
                          <img src="/workflowgallery/{{$workflow[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$workflow[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$workflow[$i]->description}}</p>
                        </div>
                      </div>    
                    </div>
                  </div>
                  @else
                  <div class="row mtli-row-clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      <div class="row iconbox-theme-colored bg-white p-15 border-1px clearfix">
                        <p class="col-md-4 col-sm-4 pull-right flip mb-0 mr-0 mt-5">
                          <img src="/workflowgallery/{{$workflow[$i]->photo}}" alt="">
                        </p>
                        <div class="col-md-8 col-sm-8 icon-box-details pull-left">
                          <h4 class="icon-box-title font-16 font-weight-600 m-0 mb-5">{{$workflow[$i]->title}}</h4>
                          <p class="text-gray font-13 mb-0">{{$workflow[$i]->description}}</p>
                        </div>
                      </div>    
                    </div>
                  </div>
                  @endif
                  @endfor
                </div>
                </div>
              </div>
           </div>
        </div>
    </section>

    <!-- Divider: Call To Action -->
    <section class="bg-theme-color-2">
      <div class="container pt-10 pb-20">
        <div class="row">
          <div class="call-to-action">
            <div class="col-md-6">
              <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="pe-7s-mail mr-10 font-48 vertical-align-middle"></i> SUBSCRIBE TO OUR NEWSLETTER</h3>
            </div>
            <div class="col-md-6">
              <!-- Mailchimp Subscription Form Starts Here -->
              <form id="mailchimp-subscription-form" class="newsletter-form mt-10">
                <div class="input-group">
                  <input type="email" value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer">
                  <span class="input-group-btn">
                    <button data-height="45px" class="btn bg-theme-colored text-white btn-xs m-0 font-14" type="submit">Subscribe</button>
                  </span>
                </div>
              </form>
              <!-- Mailchimp Subscription Form Validation-->
              <script type="text/javascript">
                $('#mailchimp-subscription-form').ajaxChimp({
                    callback: mailChimpCallBack,
                    url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
                });

                function mailChimpCallBack(resp) {
                    // Hide any previous response text
                    var $mailchimpform = $('#mailchimp-subscription-form'),
                        $response = '';
                    $mailchimpform.children(".alert").remove();
                    if (resp.result === 'success') {
                        $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    } else if (resp.result === 'error') {
                        $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                    }
                    $mailchimpform.prepend($response);
                }
              </script>
              <!-- Mailchimp Subscription Form Ends Here -->
            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')