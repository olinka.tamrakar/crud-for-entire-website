 <footer id="footer" class="footer divider layer-overlay overlay-dark-9" data-bg-img="http://via.placeholder.com/1920x1280">
    <div class="container">
      <div class="row border-bottom">
        <div class="col-md-12 col-sm-12">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="row">
                <div class="col-sm-6 col-md-6">
                  <div class="widget dark">
                    <img class="mt-5 mb-20" alt="" src="{{ asset('frontends/images/logo.png')}}">
                    <p>{{$contact->location}}</p>
                    <ul class="list-inline mt-5">
                      <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$contact->number}}</a> </li>
                      <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">{{$contact->email}}</a> </li>
                      <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-color-2 mr-5"></i> <a class="text-gray" href="#">www.yourdomain.com</a> </li>
                      <li class="m-0 pl-10 pr-10 mt-10">
                  <form id="mailchimp-subscription-form-footer" class="newsletter-form" novalidate="true">
                        <div class="">
                            <input value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="35px" id="mce-EMAIL-footer" style="height: 35px;color: #fff" type="email">
                            <div class="mt-10">
                              <button data-height="35px" class="btn bg-theme-color-2 text-white btn-xs m-0 font-14" type="submit" style="height: 40px;width: 100%;">Subscribe</button>
                            </div>
                        </div>
                    </form>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="widget dark">
                    <h4 class="widget-title">Useful Links</h4>
                    <ul class="list angle-double-right list-border">
                      <li><a href="page-about-style1.html">About Us</a></li>
                      <li><a href="page-course-list.html">Our Courses</a></li>
                      <li><a href="page-pricing-style1.html">Pricing Table</a></li>
                      <li><a href="page-gallery-3col.html">Gallery</a></li>
                      <li><a href="shop-category.html">Shop</a></li>              
                    </ul>
                  </div>
                </div>
              </div>
            </div>            
            <div class="col-md-6 col-sm-6">
              <div class="row">
                <div class="col-sm-6 col-md-6">
                  <div class="widget dark">
                    <h4 class="widget-title line-bottom-theme-colored-2">Opening Hours</h4>
                    <div class="opening-hourse">
                      <ul class="list-border">
                        @foreach($openinghour as $openinghours)
                          <li class="clearfix"> <span> {{$openinghours->day}} :  </span>
                            <div class="value pull-right"> {{$openinghours->from}} - {{$openinghours->from}} </div>
                          </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-6">
                  <div class="widget dark">
                    <h4 class="widget-title">Facebook</h4>
                    <div class="fb_wrapper">
                      <div class="fb-page" data-href="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf">It Gamut Enterprises Pvt. Ltd.</a></blockquote></div>
                    </div>
                    <div></div>
                  </div>
                </div>
               </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-black-333">
      <div class="container pt-20 pb-20">
        <div class="row">
          <div class="col-md-6">
            <p class="font-11 text-black-777 m-0">Copyright &copy;2017 Code Gamut. Powered By:<a href="http://itgamut.net/"> IT Gamut Enterprises Pvt. Ltd.</a></p> 
          </div>
          <div class="col-md-6 text-right">
            <div class="widget no-border m-0">
              <ul class="list-inline sm-text-center mt-5 font-12">
                <li>
                  <a href="#">FAQ</a>
                </li>
                <li>|</li>
                <li>
                  <a href="#">Help Desk</a>
                </li>
                <li>|</li>
                <li>
                  <a href="#">Support</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
 </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=220903367995172";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="{{ asset('frontends/js/custom.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
      (Load Extensions only on Local File Systems ! 
       The following part can be removed on Server for On Demand Loading) -->
<!-- <script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script> -->

</body>

</html>