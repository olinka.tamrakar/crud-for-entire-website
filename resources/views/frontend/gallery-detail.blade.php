@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset('frontend/images/bg/bg3.jpg')}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title text-white">Gallery Grid</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li class="active text-white">Album</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="heading-line-bottom">
               <h2 class="heading-title">{{$gallerydetail[0]->name}}</h2>
            </div>
            <!-- Portfolio Gallery Grid -->
            <div class="gallery-isotope grid-5 masonry gutter-small clearfix" data-lightbox="gallery">
              @foreach($gallerydetail as $gallerydetails)
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="/gallerygallery/{{$gallerydetails->photos}}" data-lightbox="gallery-item" title="Title Here 1">
                  <img src="/gallerygallery/{{$gallerydetails->photos}}" alt="">
                </a>
              </div>
              <!-- Portfolio Item End -->
              @endforeach
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item wide">
                <a href="http://placehold.it/800x600" data-lightbox="gallery-item" title="Title Here 2"><img src="http://placehold.it/800x600" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 3"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x600" data-lightbox="gallery-item" title="Title Here 4"><img src="http://placehold.it/400x600" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x600" data-lightbox="gallery-item" title="Title Here 5"><img src="http://placehold.it/400x600" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 6"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 6"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 6"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 6"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <a href="http://placehold.it/400x300" data-lightbox="gallery-item" title="Title Here 6"><img src="http://placehold.it/400x300" alt=""></a>
              </div>
              <!-- Portfolio Item End -->
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')