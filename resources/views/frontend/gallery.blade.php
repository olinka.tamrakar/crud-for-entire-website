@include('frontend.includes.header')  
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title text-white">Gallery</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li class="active text-white">Gallery</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Gallery Grid 3 -->
    <section id="gallery_main">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!-- Portfolio Filter -->
              <div class="portfolio-filter font-alt align-center mb-6 0">
                <a href="#" class="active" data-filter="*">All</a>
                @foreach($gallerycategory as $gallerycategorys)
                  <a href="#{{$gallerycategorys->name}}" class="" 
                     data-filter=".{{$gallerycategorys->name}}">{{$gallerycategorys->name}}</a>
                @endforeach
              </div>
              <!-- End Portfolio Filter -->

              <!-- Portfolio Gallery Grid -->
              <div class="gallery-isotope grid-3 gutter-small clearfix" data-lightbox="gallery">

              @foreach($gallery as $gallerys)
                <!-- Portfolio Item Start -->
                <div class="gallery-item {{$gallerys->name}}">
                  <div class="thumb">
                    <img class="img-fullwidth" src="/gallerygallery/{{$gallerys->photos}}" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder">
                      <h3 class="title text-center">{{$gallerys->photos}}</h3>
                      <a href="{{Route('frontend.gallery-detail',$gallerys->id)}}" class="gallery_link" title="Your Title Here"></a>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
              @endforeach
              </div>
              <!-- End Portfolio Gallery Grid -->

            </div>
          </div>
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')