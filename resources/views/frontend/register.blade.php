@include('frontend.includes.header')
<div class="content-wrapper" style="min-height: 800px;">
	<body class="register-page">
		<div class="register-box">
			<div class="register-box-body">
				<h4 class="widget-title line-bottom">Register <span class="text-theme-color-2">Now</span></h4>
				<form method="POST" action="{{Route('student.register')}}" aria-label="{{ __('Register') }}" enctype="multipart/form-data" id="addforms">
					@csrf
					<div class="form-group has-feedback">
						<input id="name" type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('name'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="phone" type="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" placeholder="Phone" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('phone'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('phone') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" placeholder="Address" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('address'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('address') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" placeholder="Password" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="gender" type="gender" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" value="{{ old('gender') }}" placeholder="Gender" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('gender'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('gender') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback">
						<input id="dateofbirth" type="dateofbirth" class="form-control{{ $errors->has('dateofbirth') ? ' is-invalid' : '' }}" name="dateofbirth" value="{{ old('dateofbirth') }}" placeholder="Date of Birth" required autofocus>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						@if ($errors->has('dateofbirth'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('dateofbirth') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group has-feedback" title="Upload teacher's photo">
						<input type="file" name="photo" id="photo" required autofocus>
						@if ($errors->has('photo'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('photo') }}</strong>
						</span>
						@endif
					</div>

					<input type="hidden" name="role" value="Student">
					<input type="hidden" name="status" value="Active">

					<div class="form-group">
						<input name="form_botcheck" class="form-control" type="hidden" value="" />
						<button type="submit" class="btn btn-theme-colored btn-flat btn-xs btn-quick-contact text-white pt-5 pb-5" data-loading-text="Please wait...">Register</button>

						<button onclick ="goback()" class="btn btn-theme-colored btn-flat btn-xs btn-quick-contact text-white pt-5 pb-5" class="btn btn-primary btn-block btn-flatdata-loading-text="Please wait...">
							{{ __('Cancel') }}
						</button>
					</div>
				</form>
			</div>
		</div>
	</body>
</div>
@include('frontend.includes.footer')