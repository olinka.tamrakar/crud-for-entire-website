@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset('frontend/images/bg/bg3.jpg')}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="font-28 text-white">Courses</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="course.php">Courses</a></li>
                <li class="active text-white">Course Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: Blog -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="single-service">
              <img src="/coursegallery/{{$course->photos}}" alt="">
              <h3 class="text-theme-colored line-bottom text-theme-colored">{{$course->name}}</h3>
              <h4 class="mt-0"><span class="text-theme-color-2">Price :</span> ${{$course->price}}</h4>
                <ul class="review_text list-inline">
                  <li>
                    <div class="star-rating" title="Rated 4.50 out of 5"><span style="width: 90%;">4.50</span></div>
                  </li>
                </ul>
              {{$course->features}}
              <h4 class="line-bottom mt-20 mb-20 text-theme-colored">Key Features</h4>
              <div class="key_features">
                  <ul class="list-border">

                    <li class="clearfix"> <span class="pull-left mr-10"><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                      <div class="value pull-left">{{$course->features}}</div>
                    </li>
                  </ul>
              </div>
                <h4 class="line-bottom mt-20 mb-20 text-theme-colored">Key Features</h4>
               <div class="course_tabs">
                  <ul id="myTab" class="nav nav-tabs boot-tabs">
                    <li class="active"><a href="#syllabus" data-toggle="tab">Syllabus</a></li>
                    <!-- <li><a href="#large" data-toggle="tab">Categories</a></li>
                    <li><a href="#faqs_course" data-toggle="tab">FAQ's</a></li> -->
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade in active" id="syllabus">
                      <table class="table table-bordered table-striped table-hover"> 
                        <tr>
                          <td class="text-center font-16 font-weight-600 bg-theme-color-2 text-white" colspan="4">Courses Syllabus</td>
                        </tr>
                        <tr> <th>Title</th> <th>Class Time</th> <th>Estimate Time</th> <th>Course Duration</th> </tr>
                        <tbody> 
                          <tr> 
                            <th scope="row">{{$syllabus->id}}. {{$syllabus->syllabusname}}</th> 
                            <td>{{$syllabus->classtime}}</td> 
                            <td>{{$syllabus->estimatetime}}</td> 
                            <td>{{$syllabus->courseduration}}</td> 
                          </tr>
                        </tbody> 
                      </table>
                    </div>
                    <!-- <div class="tab-pane fade" id="large">
                      <table class="table table-bordered"> 
                        <tr>
                          <td class="text-center font-16 font-weight-600 bg-theme-color-2 text-white" colspan="4">Prices For All Lesson Type</td>
                        </tr>
                        <tr> <th>Coures Type</th> <th>Class time</th> <th>Course Duration</th> <th>Price</th> </tr>
                        <tbody> 
                          <tr> <th scope="row">Applied Psychology</th> <td>45 minutes</td> <td>3 years</td> <td>$810</td> </tr>
                          <tr> <th scope="row">Business Administration (MBA)</th> <td>45 minutes</td> <td>2 years</td> <td>$940</td> </tr>
                          <tr> <th scope="row">Computer Science (BSc)</th> <td>1 Hours</td> <td>4 years</td> <td>$1180</td> </tr>
                          <tr> <th scope="row">Development Studies (MDS)</th> <td>1 Hours</td> <td>5 years</td> <td>$1400</td> </tr> 
                          <tr> <th scope="row">Engineering Technology (BSc)</th> <td>30 minutes</td> <td>3 years</td> <td>$600</td> </tr> 
                        </tbody> 
                      </table>
                    </div>
                    <div class="tab-pane fade" id="faqs_course">
                      <div id="accordion_course" class="panel-group accordion">
                        <div class="panel">
                          <div class="panel-title"> <a class="active" data-parent="#accordion1" data-toggle="collapse" href="#accordion11" aria-expanded="true"> <span class="open-sub"></span> Why this Company is Best?</a> </div>
                          <div id="accordion11" class="panel-collapse collapse in" role="tablist" aria-expanded="true">
                            <div class="panel-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit quae repellendus provident dolor iure poss imusven am aliquam. Officiis totam ea laborum deser unt vonsess.  iure poss imusven am aliquam</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel">
                          <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordion12" class="" aria-expanded="true"> <span class="open-sub"></span> Why this Company is Best?</a> </div>
                          <div id="accordion12" class="panel-collapse collapse" role="tablist" aria-expanded="true">
                            <div class="panel-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit quae repellendus provident dolor iure poss imusven am aliquam. Officiis totam ea laborum deser unt vonsess.  iure poss imusven am aliquam</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel">
                          <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordion13" class="" aria-expanded="true"> <span class="open-sub"></span> Why this Company is Best?</a> </div>
                          <div id="accordion13" class="panel-collapse collapse" role="tablist" aria-expanded="true">
                            <div class="panel-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit quae repellendus provident dolor iure poss imusven am aliquam. Officiis totam ea laborum deser unt vonsess.  iure poss imusven am aliquam</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel">
                          <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordion14" class="" aria-expanded="true"> <span class="open-sub"></span> Why this Company is Best?</a> </div>
                          <div id="accordion14" class="panel-collapse collapse" role="tablist" aria-expanded="true">
                            <div class="panel-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit quae repellendus provident dolor iure poss imusven am aliquam. Officiis totam ea laborum deser unt vonsess.  iure poss imusven am aliquam</p>
                            </div>
                          </div>
                        </div>
                        <div class="panel">
                          <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse" href="#accordion15" class="" aria-expanded="true"> <span class="open-sub"></span> Why this Company is Best?</a> </div>
                          <div id="accordion15" class="panel-collapse collapse" role="tablist" aria-expanded="true">
                            <div class="panel-content">
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore impedit quae repellendus provident dolor iure poss imusven am aliquam. Officiis totam ea laborum deser unt vonsess.  iure poss imusven am aliquam</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> -->
                  </div>
               </div>
               <h4 class="line-bottom mt-20 mb-20 text-theme-colored">Related Courses</h4>
               <div class="rel_courses">
                 <div class="row">
                  @foreach($relatedcourse as $relatedcourses)
                  @if($relatedcourses->id != $course->id)
                   <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 mt-xs-10">
                      <div class="team-thumb">
                        <img class="img-fullwidth" alt="" src="/coursegallery/screencapture-localhost-8000-institute-view-2019-01-03-23_50_23.jpg">
                      </div>
                      <div class="team-bottom-part border-bottom-theme-color-2-2px bg-lighter border-1px text-center p-10 pb-10">
                        <h4 class="text-uppercase font-raleway font-weight-600"><a class="text-theme-color-2" href="{{Route('frontend.course-detail',$relatedcourses->id)}}">{{$relatedcourses->name}}</a></h4>
                        <p>{{$relatedcourses->description}}</p>
                      </div>
                    </div>
                  @endif
                  @endforeach
                 </div>
               </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="sidebar sidebar-left mt-sm-30 ml-40">
              <div class="widget">
                <h4 class="widget-title line-bottom">Courses <span class="text-theme-color-2">List</span></h4>
                <div class="services-list">
                  @foreach($courseall as $coursealls)
                  <ul class="list list-border angle-double-right">
                    @if($course->id == $coursealls->id)
                      <li class="active"><a href="javascript:void(0)">{{$course->name}}</a></li>
                    @else
                      <li><a href="#">{{$coursealls->name}}</a></li>
                    @endif
                  </ul>
                  @endforeach
                </div>
              </div>
              <div class="widget">
                <h4 class="widget-title line-bottom">What You<span class="text-theme-color-2"> Get</span></h4>
                <div class="opening-hourse">
                  <ul class="list-border">
                    <li class="clearfix"> <span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                      <div class="value pull-right">Educated Staff</div>
                    </li>
                    <li class="clearfix"> <span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                      <div class="value pull-right">Timesheets</div>
                    </li>
                    <li class="clearfix"> <span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                      <div class="value pull-right">Video Lessons</div>
                    </li>
                    <li class="clearfix"> <span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                      <div class="value pull-right">Video Lessons</div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h4 class="widget-title line-bottom">Teachers</h4>
                @foreach($teacher as $teachers)
                <div class="team-thumb">
                  <img class="img-fullwidth" alt="" src="/teachergallery/{{$teachers->file}}">
                </div>
                <div class="team-bottom-part border-bottom-theme-color-2-2px bg-lighter border-1px text-center p-10 pt-20 pb-10">
                  <h4 class="text-uppercase font-raleway font-weight-600 m-0"><a class="text-theme-color-2" href="page-teachers-details.html">{{$teachers->name}}</a></h4>
                  <h5 class="text-theme-color">{{$teachers->specialization}}</h5>
                  <ul class="styled-icons icon-sm icon-dark icon-theme-colored">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                  </ul>
                </div>
                @endforeach

              </div>
              <div class="widget">
                <h4 class="widget-title line-bottom">Enroll <span class="text-theme-color-2">Now</span></h4>
                <form id="quick_contact_form_sidebar" name="footer_quick_contact_form" class="quick-contact-form" action="#" method="post">
                  <div class="form-group">
                    <input name="form_name" class="form-control" type="text" required="" placeholder="Full Name">
                  </div>
                  <div class="form-group">
                    <input name="form_phone" class="form-control" type="text" required="" placeholder="Your Phone">
                  </div>
                  <div class="form-group">
                    <input name="form_email" class="form-control" type="text" required="" placeholder="Enter Email">
                  </div>
                  <div class="form-group">
                    <textarea name="form_message" class="form-control" required="" placeholder="Enter Message" rows="3"></textarea>
                  </div>
                  <div class="form-group">
                    <input name="form_botcheck" class="form-control" type="hidden" value="" />
                    <button type="submit" class="btn btn-theme-colored btn-flat btn-xs btn-quick-contact text-white pt-5 pb-5" data-loading-text="Please wait...">Send Message</button>
                  </div>
                </form>

                <!-- Quick Contact Form Validation-->
                <script type="text/javascript">
                  $("#quick_contact_form_sidebar").validate({
                    submitHandler: function(form) {
                      var form_btn = $(form).find('button[type="submit"]');
                      var form_result_div = '#form-result';
                      $(form_result_div).remove();
                      form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                      var form_btn_old_msg = form_btn.html();
                      form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                      $(form).ajaxSubmit({
                        dataType:  'json',
                        success: function(data) {
                          if( data.status == 'true' ) {
                            $(form).find('.form-control').val('');
                          }
                          form_btn.prop('disabled', false).html(form_btn_old_msg);
                          $(form_result_div).html(data.message).fadeIn('slow');
                          setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                        }
                      });
                    }
                  });
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')