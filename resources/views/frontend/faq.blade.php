@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title text-white">FAQ'S</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li class="active text-white">FAQ'S</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          @foreach($faq as $faqs)
          <div class="col-md-10 col-md-push-1">
            <div class="list-group">
              <a href="#section-one" class="list-group-item smooth-scroll-to-target">{{$faqs->question}}</a>
            </div>
          </div>
          <div class="col-md-10 col-md-push-1">
            <div id="section-one" class="mb-50">
              <h3>{{$faqs->question}}</h3>
              <hr>
              <p class="mb-20">{{$faqs->answer}}</p>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')