@include('frontend.includes.header')
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="font-28 text-white">Blogs</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="index.php">Home</a></li>
                <li><a href="blogs.php">Blogs</a></li>
                <li class="active text-white">Blogs Detail</li>
              </ol>
            </div>
          </div>
        </div>
      </div>      
    </section>

    <!-- Section: Blog -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-9 blog-pull-right">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header">
                  <div class="post-thumb thumb"> <img src="{{ asset("bloggallery/{$blog->photos}")}}" alt="" class="img-responsive img-fullwidth"> </div>
                </div>  
                <div class="entry-title pt-10 pl-15">
                  <h4><a class="text-uppercase" href="#">{{$blog->title}}</a></h4>
                </div>
                <div class="entry-meta pl-15">
                  <ul class="list-inline">
                    <li>Posted: <span class="text-theme-color-2">{{$blog->created_at}}</span></li>
                    <li>By: <span class="text-theme-color-2">Admin</span></li>
                    <li><i class="fa fa-comments-o ml-5 mr-5"></i> 5 comments</li>
                  </ul>
                </div>
                <div class="entry-content mt-10">
                  <p class="mb-15">{!! $blog->description !!}</p>
<!--                   <blockquote class="theme-colored pt-20 pb-20">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
                  </blockquote>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> -->
                  <div class="mt-30 mb-0">
                    <h5 class="pull-left mt-10 mr-20 text-theme-color-2">Share:</h5>
                    <ul class="styled-icons icon-circled m-0">
                      <li><a href="#" data-bg-color="#3A5795"><i class="fa fa-facebook text-white"></i></a></li>
                      <li><a href="#" data-bg-color="#55ACEE"><i class="fa fa-twitter text-white"></i></a></li>
                      <li><a href="#" data-bg-color="#A11312"><i class="fa fa-google-plus text-white"></i></a></li>
                    </ul>
                  </div>
                </div>
              </article>
              <div class="tagline p-0 pt-20 mt-5">
                <div class="row">
                  <div class="col-md-8">
                    <div class="tags">
                      <p class="mb-0"><i class="fa fa-tags text-theme-color-2"></i> <span>Tags:</span> Engine, Wheel, Oil, Brake</p>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="share text-right">
                      <p><i class="fa fa-share-alt text-theme-color-2"></i> Share</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="author-details media-post">
                <a href="#" class="post-thumb mb-0 pull-left flip pr-20"><img class="img-thumbnail" alt="" src="{{ asset('frontend/images/blog/author.jpg')}}"></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-0"><a href="#" class="font-18">John Doe</a></h5>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                  <ul class="styled-icons square-sm m-0">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                  </ul>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="comments-area">
                <h5 class="comments-title">Comments</h5>
                <ul class="comment-list">
                  <li>
                    <div class="media comment-author"> <a class="media-left pull-left flip" href="#"><img class="img-thumbnail" src="{{ asset('frontend/images/blog/comment1.jpg')}}" alt=""></a>
                      <div class="media-body">
                        <h5 class="media-heading comment-heading">John Doe says:</h5>
                        <div class="comment-date">23/06/2014</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ea commodo consequat...</p>
                        <a class="replay-icon pull-right text-theme-colored" href="#"> <i class="fa fa-reply text-theme-colored"></i> Replay</a> </div>
                    </div>
                  </li>
                  <li>
                    <div class="media comment-author"> <a class="media-left pull-left flip" href="#"><img class="img-thumbnail" src="{{ asset('frontend/images/blog/comment2.jpg')}}" alt=""></a>
                      <div class="media-body">
                        <h5 class="media-heading comment-heading">John Doe says:</h5>
                        <div class="comment-date">23/06/2014</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ea commodo consequat...</p>
                        <a class="replay-icon pull-right text-theme-colored" href="#"> <i class="fa fa-reply text-theme-colored"></i> Replay</a>
                        <div class="clearfix"></div>
                        <div class="media comment-author nested-comment"> <a href="#" class="media-left pull-left flip pt-20"><img alt="" src="{{ asset('frontend/images/blog/comment3.jpg')}}" class="img-thumbnail"></a>
                          <div class="media-body p-20 bg-lighter">
                            <h5 class="media-heading comment-heading">John Doe says:</h5>
                            <div class="comment-date">23/06/2014</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ea commodo consequat...</p>
                            <a class="replay-icon pull-right text-theme-colored" href="#"> <i class="fa fa-reply text-theme-colored"></i> Replay</a>
                          </div>
                        </div>
                        <div class="media comment-author nested-comment"> <a href="#" class="media-left pull-left flip pt-20"><img alt="" src="{{ asset('frontend/images/blog/comment1.jpg')}}" class="img-thumbnail"></a>
                          <div class="media-body p-20 bg-lighter">
                            <h5 class="media-heading comment-heading">John Doe says:</h5>
                            <div class="comment-date">23/06/2014</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ea commodo consequat...</p>
                            <a class="replay-icon pull-right text-theme-colored" href="#"> <i class="fa fa-reply text-theme-colored"></i> Replay</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media comment-author"> <a class="media-left pull-left flip" href="#"><img class="img-thumbnail" src="{{ asset('frontend/images/blog/comment2.jpg')}}" alt=""></a>
                      <div class="media-body">
                        <h5 class="media-heading comment-heading">John Doe says:</h5>
                        <div class="comment-date">23/06/2014</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna et sed aliqua. Ut enim ea commodo consequat...</p>
                        <a class="replay-icon pull-right text-theme-colored" href="#"> <i class="fa fa-reply text-theme-colored"></i> Replay</a> </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="comment-box">
                <div class="row">
                  <div class="col-sm-12">
                    <h5>Leave a Comment</h5>
                    <div class="row">
                      <form role="form" id="comment-form">
                        <div class="col-sm-6 pt-0 pb-0">
                          <div class="form-group">
                            <input type="text" class="form-control" required name="contact_name" id="contact_name" placeholder="Enter Name">
                          </div>
                          <div class="form-group">
                            <input type="text" required class="form-control" name="contact_email2" id="contact_email2" placeholder="Enter Email">
                          </div>
                          <div class="form-group">
                            <input type="text" placeholder="Enter Website" required class="form-control" name="subject">
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <textarea class="form-control" required name="contact_message2" id="contact_message2"  placeholder="Enter Message" rows="7"></textarea>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-dark btn-flat pull-right m-0" data-loading-text="Please wait...">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-3">
            <div class="sidebar sidebar-left mt-sm-30">
              <div class="widget">
                <h5 class="widget-title line-bottom">Search box</h5>
                <div class="search-form">
                  <form>
                    <div class="input-group">
                      <input type="text" placeholder="Click to Search" class="form-control search-input">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Categories</h5>
                <div class="categories">
                  <ul class="list list-border angle-double-right">
                    <li><a href="#">Creative<span>(19)</span></a></li>
                    <li><a href="#">Portfolio<span>(21)</span></a></li>
                    <li><a href="#">Fitness<span>(15)</span></a></li>
                    <li><a href="#">Gym<span>(35)</span></a></li>
                    <li><a href="#">Personal<span>(16)</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Archives</h5>
                <ul class="list-divider list-border list check">
                  <li><a href="#">Vehicle Accidents</a></li>
                  <li><a href="#">Family Law</a></li>
                  <li><a href="#">Personal Injury</a></li>
                  <li><a href="#">Personal Injury</a></li>
                  <li><a href="#">Case Investigation</a></li>
                  <li><a href="#">Business Taxation</a></li>
                </ul>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Latest News</h5>
                <div class="latest-posts">
                  @foreach($latestblog as $blogs)
                  <article class="post media-post clearfix pb-0 mb-10">
                    <a class="post-thumb" href="{{Route('frontend.blogs-detail',$blogs->id)}}"><img src="/bloggallery/{{$blogs->photos}}" alt="" height="50px" width="50px"></a>
                    <div class="post-right">
                      <h5 class="post-title mt-0"><a href="{{Route('frontend.blogs-detail',$blogs->id)}}">{{$blogs->title}}</a></h5>
                      <p>{!! $blogs->description !!}</p>
                    </div>
                  </article>
                  @endforeach
                </div>
              </div>
              <div class="widget">
                <h5 class="widget-title line-bottom">Facebook Updates</h5>
                <div>                  
                  <div class="fb-page" data-href="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/itgamutenterprises/?hc_ref=ARQkGUuPcDhgiSit-AEMJfMre2HqEKpxnAN-mmNvBm7F0sKtIMQ1Q1LjrfJIo1tF4WM&amp;fref=nf">It Gamut Enterprises Pvt. Ltd.</a></blockquote></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
@include('frontend.includes.footer')