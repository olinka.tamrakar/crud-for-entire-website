@include('frontend.includes.header')
<!-- Start main-content -->
<div class="main-content">

  <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="font-28 text-white">Teachers</h2>
            <ol class="breadcrumb text-center text-black mt-10">
              <li><a href="index.php">Home</a></li>
              <li class="active text-white">Teachers</li>
            </ol>
          </div>
        </div>
      </div>
    </div>      
  </section>

  <!-- Section: Team -->
  <section id="team">
    <div class="container">
      <div class="row mtli-row-clearfix">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="sidebar sidebar-left mt-sm-30">
            <div class="widget">
              <h5 class="widget-title line-bottom">Find <span class="text-theme-color-2">Teacher</span></h5>
              <div class="search-form">
                <form method="POST" action="{{Route('teacher.search')}}" enctype="multipart/form-data">
                  @csrf
                  <div class="input-group">
                    <input type="text" placeholder="Click to Search" class="form-control search-input" name="keyword" id="keyword">
                    <span class="input-group-btn">
                      <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
            <div class="widget">
              <h5 class="widget-title line-bottom">Course <span class="text-theme-color-2">Categories</span></h5>
              <div class="categories">
                <ul class="list list-border angle-double-right">
                  @foreach($coursecategory as $coursecategorys)
                  <li><a href="{{Route('category.search',$coursecategorys->id)}}">{{$coursecategorys->name}} <span>(19)</span></a></li>
                  @endforeach
                </ul>
              </div>
            </div>
            <div class="widget">
              <h5 class="widget-title line-bottom">Latest <span class="text-theme-color-2">Course</span></h5>
              <div class="latest-posts">
                @foreach($latestcourse as $latestcourses)
                <article class="post media-post clearfix pb-0 mb-10">
                  <a class="post-thumb" href="#"><img src="{{ asset("/coursegallery/{$latestcourses->photos}")}}" alt="" height="75px" width="75px"></a>
                  <div class="post-right">
                    <h5 class="post-title mt-0"><a href="#">{{$latestcourses->name}}</a></h5>
                    <p>{{$latestcourses->description}}</p>
                  </div>
                </article>
                @endforeach
              </div>
            </div>
            <div class="widget">
              <h5 class="widget-title line-bottom">Photos <span class="text-theme-color-2">from Flickr</span></h5>
              <div id="flickr-feed" class="clearfix">
              </div>
            </div>
          </div>            
        </div>
        <div class="col-xs-12 col-sm-6 col-md-9">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 sm-text-center mb-30 mb-sm-30">
              @foreach($teacher as $teachers)
              <div class="team-members maxwidth400 teacher_main">
                <div class="team-thumb">
                  <img class="img-fullwidth" alt="" src="/teachergallery/{{$teachers->file}}" height="260px" width="200px">
                </div>
                <div class="team-bottom-part border-bottom-theme-color-2-2px bg-lighter border-1px text-center p-10 pt-20 pb-10">
                  <h4 class="text-uppercase font-raleway font-weight-600 m-0"><a class="text-theme-color-2" href="{{Route('frontend.teachers-detail')}}"> {{$teachers->name}}</a></h4>
                  <h5 class="text-theme-color">Teacher - {{$teachers->specialization}}</h5>
                  <p>{{$teachers->description}}</p>
                <ul class="styled-icons icon-sm icon-dark icon-theme-colored">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>          
    </div>
    <div class="row">
      <div class="col-sm-12">
        <nav>
          <ul class="pagination theme-colored xs-pull-center m-0">
            <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">...</a></li>
            <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</section>

</div>
<!-- end main-content -->

<!-- Footer -->
@include('frontend.includes.footer')