@include('frontend.includes.header')
<!-- Start main-content -->
<div class="main-content">
  <!-- Section: inner-header -->
  <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="{{ asset("pageimagegallery/{$pageimage->photo}")}}">
    <div class="container pt-60 pb-60">
      <!-- Section Content -->
      <div class="section-content">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="font-28 text-white">Courses</h2>
            <ol class="breadcrumb text-center text-black mt-10">
              <li><a href="index.php">Home</a></li>
              <li class="active text-white">Courses</li>
            </ol>
          </div>
        </div>
      </div>
    </div>      
  </section>

  <!-- Section: Course gird -->
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-9 blog-pull-right">
          <div class="row">
            <div class="col-md-12 course_main course_grid">
              <div class="short_view mb-10 pb-5">
                <p>
                  <span>Short View</span>
                  <a class="pull-right short_view_list" href="javascript:void(0);" title="List View">
                    <i class="fa fa-list" aria-hidden="true"></i>
                  </a>
                  <a class="pull-right mr-10 short_view_grid" href="javascript:void(0);" title="Grid View">
                    <i class="fa fa-th" aria-hidden="true"></i></a>
                  </p>
                </div>
                <div class="row">
                  @foreach($course as $courses)
                  <div class="col-sm-6 col-md-4">
                    <div class="service-block bg-white">
                      <div class="thumb"> <img alt="featured project" src="/coursegallery/{{$courses->photos}}" class="img-fullwidth">
                        <h4 class="text-white mt-0 mb-0"><span class="price">{{$courses->price}}</span></h4>
                      </div>
                      <div class="content text-left flip p-25 pt-0">
                        <h4 class="line-bottom mb-10">{{$courses->name}}</h4>
                        <p>{{$courses->features}}</p>
                        <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{Route('frontend.course-detail',$courses->id)}}">view details</a>
                      </div>
                    </div>
                  </div>
                  @endforeach
                </div>
              </div>
              <div class="col-md-12 course_main course_list" style="display: none;">
                <div class="short_view mb-10 pb-5">
                  <p>
                    <span>Short View</span>
                    <a class="pull-right ml-10 short_view_grid" href="javascript:void(0);" title="Grid View">
                      <i class="fa fa-th" aria-hidden="true"></i></a>
                      <a class="pull-right short_view_list" href="javascript:void(0);" title="List View">
                        <i class="fa fa-list" aria-hidden="true"></i>
                      </a>
                    </p>
                  </div>

                  @foreach($course as $courses)
                  <div class="row mb-15 service-list">
                    <div class="col-sm-6 col-md-4">
                      <div class="thumb"> <img alt="featured project" src="/coursegallery/{{$courses->photos}}" class="img-fullwidth"></div>
                    </div>
                    <div class="col-sm-6 col-md-8">
                      <h4 class="line-bottom mt-0 mt-sm-20">{{$courses->name}}</h4>
                      <ul class="review_text list-inline">
                        <li><h4 class="mt-0"><span class="text-theme-color-2">Price :</span> {{$courses->price}}</h4></li>
                        <li>
                          <div class="star-rating" title="Rated 4.50 out of 5"><span style="width: 90%;">4.50</span></div>
                        </li>
                      </ul>
                      <p>{{$courses->features}}</p>
                      <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="{{Route('frontend.course-detail',$courses->id)}}">view details</a>
                    </div>
                  </div>
                  <hr>
                  @endforeach
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3">
              <div class="sidebar sidebar-left mt-sm-30">
                <div class="widget">
                  <h5 class="widget-title line-bottom">Search <span class="text-theme-color-2">Courses</span></h5>
                  <div class="search-form">
                    <form method="post" action="{{Route('course.search')}}" enctype="multipart/form-data">
                      @csrf
                      <div class="input-group">
                        <input type="text" placeholder="Click to Search" class="form-control search-input" name="keyword" id="keyword">
                        <span class="input-group-btn">
                          <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="widget">
                  <h5 class="widget-title line-bottom">Course <span class="text-theme-color-2">Categories</span></h5>
                  <div class="categories">
                    <ul class="list list-border angle-double-right">
                      @foreach($coursecategories as $coursecategory)
                      <li><a href="{{Route('category.search',$coursecategory->id)}}">{{$coursecategory->name}}<span>(19)</span></a></li>
                      @endforeach
                    </ul>
                  </div>
                </div>
                <div class="widget">
                  <h5 class="widget-title line-bottom">Latest <span class="text-theme-color-2">Course</span></h5>
                  <div class="latest-posts">
                    @foreach($latestcourse as $course)
                    <article class="post media-post clearfix pb-0 mb-10">
                      <a class="post-thumb" href="#"><img src="{{ asset("/coursegallery/$course->photos")}}" alt="" style="width: 75px; height: 75px;"></a>
                        <div class="post-right">
                          <h5 class="post-title mt-0"><a href="#">{{$course->name}}</a></h5>
                          <p>{{$course->description}}</p>
                        </div>
                      </article>
                      @endforeach
                    </div>
                  </div>
                  <div class="widget">
                    <h5 class="widget-title line-bottom">Photos <span class="text-theme-color-2">from Flickr</span></h5>
                    <div id="flickr-feed" class="clearfix">
                  <!-- Flickr Link 
                  <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08">
                  </script>-->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <nav>
              <ul class="pagination theme-colored xs-pull-center m-0">
                <li> <a href="#" aria-label="Previous"> <span aria-hidden="true">«</span> </a> </li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">...</a></li>
                <li> <a href="#" aria-label="Next"> <span aria-hidden="true">»</span> </a> </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  @include('frontend.includes.footer')