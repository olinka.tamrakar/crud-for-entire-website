@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Teacher's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('courses.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('course.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Course') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                </div>
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                    </tr>

                    <tr>
                    	@foreach($course as $courses) 
                    				<td>{{$courses->id}}</td>
                            <td>{{$courses->name}}</td>             
                    </tr>
                    	@endforeach
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
