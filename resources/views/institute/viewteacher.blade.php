@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Teacher's List</h3>
                   <div class="box-tools">
                    @can('teachers.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('gallery.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Teacher') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Address</th>
                      <th>Phone</th>
                      <th>Email</th>
                    </tr>
                    </thead>
                    
                    <tbody>
                    <tr>
                    	@foreach($teacher as $teachers) 
                    				<td>{{$teachers->id}}</td>
                            <td>{{$teachers->name}}</td>             
                            <td>{{$teachers->address}}</td>             
                            <td>{{$teachers->phone}}</td>             
                            <td>{{$teachers->email}}</td>             
                    </tr>
                    	@endforeach
                    </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
