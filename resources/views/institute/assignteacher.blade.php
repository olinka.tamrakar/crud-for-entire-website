@extends('includes.header1')

@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">

<div class="register-logo">
	<a href="../../index2.html"><b>Assign Teacher</b></a>
</div>

<div class="register-box-body">
<form action="/institute/assignteacher" method="POST" enctype="multipart/form-data" id = "addforms">	
	@csrf
	<div class="form-group has-feedback">
		<b>Select the teachers</b> <br><br>
		@foreach($teachers as $teacher)
		<input id="teachers" type="checkbox" value="{{$teacher->id}}" class="form-control{{ $errors->has('teachers') ? ' is-invalid' : '' }}" name="teachers[]" required autofocus> {{$teacher->name}}<br>
		@endforeach

		@if ($errors->has('teachers'))
		<span class="invalid-feedback" role="alert">
			<strong>{{ $errors->first('teachers') }}</strong>
		</span>
		@endif
	</div>
	<div class="row">
			<div class="col-xs-6">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Assign Teacher') }}
				</button>
			</div>
		</div>
	</form>
</div>
</div>
</body>
@endsection
