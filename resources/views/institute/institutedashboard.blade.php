@extends('includes.header1')

@section('content-wrapper')
	
	<center><h1>Hello {{Auth::user()->name}}</h1></center>
<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-green">
		<div class="inner">
			<h3>
				{{$teacher}}
			</h3>
			<p>
				My Teachers
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-person-add"></i>
		</div>
	</div>
</div><!-- ./col -->

<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-green">
		<div class="inner">
			<h3>
				{{$course}}
			</h3>
			<p>
				My Courses
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
</div><!-- ./col -->

@endsection
