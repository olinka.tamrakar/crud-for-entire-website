@extends('includes.header1')

@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">

<div class="register-logo">
	<a href="../../index2.html"><b>Assign Course</b></a>
</div>
<div class="register-box-body">
<form action="/institute/assigncourse" method="POST" enctype="multipart/form-data" id = "addforms">	
		 @csrf	
			<div class="form-group has-feedback">
				Select the courses <br>
			@foreach($courses as $course)
				<input id="courses" type="checkbox" value="{{$course->id}}" class="form-control{{ $errors->has('courses') ? ' is-invalid' : '' }}" name="courses[]" required autofocus> {{$course->name}}
			@endforeach
				
				@if ($errors->has('courses'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('courses') }}</strong>
				</span>
				@endif
			</div>
			<div class="row">
			<div class="col-xs-6">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Assign Course') }}
				</button>
			</div>
		</div>
		</div>
</form>
</div>
</body>
@endsection
