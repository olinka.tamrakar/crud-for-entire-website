@extends('includes.header1')
@section('content-wrapper')
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Log Activity Lists</h3>
                </div><!-- /.box-header -->
                <form action="{{Route('filter')}}" method="POST">
                	@csrf
                	<p align="center">
                    <select name="title" id="title">
                      <option value="">Select any title</option>
                      <option value="blog">Blog</option>
                      <option value="category">Category</option>
                      <option value="coursecategory">Course Category</option>
                      <option value="course">Course</option>
                      <option value="editor">Editor</option>
                      <option value="event">Event</option>
                      <option value="gallery">Gallery</option>
                      <option value="institute">Institute</option>
                      <option value="settings">Settings</option>
                      <option value="teacher">Teacher</option>
                    </select>

                    <select name="action" id="action">
                      <option value="">Select any action</option>
                      <option value="added">Added</option>
                      <option value="updated">Updated</option>
                      <option value="deleted">Deleted</option>
                      <option value="activated">Activated</option>
                      <option value="deactivated">Deactivated</option>
                      <option value="password changed">Password Changed</option>
                    </select>
                	<input type="submit" name="submit" value="GO"></p>
                </form>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      	<th>S.No</th>
                        <th>Date</th>
                        <th>Time</th>
                      	<th>Subject</th>
                      	<th>URL</th>
                      	<th>Method</th>
                      	<th>Ip</th>
                      	<th width="300px">User Agent</th>
                      	<th>User Id</th>
                      </tr>
                    </thead>
                    <tbody>
                    	@if($logs->count())
                    	@foreach($logs as $key => $log)
                      @for($i=0; $i < $logs->count(); $i++) 
                    	<tr>
                    		<td>{{ ++$key }}</td>
                        <td>{{$date[$i]}}</td>
                        <td>{{$time[$i]}}</td>
                    		<td>{{ $log->subject }}</td>
                    		<td class="text-success">{{ $log->url }}</td>
                    		<td><label class="label label-info">{{ $log->method }}</label></td>
                    		<td class="text-warning">{{ $log->ip }}</td>
                    		<td class="text-danger">{{ $log->agent }}</td>
                    		<td>{{ $log->user_id }}</td>
                    		<!-- <td><button class="btn btn-danger btn-sm">Delete</button></td> -->
                    	</tr>
                      @endfor
                    	@endforeach
                    	@endif
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    
@endsection