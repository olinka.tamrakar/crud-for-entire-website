@extends('includes.header1')
@section('content-wrapper')
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Page Image's List</h3>
                </div><!-- /.box-header -->
                 <div class="box-tools">
                    @can('pageimages.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('pageimage.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Page Image') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>For</th>
                        <th>Photo</th>
                        @can('pageimages.update',Auth::user())
                          <th>Update</th>
                        @endcan
                        @can('pageimages.delete',Auth::user())
                          <th>Delete</th>
                        @endcan
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($pageimage as $pageimage)
                      <tr>
                        <td>{{$pageimage->id}}</td>
                        <td>{{$pageimage->pageimagefor}}</td>
                        <td>{{$pageimage->photo}}</td>
                        @can('pageimages.update',Auth::user())
                        <td>
                          <a href="{{Route('pageimage.update',$pageimage->id)}}">
                            <i class="fa fa-edit"></i>
                          </a>
                        </td>
                        @endcan
                        @can('pageimages.delete',Auth::user())
                        <td>
                          <a href="#">
                            <i class="fa fa-trash-o" onclick="myFunction('{{$pageimage->id}}')"></i>
                          </a>
                        </td>
                        @endcan
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the pageimage?");
    if (r == true) {
         window.location.href = "/pageimage/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection