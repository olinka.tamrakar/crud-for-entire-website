@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Page Image</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('pageimage.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			@if(!$id)
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="photo" type="file" value="{{(!$photo)?old('photo'):$photo}}" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo" placeholder="Page Image" required autofocus>

				@if ($errors->has('photo'))
				<span class="invalid-feedback" photo="alert">
					<strong>{{ $errors->first('photo') }}</strong>
				</span>
				@endif
			</div>
			@endif

			<div class="form-group has-feedback">
				<select name="pageimagefor" id="pageimagefor" required autofocus>
					<option>Select Image for</option>
					<option value="Aboutus">About Us</option>
					<option value="Ourteam">Our Team</option>
					<option value="Courses">Courses</option>
					<option value="Teachers">Teachers</option>
					<option value="Events">Events</option>
					<option value="Blog">Blog</option>
					<option value="Gallery">Gallery</option>
					<option value="Eventdetail">Event Detail</option>
					<option value="Coursedetail">Course Detail</option>
					<option value="Faq">Faq</option>
					<option value="Contact">Contact</option>
				</select>
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Page Image') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
