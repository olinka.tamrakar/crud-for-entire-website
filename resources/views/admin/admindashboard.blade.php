@extends('includes.header1')
@section('content-wrapper')

<center><h1>Hello {{Auth::user()->name}}</h1></center>

<a href="{{Route('teacher.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-green">
		<div class="inner">
			<h3>
				{{$teacher}}
			</h3>
			<p>
				Teachers
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-person-add"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('course.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-blue">
		<div class="inner">
			<h3>
				{{$course}}
			</h3>
			<p>
				Course
			</p>
		</div>
		<div class="icon">
			<i class=" ion-ios-paper-outline"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('institute.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-yellow">
		<div class="inner">
			<h3>
				{{$institute}}
			</h3>
			<p>
				Institutes
			</p>
		</div>
		<div class="icon">
			<i class="ion-ios-paper-outline"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('blog.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-red">
		<div class="inner">
			<h3>
				{{$blog}}
			</h3>
			<p>
				Blogs
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('user.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-purple">
		<div class="inner">
			<h3>
				{{$user}}
			</h3>
			<p>
				User
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-person-add"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('role.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-olive">
		<div class="inner">
			<h3>
				{{$role}}
			</h3>
			<p>
				Roles
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('permission.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-navy">
		<div class="inner">
			<h3>
				{{$permission}}
			</h3>
			<p>
				Permissions
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('course.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-gray">
		<div class="inner">
			<h3>
				{{$course}}
			</h3>
			<p>
				Courses
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('coursecategory.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-white bg-navy">
		<div class="inner">
			<h3>
				{{$coursecategory}}
			</h3>
			<p>
				Coursecategorys
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('gallery.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-teal">
		<div class="inner">
			<h3>
				{{$gallery}}
			</h3>
			<p>
				Gallerys
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('gallerycategory.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-lime">
		<div class="inner">
			<h3>
				{{$gallerycategory}}
			</h3>
			<p>
				Gallerycategorys
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('event.view')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-fuchsia">
		<div class="inner">
			<h3>
				{{$event}}
			</h3>
			<p>
				Events
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

<a href="{{Route('filter')}}">
	<div class="col-lg-3 col-xs-6">
	<!-- small box -->
	<div class="small-box bg-orange">
		<div class="inner">
			<h3>
				{{$logactivity}}
			</h3>
			<p>
				Logactivitys
			</p>
		</div>
		<div class="icon">
			<i class="ion ion-pie-graph"></i>
		</div>
	</div>
	</div><!-- ./col -->
</a>

@endsection