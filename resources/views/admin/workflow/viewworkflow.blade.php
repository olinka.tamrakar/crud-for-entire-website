@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Work Flow's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($workflow) == 4)
                        @can('workflows.create',Auth::user())
                    <div class="input-group" title="You cannot add more than 4 workflows">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add workflow') }}
                        </button>
                      </div>
                      @endcan
                    @else
                    @can('workflows.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('workflow.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add workflow') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>Created At</th>
                      <th>Title</th>
                      <th>Description</th>
                      @can('workflows.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('workflows.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($workflow as $workflows) 
                            <td>{{$workflows->id}}</td>
                    				<td>{{$workflows->title}}</td>
                            <td>{{$workflows->description}}</td>
                            @can('workflows.update',Auth::user())
                            <td>
                              <a href="/workflow/update/{{$workflows->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan
                            @can('workflows.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$workflows->id}}')"></i>
                            </a>
                          </td>   
                            @endcan  
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the workflow?");
    if (r == true) {
         window.location.href = "/workflow/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
