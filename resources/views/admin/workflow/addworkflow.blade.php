@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="{{Route('workflow.form')}}"><b>Add Work Flow</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('workflow.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="title" type="text" value="{{(!$title)?old('title'):$title}}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" placeholder="Title" required autofocus>

				@if ($errors->has('title'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('title') }}</strong>
				</span>
				@endif
			</div>
			<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
			</div>

			@if(!$id)
			<div class="form-group has-feedback" title="Upload photo related to Why Us">
				<input type="file" name="photo" id="photo" required autofocus>
				@if ($errors->has('photo'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('photo') }}</strong>
				</span>
				@endif
			</div>
			@endif

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Work Flow') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
