@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">GalleryCategory's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('gallerycategorys.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('gallerycategory.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Gallery Category') }}
                        </button></a>
                      </div>
                      @endcan
                    </div>
                  <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>GalleryCategory Name</th>
                      @can('gallerycategorys.update',Auth::user()) <th>Edit</th> @endcan
                      @can('gallerycategorys.delete',Auth::user()) <th>Delete</th> @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($gallerycategory as $gallerycategorys) 
                            <td>{{$gallerycategorys->id}}</td>
                    				<td><a href="{{Route('album.display',$gallerycategorys->id)}}">{{$gallerycategorys->name}}</a></td>
                            @can('gallerycategorys.update',Auth::user())
                            <td style="width: 200px;">
                              <a href="/gallerycategory/update/{{$gallerycategorys->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('gallerycategorys.delete',Auth::user())
                            <td style="width: 200px;">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$gallerycategorys->id}}')"></i>
        
                            </td>  
                            @endcan             
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the gallerycategory?");
    if (r == true) {
         window.location.href = "/gallerycategory/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
