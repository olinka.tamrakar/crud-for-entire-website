@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
	@if(!$id)
		<a href="../../index2.html"><b>Add Category</b></a>
	@else
		<a href="../../index2.html"><b>Update Category</b></a>
	@endif
	</div>
	
	<div class="register-box-body">
		<form action="{{Route('gallerycategory.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Category Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
				@if(!$id)
					{{ __('Add Gallery Category') }}
				@else
					{{ __('Update Gallery Category') }}
				@endif
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
