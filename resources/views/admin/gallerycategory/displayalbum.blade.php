@extends('includes.header1')
@section('content-wrapper')
<div class="container">
	<div class="btn-group" style="margin: 20px;">
		<a href="{{Route('album.display','null')}}" class="active">
			<button type="button" class="btn btn-info">All Photos</button>
		</a>
		@foreach($totalgallery as $gallerys)
		<a href="{{Route('album.display',$gallerys->id)}}">
			<button type="button" class="btn btn-info">{{$gallerys->name}}</button>
		</a>
		@endforeach
	</div>
</div>

<div class="container">	
	<div class="starter-template">
		<div class="row">
			@foreach($gallery as $gallerys)
			<div class="col-lg-3">
				<div class="thumbnail" style="min-height: 250px;">
					<a href="/gallery/{{$gallerys->photos}}"><img alt="{{$gallerys->photos}}" src="/gallery/{{$gallerys->photos}}"></a>
					<div class="caption">
						<h5><b>{{$gallerys->photos}}</b></h5>

						<p>Created date:  {{ date("d F Y",strtotime($gallerys->created_at)) }} </p>
						<p>Created at: {{date("g:ha",strtotime($gallerys->created_at)) }}</p>
					</div>
				</div>
			</div>
			@endforeach
		</div><!-- /.row -->
	</div><!-- container -->
</div>
@endsection