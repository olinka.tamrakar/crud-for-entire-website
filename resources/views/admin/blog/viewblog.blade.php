@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Blog's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('blogs.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('blog.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Blog') }}
                        </button></a>
                      </div>
                      @endcan
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>Created At</th>
                      <th>Title</th>
                      <th>Description</th>
                      @can('blogs.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('blogs.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($blog as $blogs) 
                            <td>{{$blogs->created_at}}</td>
                    				<td>{{$blogs->title}}</td>
                            <td>{{$blogs->description}}</td>
                            @can('blogs.update',Auth::user())
                            <td>
                              <a href="/blog/update/{{$blogs->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('blogs.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$blogs->id}}')"></i>
                            </a>
                          </td>   
                            @endcan            
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the blog?");
    if (r == true) {
         window.location.href = "/blog/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
