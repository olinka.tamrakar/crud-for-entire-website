@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Update Blog</b></a>
	</div>

	<div class="register-box-body">
		<form action="/blog/update/{{$id}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="title" type="text" value="{{(!$title)?old('title'):$title}}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" placeholder="Title" required autofocus>

				@if ($errors->has('title'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('title') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-menu-left form-control-feedback"></span>
				<input id="category" type="text" value="{{(!$category)?old('category'):$category}}" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category" placeholder="Category" required autofocus>

				@if ($errors->has('category'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('category') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Describe the Blog">
				<span class="glyphicon glyphicon-pencil form-control-feedback"></span>
				<textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Describe the event" rows="4" cols="50" required autofocus>{{$description}}
				</textarea>

				@if ($errors->has('description'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Blog') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
