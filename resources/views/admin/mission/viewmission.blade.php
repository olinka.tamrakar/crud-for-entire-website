@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Mission's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($mission) == 4)
                      @can('missions.create',Auth::user())
                    <div class="input-group" title="Sorry! You can add only 4 missions">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add mission') }}
                        </button>
                      </div>
                      @endcan
                    @else
                    @can('missions.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('mission.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add mission') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Description</th>
                      @can('missions.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('missions.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($mission as $missions) 
                            <td>{{$missions->id}}</td>
                    				<td>{{$missions->title}}</td>
                            <td>{{$missions->description}}</td>
                            @can('missions.update',Auth::user())
                            <td>
                              <a href="/mission/update/{{$missions->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan    
                            @can('missions.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$missions->id}}')"></i>
                            </a>
                          </td>   
                            @endcan      
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the mission?");
    if (r == true) {
         window.location.href = "/mission/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
