@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">allaboutorg Information</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                  @if(!$allaboutorg)
                    @can('allaboutorgs.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('allaboutorg.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add allaboutorg') }}
                        </button></a>
                      </div>
                      @endcan

                    @else
                      @can('allaboutorgs.update',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('allaboutorg.update',$allaboutorg[0]->id)}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Update allaboutorg') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                      @foreach($allaboutorg as $allaboutorgs)

                    <tr>
                      <th>Name of the Organization</th>
                    	<td><center>{{$allaboutorgs->organization}}</center></td>
                    </tr>

                    <tr>
                      <th>About Organization</th>
                      <td>{{$allaboutorgs->description}}</td> 
                    </tr>

                    @endforeach
                    </thead>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
