@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Course Category List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('coursecategorys.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('coursecategory.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Course Category') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      @can('coursecategorys.update',Auth::user()) <th>Update</th> @endcan
                      @can('coursecategorys.update',Auth::user()) <th>Status</th> @endcan
                    </tr>
                    </thead>
                    
                    <tbody>
                    <tr>
                      @foreach($coursecategory as $coursecategorys) 
                            <td>{{$coursecategorys->id}}</td>
                            <td>{{$coursecategorys->name}}</td>
                             @can('coursecategorys.update',Auth::user())
                            <td>
                              <a href="/coursecategory/update/{{$coursecategorys->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                             @can('coursecategorys.delete',Auth::user())
                            <td>
                              @if($coursecategorys->status== 'Active')
                              <div title="Click the button to deactivate teacher">
                              <button class="btn btn-block btn-success btn-md" type="delete" onclick="myFunction('{{$coursecategorys->id}}')" style="width: 100px">
                                  {{$coursecategorys->status}}
                              </button>
                              </div>
                              @else
                                <div title="Click the button to activate teacher">
                              <button class="btn btn-block btn-warning btn-md" type="delete" onclick="myFunction('{{$coursecategorys->id}}')" style="width: 100px"> 
                                  {{$coursecategorys->status}}
                              </button>
                              </div>
                              @endif
                            </td> 
                            @endcan
                    </tr>
                      @endforeach
                    </tbody>
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to change the status of coursecategory?");
    if (r == true) {
         window.location.href = "/coursecategory/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection