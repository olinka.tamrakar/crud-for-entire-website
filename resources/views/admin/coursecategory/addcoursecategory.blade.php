@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>
			@if(!$id) Add 
			@else Update
			@endif
		Course Category</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('coursecategory.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Course Category Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="status" value="Active">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 200px">
					@if(!$id) {{ __('Add Course Category') }}
					@else {{ __('Update Course Category') }}
					@endif
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
