@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Slider's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('sliders.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('slider.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Slider') }}
                        </button></a>
                      </div>
                      @endcan
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Subtitle</th>
                      <th>Description</th>
                      @can('sliders.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('sliders.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($slider as $sliders) 
                            <td>{{$sliders->id}}</td>
                            <td>{{$sliders->title}}</td>
                    				<td>{{$sliders->subtitle}}</td>
                            <td>{{$sliders->description}}</td>
                            @can('sliders.update',Auth::user())
                            <td>
                              <a href="/slider/update/{{$sliders->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('sliders.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$sliders->id}}')"></i>
                            </a>
                          </td>   
                            @endcan            
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the slider?");
    if (r == true) {
         window.location.href = "/slider/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
