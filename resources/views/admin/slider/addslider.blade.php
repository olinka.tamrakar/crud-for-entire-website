@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>
		@if(!$id)
			Add Slider
		@else
			Update Slider
		@endif</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('slider.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="title" type="text" value="{{(!$title)?old('title'):$title}}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" placeholder="Title" required autofocus>

				@if ($errors->has('title'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('title') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="subtitle" type="text" value="{{(!$subtitle)?old('subtitle'):$subtitle}}" class="form-control{{ $errors->has('subtitle') ? ' is-invalid' : '' }}" name="subtitle" placeholder="SubTitle" required autofocus>

				@if ($errors->has('subtitle'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('subtitle') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
			</div>

			@if(!$id)
			<div class="form-group has-feedback" title="Upload photos related to slider">
				<input type="file" name="photo" id="photo" required autofocus>
				@if ($errors->has('photo'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('photo') }}</strong>
				</span>
				@endif
			</div>
			@endif

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					@if(!$id)
						{{ __('Add Slider') }}
					@else
						{{ __('Update Slider') }}
					@endif
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
