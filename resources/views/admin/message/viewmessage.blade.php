@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Message List</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Subject</th>
                      <th>Phone</th>
                      <th>Message</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($message as $messages) 
                            <td>{{$messages->id}}</td>
                    				<td>{{$messages->form_name}}</td>
                            <td>{{$messages->form_email}}</td>            
                            <td>{{$messages->form_subject}}</td>            
                            <td>{{$messages->form_phone}}</td>            
                            <td>{{$messages->form_message}}</td>            
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
