@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
	<div class="register-box">
		<div class="register-logo">
		@if(!$id)
			<a href="../../index2.html"><b>Add Section</b></a>
		@else
			<a href="../../index2.html"><b>Update Section</b></a>
		@endif
		</div>

		<div class="register-box-body">
			<form action="{{Route('section.add',$syllabus_id)}}" method="POST" enctype="multipart/form-data" id = "addforms">
				@csrf
				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-book form-control-feedback"></span>
					<input id="sectionname" type="text" class="form-control{{ $errors->has('sectionname') ? ' is-invalid' : '' }}" name="sectionname" placeholder="sectionname" value="{{(!$sectionname)?old('sectionname'):$sectionname}}" required autofocus>

					@if ($errors->has('sectionname'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('sectionname') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group has-feedback">
					<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
					</textarea>
				</div>

				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">
							@if(!$id)
								{{ __('Add section') }}
							@else
								{{ __('Update section') }}
							@endif
						</button>
						<button type="button" class="btn btn-danger btn-block btn-flat" id ="cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</body>
@endsection
