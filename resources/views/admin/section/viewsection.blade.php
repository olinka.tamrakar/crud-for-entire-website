@extends('includes.header1')
@section('content-wrapper')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">section List</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Description</th>
              @can('sections.update',Auth::user()) <th>Update</th> @endcan
              @can('sections.delete',Auth::user()) <th>Delete</th> @endcan
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>{{$section->id}}</td>
              <td>{{$section->sectionname}}</td>
              <td>{{$section->description}}</td>
               @can('sections.update',Auth::user())
               <td>
                <a href="/section/update/{{$section->id}}">
                  <i class="fa fa-edit"></i>
                </a>
              </td>
              @endcan

              @can('sections.delete',Auth::user())
              <td>
                <a href="#">
                  <i class="fa fa-trash-o" onclick="myFunction('{{$section->id}}')"></i>
                </a>
              </td> 
              @endcan
            </tr>
          </tbody>
        </table>
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>
</div>
<script>
  function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the section?");
    if (r == true) {
     window.location.href = "/section/delete/"+id;
   } else {
    txt = "You pressed Cancel!";
  }
}
</script>
@endsection