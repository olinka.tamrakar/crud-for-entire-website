@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Why Choose Us</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('whychooseus.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback" title="Enter the introduction" >
				<textarea id="description" name ="introduction" class="form-control" style="height: 150px">{{(!$introduction)?old('introduction'):$introduction}}
				</textarea>
			</div>

			<div class="form-group has-feedback" title="Enter the question">
				<textarea id="question" name ="question" class="form-control" style="height: 50px">{{(!$question)?old('question'):$question}}
				</textarea>
			</div>

			<div class="form-group has-feedback" title="Enter the answer">
				<textarea id="answer" name ="answer" class="form-control" style="height: 50px">{{(!$answer)?old('answer'):$answer}}
				</textarea>
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 250px;">
					{{ __('Add Why Choose Us') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
