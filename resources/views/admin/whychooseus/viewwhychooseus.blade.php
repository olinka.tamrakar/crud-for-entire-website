@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Why Choose us</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($whychooseus) == 5)
                      @can('whychooseuss.create',Auth::user())
                    <div class="input-group" title="Sorry! You can add only 4 whychooseuss">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add Why Choose us') }}
                        </button>
                      </div>
                      @endcan
                    @else
                    @can('whychooseuss.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('whychooseus.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Why Choose Us') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Introduction</th>
                      <th>Question</th>
                      <th>Answer</th>
                      @can('whychooseuss.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('whychooseuss.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                      @foreach($whychooseus as $whychooseuss) 
                            <td>{{$whychooseuss->id}}</td>
                            <td>{{$whychooseuss->introduction}}</td>
                            <td>{{$whychooseuss->question}}</td>
                            <td>{{$whychooseuss->answer}}</td>
                            @can('whychooseuss.update',Auth::user())
                            <td>
                              <a href="/whychooseus/update/{{$whychooseuss->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan    
                            @can('whychooseuss.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$whychooseuss->id}}')"></i>
                            </a>
                          </td>   
                            @endcan      
                    </tr>
                      @endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the whychooseus?");
    if (r == true) {
         window.location.href = "/whychooseus/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
