@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Permission</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('permission.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Permission Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" name="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<select name="permissionfor" id="permissionfor" required autofocus>
					<option>Select permissions for</option>
					<option value="Course">Course</option>
					<option value="coursecategory">Course Category</option>
					<option value="Slider">Slider</option>
					<option value="Gallery">Gallery</option>
					<option value="Gallerycategory">Gallery Category</option>
					<option value="Blog">Blog</option>
					<option value="Blogcategory">Blog Category</option>
					<option value="Event">Event</option>
					<option value="Institute">Institute</option>
					<option value="Teacher">Teacher</option>
					<option value="Editor">Editor</option>
					<option value="Permission">Permission</option>
					<option value="Role">Role</option>
					<option value="User">User</option>
					<option value="Faq">FAQs</option>
					<option value="Contact">Contact</option>
					<option value="Allaboutorg">Allaboutorg</option>
					<option value="Mission">Mission</option>
					<option value="Workflow">Workflow</option>
					<option value="Whyus">Why Us</option>
					<option value="Ourteam">Our Team</option>
					<option value="Openinghours">Opening Hours</option>
					<option value="Links">Links</option>
					<option value="Indexpage">Index Page</option>
					<option value="Features">Features</option>
					<option value="WhyChooseus">WhyChooseus</option>
					<option value="Syllabus">Syllabus</option>
					<option value="Section">Section</option>
					<option value="Pageimage">Pageimage</option>
				</select>
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Permission') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
