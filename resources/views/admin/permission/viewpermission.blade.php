@extends('includes.header1')
@section('content-wrapper')
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Permissions List</h3>
                </div><!-- /.box-header -->
                 <div class="box-tools">
                    @can('permissions.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('permission.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Permission') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                      	<th>Permission</th>
                        <th>For</th>
                        @can('permissions.update',Auth::user())
                          <th>Update</th>
                        @endcan
                        @can('permissions.delete',Auth::user())
                          <th>Delete</th>
                        @endcan
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($permission as $permission)
                      <tr>
                        <td>{{$permission->id}}</td>
                        <td>{{$permission->name}}</td>
                        <td>{{$permission->permissionfor}}</td>
                        @can('permissions.update',Auth::user())
                        <td>
                          <a href="{{Route('permission.update',$permission->id)}}">
                            <i class="fa fa-edit"></i>
                          </a>
                        </td>
                        @endcan
                        @can('permissions.delete',Auth::user())
                        <td>
                          <a href="#">
                            <i class="fa fa-trash-o" onclick="myFunction('{{$permission->id}}')"></i>
                          </a>
                        </td>
                        @endcan
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the permission?");
    if (r == true) {
         window.location.href = "/permission/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection