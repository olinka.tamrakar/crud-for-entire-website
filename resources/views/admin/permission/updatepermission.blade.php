@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Update Permission</b></a>
	</div>

	<div class="register-box-body">
		<form action="/permission/update/{{$datas->id}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" value="{{(!$datas->name)?old('name'):$datas->name}}" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Permission Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" name="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<select name="permissionfor" id="permissionfor" required autofocus>
					<option disabled>Select permissions for</option>
					<option value="Course">Course</option>
					<option value="Event">Event</option>
					<option value="Blog">Blog</option>
					<option value="Gallery">Gallery</option>
					<option value="Institute">Institute</option>
					<option value="Teacher">Teacher</option>
					<option value="Editor">Editor</option>
				</select>
			</div>

			<input type="hidden" name="id" value="{{$datas->id}}">

			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width:150px;">
					{{ __('Update Permission') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
