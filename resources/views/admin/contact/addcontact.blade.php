@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Contact</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('contact.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="location" type="text" value="{{(!$location)?old('location'):$location}}" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" placeholder="Office Location" required autofocus>

				@if ($errors->has('location'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('location') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="number" type="text" value="{{(!$number)?old('number'):$number}}" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" placeholder="Office Contact Number" required autofocus>

				@if ($errors->has('number'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('number') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="email" type="text" value="{{(!$email)?old('email'):$email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Office Contact Email" required autofocus>

				@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="videocall" type="text" value="{{(!$videocall)?old('videocall'):$videocall}}" class="form-control{{ $errors->has('videocall') ? ' is-invalid' : '' }}" name="videocall" placeholder="Make a Videocall" required autofocus>

				@if ($errors->has('videocall'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('videocall') }}</strong>
				</span>
				@endif
			</div>
			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Contact') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
