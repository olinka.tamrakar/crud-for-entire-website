@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Contact Information</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                  @if(!$contact)
                    @can('contacts.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('contact.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add contact') }}
                        </button></a>
                      </div>
                      @endcan

                    @else
                      @can('contacts.update',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('contact.update',$contact[0]->id)}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Update contact') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                      @foreach($contact as $contacts)

                    <tr>
                      <th>Office Location</th>
                    	<td>{{$contacts->location}}</td>
                    </tr>

                    <tr>
                      <th>Office Contact Number</th>
                      <td>{{$contacts->number}}</td> 
                    </tr>

                    <tr>
                      <th>Office Contact Email</th>
                      <td>{{$contacts->email}}</td>
                    </tr>

                    <tr>
                      <th>Office Contact Video Call</th>
                      <td>{{$contacts->videocall}}</td>
                    </tr>
                     
                    </tr>
                    @endforeach
                    </thead>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
