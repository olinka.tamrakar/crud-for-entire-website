@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Event's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                     @can('events.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('event.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Event') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Topics</th>
                      <th>Host</th>
                      <th>Location</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Website</th>
                      <th>Key Note Speakers</th>
                       @can('events.update',Auth::user())<th>Update</th>@endcan
                       @can('events.delete',Auth::user())<th>Delete</th>@endcan
                    </tr>
                    </thead>

                    <tbody><tr>
                    	@foreach($event as $events) 
                    				<td>{{$events->topics}}</td>
                            <td>{{$events->host}}</td>
                            <td>{{$events->location}}</td>
                            <td>{{$events->starttime}}</td>
                            <td>{{$events->endtime}}</td>
                            <td>{{$events->startdate}}</td>
                            <td>{{$events->enddate}}</td>
                            <td>{{$events->website}}</td>
                            <td>{{$events->keynotespeakers}}</td>
                            @can('events.update',Auth::user())
                            <td style="width:26px">
                              <a href="/event/update/{{$events->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('events.delete',Auth::user())
                            <td>
                              <a href="#">
                                <i class="fa fa-trash-o" onclick="myFunction('{{$events->id}}')"></i>
                              </a>
                            </td>  
                            @endcan             
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the event?");
    if (r == true) {
         window.location.href = "/event/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
