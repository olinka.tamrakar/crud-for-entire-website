@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Event</b></a>
	</div>

	<div class="register-box-body">
		<form action="/event/add" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="topics" value="{{(!$topics)?old('topics'):$topics}}" type="text" class="form-control{{ $errors->has('topics') ? ' is-invalid' : '' }}" name="topics" placeholder="Topics" required autofocus>

				@if ($errors->has('topics'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('topics') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-home form-control-feedback"></span>
				<input id="host" value="{{(!$host)?old('host'):$host}}" type="text" class="form-control{{ $errors->has('host') ? ' is-invalid' : '' }}" name="host" placeholder="Host" required autofocus>

				@if ($errors->has('host'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('host') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-star form-control-feedback"></span>
				<input id="location" value="{{(!$location)?old('location'):$location}}" type="text" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" placeholder="Location" required autofocus>

				@if ($errors->has('location'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('location') }}</strong>
				</span>
				@endif
			</div>

			<div title="Enter Start Time" class="form-group has-feedback">
				<span class="glyphicon glyphicon-time form-control-feedback"></span>
				<input value="{{(!$starttime)?old('starttime'):$starttime}}" id="starttime" type="time" class="form-control{{ $errors->has('starttime') ? ' is-invalid' : '' }}" name="starttime" placeholder="Start Time" required>

				@if ($errors->has('starttime'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('starttime') }}</strong>
				</span>
				@endif
			</div>

			<div title="Enter Start Time" class="form-group has-feedback">
				<span class="glyphicon glyphicon-time form-control-feedback"></span>
				<input value="{{(!$endtime)?old('endtime'):$endtime}}" id="endtime" type="time" class="form-control{{ $errors->has('endtime') ? ' is-invalid' : '' }}" name="endtime" placeholder="Start Time" required>

				@if ($errors->has('endtime'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('endtime') }}</strong>
				</span>
				@endif
			</div>

			<div title="Enter Start Date" class="form-group has-feedback">
				<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
				<input id="startdate" value="{{(!$startdate)?old('startdate'):$startdate}}" type="date" class="form-control{{ $errors->has('startdate') ? ' is-invalid' : '' }}" name="startdate" placeholder="Start Date" required>

				@if ($errors->has('startdate'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('startdate') }}</strong>
				</span>
				@endif
			</div>

			<div title="Enter End Date" class="form-group has-feedback">
				<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
				<input id="enddate" value="{{(!$enddate)?old('enddate'):$enddate}}" type="date" class="form-control{{ $errors->has('enddate') ? ' is-invalid' : '' }}" name="enddate" placeholder="End Date" required autofocus>

				@if ($errors->has('enddate'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('enddate') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-globe form-control-feedback"></span>
				<input id="website" value="{{(!$website)?old('website'):$website}}" type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" placeholder="Website" required autofocus>

				@if ($errors->has('website'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('website') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<input id="keynotespeakers" value="{{(!$keynotespeakers)?old('keynotespeakers'):$keynotespeakers}}" type="text" class="form-control{{ $errors->has('keynotespeakers') ? ' is-invalid' : '' }}" name="keynotespeakers" placeholder="Key Note Speakers" required autofocus>

				@if ($errors->has('keynotespeakers'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('keynotespeakers') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Describe the event">
				<span class="glyphicon glyphicon-pencil form-control-feedback"></span>
				<textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Describe the event" rows="4" cols="50" required autofocus>{{$description}}
				</textarea>

				@if ($errors->has('description'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Upload photos related to Blog">
				<input type="file" name="photos" value id="photos" autofocus>
				@if ($errors->has('photos'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('photos') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">

			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Event') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
