@extends('includes.header1')
@section('content-wrapper')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Gallery's List</h3>
      </div><!-- /.box-header -->
      <div class="box-tools">
        @can('gallerys.create',Auth::user())
        <div class="container">
          <div class="starter-template" style="display: inline-block;">
            <div class="row">
              <div class="col-lg-3">
                <a href="{{Route('gallery.form')}}">
                  <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                   {{ __('Add Gallery') }}
                 </button></a>
               </div>
               <div class="col-lg-3">
                <a href="{{Route('gallery.view')}}">
                  <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                   {{ __('List View') }}
                 </button></a>
               </div>
               <div class="col-lg-3">
                <a href="{{Route('album.display','null')}}">
                  <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                   {{ __('Image View') }}
                 </button></a>
               </div>
             </div>
             @endcan
           </div>
         </div>
       </div>
     <div class="box-body">
      <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th>ID</th>
            <th>Category</th>
            <th>Photos</th>
            @can('gallerys.update',Auth::user())<th>Update</th>@endcan
            @can('gallerys.delete',Auth::user())<th>Delete</th>@endcan
          </tr>

          <tbody>
            <tr>
              @foreach ($gallery as $gallerys) 
              <td>{{$gallerys->id}}</td>
              <td>{{$gallerys->name}}</td>
              <td><a href ="{{asset("/gallerygallery/{$gallerys->photos}")}}"> {{$gallerys->photos}}</a></td>
              @can('gallerys.update',Auth::user())
              <td style="width:26px">
                <a href="/gallerygallery/update/{{$gallerys->id}}"><i class="fa fa-edit"></i></a>
              </td>
              @endcan

              @can('gallerys.delete',Auth::user())
              <td>
                <a href="#"><i class="fa fa-trash-o" onclick="myFunction('{{$gallerys->id}}')">
                </i></a>
              </td> 
              @endcan
            </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div>
</div>
<script>
  function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to change the status?");
    if (r == true) {
     window.location.href = "/gallery/delete/"+id;
   } else {
    txt = "You pressed Cancel!";
  }
}
</script>
@endsection