@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Add Gallery</b></a>
  </div>

  <div class="register-box-body">
    <form action="/gallery/add" method="POST" enctype="multipart/form-data" id = "addforms">
      @csrf

      <div class="form-group has-feedback">
        <select name="gallerycategory_id" id="gallerycategory_id" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" required autofocus>
          @foreach($category as $categorys)
            <option value="{{$categorys->id}}">{{$categorys->name}}</option>
          @endforeach
        </select>
      </div>
      
      <div class="form-group has-feedback" title="Upload photos for the gallery">
        <input type="file" name="photos[]" id="photos" multiple required autofocus>
        @if ($errors->has('photos'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('photos') }}</strong>
        </span>
        @endif
      </div>

      <input type="hidden" name="id" value="{{$id}}">
      
      <div class="row">
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">
          {{ __('Add Gallery') }}
        </button>
      </div>
    </div>
    </form>
  </div>  
</div>    
</body>
@endsection
