 <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset('css/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap/css/bootstrap.css.map')}}" rel="stylesheet" type="text/css" />

    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- Theme style -->
    <link href="{{ asset('css/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
    
    <!-- iCheck -->
    <link href="css/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
<body class="login-page">
    <div class="login-box">
        <div class="login-logo">
        <a href="../../index2.html"><b>Admin Login</b></a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>            
                    <form method="POST" action="/adminlogin" aria-label="{{ __('Login') }}" id = "addforms">
                        @csrf
                            <div class="form-group has-feedback">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        

                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox icheck">
                                    <label>
                                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            </div><!-- /.col -->
                        </div>
                    </form>
        </div>
    </div>
        <!-- jQuery 2.1.3 -->
        <script src="css/plugins/jQuery/jQuery-2.1.3.min.js"></script>
        
        <!-- Bootstrap 3.3.2 JS -->
        <script src="css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- iCheck -->
        <script src="css/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        
        <script>
        $(function () {
            $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
            });
        });
        </script>
    </body>

