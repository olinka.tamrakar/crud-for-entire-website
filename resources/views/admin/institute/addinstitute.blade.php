@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Institute</b></a>
	</div>

	<div class="register-box-body">
		<form action="/institute/add" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Full name" value="{{(!$name)?old('name'):$name}}" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-home form-control-feedback"></span>
				<input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Full Address" value="{{(!$address)?old('address'):$address}}" required autofocus>

				@if ($errors->has('address'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('address') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-phone form-control-feedback"></span>
				<input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Phone" value="{{(!$phone)?old('phone'):$phone}}" required autofocus>

				@if ($errors->has('phone'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('phone') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{(!$email)?old('email'):$email}}" required>

				@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password"  required>

				@if ($errors->has('password'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<input id="website" type="text" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" placeholder="website" value="{{(!$website)?old('website'):$website}}" required>

				@if ($errors->has('website'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('website') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Enter the Established Date">
				<span class="glyphicon glyphicon-calendar form-control-feedback"></span>
				<input id="establisheddate" type="date" class="form-control{{ $errors->has('establisheddate') ? ' is-invalid' : '' }}" name="establisheddate" placeholder="Established Date" value="{{(!$establisheddate)?old('establisheddate'):$establisheddate}}" required>

				@if ($errors->has('establisheddate'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('establisheddate') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				Select the courses <br>
			@foreach($courses as $course)
				<input id="courses" type="checkbox" value="{{$course->id}}" class="form-control{{ $errors->has('courses') ? ' is-invalid' : '' }}" name="courses[]" autofocus> {{$course->name}}
			@endforeach
				
				@if ($errors->has('courses'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('courses') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Upload Institute's logo">
				<input type="file" name="logo" id="logo" required autofocus>
				@if ($errors->has('logo'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('logo') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<input type="hidden" name="status" value="Active">
			<input type="hidden" name="role" value="Institute">
			
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Institute') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
