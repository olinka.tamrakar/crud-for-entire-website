@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Institute's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @can('institutes.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('institute.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Institute') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                 <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th>Name</th>
                      <th>Established Date</th>
                      <th>Address</th>
                      <th>Phone</th>
                      <th>Email</th>
                      <th>Website</th>
                      <th>Established Date</th>
                      @can('institutes.update',Auth::user())<th>Edit</th>@endcan
                      @can('institutes.delete',Auth::user())<th>Delete</th>@endcan
                     </tr>
                    </thead>
                    <tbody>
                    <tr>
                    	@foreach($user as $users) 
                        @foreach ($institute as $institutes) 
                          @if($users->id == $institutes->user_id)
                            <td>{{$institutes->id}}</td>
                            <td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            <td>{{$users->address}}</td>
                            <td>{{$users->phone}}</td>
                            <td>{{$institutes->website}}</td>
                            <td>{{$institutes->establisheddate}}</td>
                            
                            @can('institutes.update',Auth::user())
                            <td style="width:26px">
                              <a href="/institute/update/{{$institutes->id}}"> 
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('institutes.delete',Auth::user())
                            <td>
                              <!-- @if($institutes->status== 'Active') -->
                              <div title="Click the button to deactivate institute">
                              <button class="btn btn-block btn-danger btn-md" type="delete" onclick="myFunction('{{$institutes->id}}')">
                                  Delete
                              </button>
                              </div>
                              <!-- @else
                                <div title="Click the button to activate institute">
                              <button class="btn btn-block btn-warning btn-md" type="delete" onclick="myFunction('{{$institutes->id}}')">
                                  {{$institutes->status}}
                              </button>
                              </div>
                              @endif -->
                             
                            </td>
                            @endcan
                          @endif
                        @endforeach
                    </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the blog?");
    if (r == true) {
         window.location.href = "/institute/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
