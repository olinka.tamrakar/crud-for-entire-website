@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Why Us's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($whyus) == 4)
                       <div class="input-group" title="You can add only 4 'Why Us' points">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add Why Us') }}
                        </button>
                      </div>
                    @else
                    @can('whyuss.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('whyus.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add whyus') }}
                        </button></a>
                      </div>
                      @endcan
                      @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>Created At</th>
                      <th>Title</th>
                      <th>Description</th>
                      @can('whyuss.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('whyuss.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                    	@foreach($whyus as $whyuss) 
                            <td>{{$whyuss->id}}</td>
                    				<td>{{$whyuss->title}}</td>
                            <td>{{$whyuss->description}}</td>
                            @can('whyuss.update',Auth::user())
                            <td>
                              <a href="/whyus/update/{{$whyuss->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan  
                            @can('whyuss.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$whyuss->id}}')"></i>
                            </a>
                          </td>   
                            @endcan   
                    </tr>
                    	@endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the whyus?");
    if (r == true) {
         window.location.href = "/whyus/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
