@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Update Editor</b></a>
	</div>

	<div class="register-box-body">
		<form action="/editor/update/{{$id}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-cash form-control-feedback"></span>
				<input id="email" type="text" value="{{(!$email)?old('email'):$email}}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" required autofocus>

				@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-home form-control-feedback"></span>
				<input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Full Address" value="{{(!$address)?old('address'):$address}}" required autofocus>

				@if ($errors->has('address'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('address') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-phone form-control-feedback"></span>
				<input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Phone" value="{{(!$phone)?old('phone'):$phone}}" required autofocus>

				@if ($errors->has('phone'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('phone') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<input type="hidden" name="role" value="editor">
			<input type="hidden" name="status" value="Active">

			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px">
					{{ __('Update Editor') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
