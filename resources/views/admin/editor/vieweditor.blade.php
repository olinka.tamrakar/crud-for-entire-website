@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Editor List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                     @can('editors.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('editor.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Editor') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <tr>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Status</th>
                      @can('editors.update',Auth::user())<th>Update</th>@endcan
                      @can('editors.delete',Auth::user())<th>Delete</th>@endcan
                    </tr>

                    <tr>
                        @foreach($user as $users) 
                        @foreach ($editor as $editors) 
                          @if($users->id == $editors->user_id)
                            <td>{{$editors->id}}</td>
                            <td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            @can('editors.update',Auth::user())
                            <td style="width:26px">
                              <a href="/editor/update/{{$editors->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('editors.delete',Auth::user())
                            <td>

                              <div title="Click the button to deactivate editor">
                                <a href="#">
                                  <i class="fa fa-trash-o" onclick="myFunction('{{$editors->id}}')"></i>
                                </a>
                              </div>

                             <!--  @if($editors->status== 'Active')
                              <div title="Click the button to deactivate editor">
                              <button class="btn btn-block btn-success btn-md" type="delete" onclick="myFunction('{{$editors->id}}')">
                                  {{$editors->status}}
                              </button>
                              </div>
                              @else
                                <div title="Click the button to activate editor">
                              <button class="btn btn-block btn-warning btn-md" type="delete" onclick="myFunction('{{$editors->id}}')">
                                  {{$editors->status}}
                              </button>
                              </div>
                              @endif -->
                             
                            </td>
                            @endcan
                          @endif
                        @endforeach
                    </tr>
                      @endforeach
                  </table>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the editor?");
    if (r == true) {
         window.location.href = "/editor/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection