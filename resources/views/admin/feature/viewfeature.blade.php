@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Feature's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($feature) == 6)
                       <div class="input-group" title="You can add only 4 'Feature' points">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add Feature') }}
                        </button>
                      </div>
                    @else
                    @can('features.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('feature.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add feature') }}
                        </button></a>
                      </div>
                      @endcan
                      @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Heading</th>
                      <th>Description</th>
                      @can('features.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('features.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                      @foreach($feature as $features) 
                            <td>{{$features->id}}</td>
                            <td>{{$features->heading}}</td>
                            <td>{{$features->description}}</td>
                            @can('features.update',Auth::user())
                            <td>
                              <a href="/feature/update/{{$features->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan  
                            @can('features.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$features->id}}')"></i>
                            </a>
                          </td>   
                            @endcan   
                    </tr>
                      @endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the feature?");
    if (r == true) {
         window.location.href = "/feature/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
