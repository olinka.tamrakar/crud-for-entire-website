@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add features</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('feature.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="heading" type="text" value="{{(!$heading)?old('heading'):$heading}}" class="form-control{{ $errors->has('heading') ? ' is-invalid' : '' }}" name="heading" placeholder="Heading" required autofocus>

				@if ($errors->has('heading'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('heading') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
			</div>

			@if(!$id)
			<div class="form-group has-feedback" title="Upload photo for the features">
				<input type="file" name="photo" id="photo" required autofocus>
				@if ($errors->has('photo'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('photo') }}</strong>
				</span>
				@endif
			</div>
			@endif

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 250px;">
					{{ __('Add features') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
