@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Role</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('role.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$data['name'])?old('name'):$data->name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Role Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<label for="name">Blog Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Blog')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Course Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Course')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Teacher Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Teacher')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Editor Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Editor')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Event Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Event')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Gallery Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Gallery')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Slider Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Slider')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Institute Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Institute')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Course Category Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Coursecategory')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Role Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Role')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Permission Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Permission')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">User Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'User')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Gallerycategory Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Gallerycategory')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Blogcategory Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Blogcategory')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= {{$permissions->id}}>{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Contact Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Contact')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Allaboutorg Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Allaboutorg')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Mission Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Mission')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Whyus Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Whyus')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Workflow Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Workflow')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">OurTeam Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Ourteam')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Indexpage Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Indexpage')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Links Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Links')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Features Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Features')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Why Choose Us Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'WhyChooseus')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Openinghours Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Openinghours')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<div class="form-group has-feedback">
				<label for="name">Pageimage Permissions</label>
				@foreach($permission as $permissions)
					@if($permissions->permissionfor == 'Pageimage')
				<div class="checkbox">
					<input type="checkbox" name="permission[]" value= "{{$permissions->id}}">{{$permissions->name}}
				</div>
					@endif
				@endforeach
			</div>

			<input type="hidden" name="id" value="{{$data['id']}}">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Role') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
