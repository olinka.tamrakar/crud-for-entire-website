@extends('includes.header1')
@section('content-wrapper')
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users Lists</h3>
                </div><!-- /.box-header -->
                <div class="box-tools">
                    @can('roles.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('role.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Role') }}
                        </button></a>
                      </div>
                      @endcan
                  </div>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                      	<th>Role</th>
                        @can('roles.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('roles.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($role as $role)
                      <tr>
                        <td>{{$role->id}}</td>
                        <td>{{$role->name}}</td>
                        @can('roles.update',Auth::user())
                        <td>
                          <a href="{{Route('role.update',$role->id)}}">
                            <i class="fa fa-edit"></i>
                          </a>
                        </td>
                        @endcan

                        @can('roles.delete',Auth::user())
                        <td>
                          <a href="#">
                            <i class="fa fa-trash-o" onclick="myFunction('{{$role->id}}')"></i>
                          </a>
                        </td>
                        @endcan
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the role?");
    if (r == true) {
         window.location.href = "/role/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection