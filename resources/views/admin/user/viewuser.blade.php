@extends('includes.header1')
@section('content-wrapper')
	<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Users Lists</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>User ID</th>
                      	<th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    @foreach($roles as $role)
                      @if($user->id == $role->user_id)
                      <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$role->name}}</td>
                      </tr>
                      @endif
                      @endforeach
                    @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
    
@endsection