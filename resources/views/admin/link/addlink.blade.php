@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Index Page</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('link.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="socialmedia" type="text" value="{{(!$socialmedia)?old('socialmedia'):$socialmedia}}" class="form-control{{ $errors->has('socialmedia') ? ' is-invalid' : '' }}" name="socialmedia" placeholder="Social Media Name" required autofocus>

				@if ($errors->has('socialmedia'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('socialmedia') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="link" type="text" value="{{(!$link)?old('link'):$link}}" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="link" placeholder="URL" required autofocus>

				@if ($errors->has('link'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('link') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 250px;">
					{{ __('Add Index Page') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
