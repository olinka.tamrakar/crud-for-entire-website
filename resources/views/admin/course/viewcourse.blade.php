@extends('includes.header1')
@section('content-wrapper')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Course List</h3>
      </div><!-- /.box-header -->
      <div class="box-tools">
       @can('courses.create',Auth::user())
       <div class="input-group">
        <a href="{{Route('course.form')}}">
          <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
           {{ __('Add course') }}
         </button></a>
       </div>
       @endcan
     </div>
     <div class="box-body">
      <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Features</th>
            <th>Description</th>
            <th></th>
            @can('courses.update',Auth::user()) <th>Update</th> @endcan
            @can('courses.delete',Auth::user()) <th>Delete</th> @endcan
          </tr>
        </thead>

        <tbody>
          <tr>
            @foreach($course as $courses) 
            <td>{{$courses->name}}</td>
            <td>{{$courses->price}}</td>
            <td>{{$courses->features}}</td>
            <td>{{$courses->description}}</td>
            <td> 
              <a href="{{Route('syllabus.form',$courses->id)}}">
              <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
               {{ __('Add Syallbus') }}
             </button></a>

             <a href="{{Route('syllabus.view',$courses->id)}}">
              <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
               {{ __('View Syallbus') }}
             </button></a></td>
             @can('courses.update',Auth::user())
             <td>
              <a href="/course/update/{{$courses->id}}">
                <i class="fa fa-edit"></i>
              </a>
            </td>
            @endcan

            @can('courses.delete',Auth::user())
            <td>
              <a href="#">
                <i class="fa fa-trash-o" onclick="myFunction('{{$courses->id}}')"></i>
              </a>
            </td> 
            @endcan
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<script>
  function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the course?");
    if (r == true) {
     window.location.href = "/course/delete/"+id;
   } else {
    txt = "You pressed Cancel!";
  }
}
</script>
@endsection