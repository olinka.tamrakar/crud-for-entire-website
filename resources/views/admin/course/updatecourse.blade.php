@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Update Course</b></a>
	</div>

	<div class="register-box-body">
		<form action="/course/update/{{$id}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-cash form-control-feedback"></span>
				<input id="price" type="text" value="{{(!$price)?old('price'):$price}}" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" placeholder="Price" required autofocus>

				@if ($errors->has('price'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('price') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				Select the courses
				<select name="coursecategory_id">
			@foreach($coursecategory as $course)
			<option value="{{$course->id}}" class="form-control{{ $errors->has('courses') ? ' is-invalid' : '' }}" required autofocus>
				{{$course->name}}
			</option>
			@endforeach
					
				</select>
				
				@if ($errors->has('courses'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('courses') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Enter features">
				<input type="hidden" name="count" value="1" />
					<div class="control-group" id="fields">
						<div class="controls" id="profs"> 
							<div class="input-append">
								<div id="field">
									@foreach($features as $featuress)
									<input autocomplete="off"  
											id="field1" 
											oninput="this.className = ''" 
											name="features[]" 
											class="form-control {{ $errors->has('features') ? ' is-invalid' : '' }}" 
											type="text" 
											value="{{$featuress}}" 
											placeholder="Enter features" 
											data-items="8" 
											required />
									<button id="b1" class="btn add-more" type="button">
										+
									</button>
									@endforeach
								</div>
								@if ($errors->has('features'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('features') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>
			</div>

			<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
				</div>


			<input type="hidden" name="id" value="{{$id}}">

			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 150px">
					{{ __('Update Course') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
