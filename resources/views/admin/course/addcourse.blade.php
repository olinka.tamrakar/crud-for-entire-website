@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
	<div class="register-box">
		<div class="register-logo">
			<a href="../../index2.html"><b>Add Course</b></a>
		</div>

		<div class="register-box-body">
			<form action="{{Route('course.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
				@csrf
				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-book form-control-feedback"></span>
					<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" required autofocus>

					@if ($errors->has('name'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-cash form-control-feedback"></span>
					<input id="price" type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" placeholder="Price" required autofocus>

					@if ($errors->has('price'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('price') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group has-feedback" title="Enter Features">
					<input type="hidden" name="count" value="1" />
					<div class="control-group" id="fields">
						<div class="controls" id="profs"> 
							<div class="input-append">
								<div id="field"><input autocomplete="off" class="input" id="field1" oninput="this.className = ''" name="features[]" class="form-control {{ $errors->has('features') ? ' is-invalid' : '' }}" type="text" placeholder="Enter Features" data-items="8" required /><button id="b1" class="btn add-more" type="button">+</button></div>
								@if ($errors->has('features'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('features') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>
				</div>

				<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
				</div>

				<div class="form-group has-feedback">
					Select the course category
					<select name="coursecategory_id">
						@foreach($coursecategory as $course)
						<option value="{{$course->id}}" class="form-control{{ $errors->has('courses') ? ' is-invalid' : '' }}" required autofocus>
							{{$course->name}}
						</option>
						@endforeach
						
					</select>
					
					@if ($errors->has('courses'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('courses') }}</strong>
					</span>
					@endif
				</div>

				<div class="form-group has-feedback" title="Upload photos related to course">
					<input type="file" name="photos" id="file" required autofocus>
					@if ($errors->has('file'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('file') }}</strong>
					</span>
					@endif
				</div>

				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">
							{{ __('Add Course') }}
						</button>
						<button type="button" class="btn btn-danger btn-block btn-flat" id ="cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</body>
@endsection
