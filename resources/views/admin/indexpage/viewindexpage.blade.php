@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Index Page Information</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                  @if(!$indexpage)
                    @can('indexpages.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('indexpage.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add indexpage') }}
                        </button></a>
                      </div>
                      @endcan

                    @else
                      @can('indexpages.update',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('indexpage.update',$indexpage[0]->id)}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Update indexpage') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                      @foreach($indexpage as $indexpages)

                    <tr>
                      <th>Heading</th>
                    	<td>{{$indexpages->heading}}</td>
                    </tr>

                    <tr>
                      <th>Quote</th>
                      <td>{{$indexpages->quote}}</td> 
                    </tr>

                    <tr>
                      <th>Description</th>
                      <td>{{$indexpages->description}}</td> 
                    </tr>

                    @endforeach
                    </thead>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
@endsection
