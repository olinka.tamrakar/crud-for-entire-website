@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Our Team</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('ourteam.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="name" type="text" value="{{(!$name)?old('name'):$name}}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="position" type="text" value="{{(!$position)?old('position'):$position}}" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" placeholder="Position" required autofocus>

				@if ($errors->has('position'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('position') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<textarea id="description" name ="description" class="form-control" style="height: 150px">{{(!$description)?old('description'):$description}}
				</textarea>
			</div>

			@if(!$id)
			<div class="form-group has-feedback" title="Upload photo related to Blog">
				<input type="file" name="photo" id="photo" required autofocus>
				@if ($errors->has('photo'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('photo') }}</strong>
				</span>
				@endif
			</div>
			@endif
			<input type="hidden" position="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Add Our Team') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
