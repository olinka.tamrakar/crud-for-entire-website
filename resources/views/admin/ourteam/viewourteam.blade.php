@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Our Team's List</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($ourteam) == 6)
                      @can('ourteams.create',Auth::user())
                    <div class="input-group" title="Sorry! You can add only 4 ourteams">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add ourteam') }}
                        </button>
                      </div>
                      @endcan
                    @else
                    @can('ourteams.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('ourteam.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add ourteam') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Description</th>
                      @can('ourteams.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('ourteams.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                      @foreach($ourteam as $ourteams) 
                            <td>{{$ourteams->id}}</td>
                            <td>{{$ourteams->title}}</td>
                            <td>{{$ourteams->description}}</td>
                            @can('ourteams.update',Auth::user())
                            <td>
                              <a href="/ourteam/update/{{$ourteams->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan    
                            @can('ourteams.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$ourteams->id}}')"></i>
                            </a>
                          </td>   
                            @endcan      
                    </tr>
                      @endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the ourteam?");
    if (r == true) {
         window.location.href = "/ourteam/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
