@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>
			@if(!$id) Add 
			@else Update
			@endif
		FAQs</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('faq.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-question-sign form-control-feedback"></span>
				<input id="question" type="text" value="{{(!$question)?old('question'):$question}}" class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" placeholder="Enter the question" required autofocus style="height: 50px;">

				@if ($errors->has('question'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('question') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<textarea id="description" name ="answer" class="form-control" style="height: 150px">{{(!$answer)?old('answer'):$answer}}
				</textarea>
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="status" value="Active">
			<div class="row">
			<div class="col-xs-5">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 200px">
					@if(!$id) {{ __('Add FAQs') }}
					@else {{ __('Update FAQs') }}
					@endif
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
