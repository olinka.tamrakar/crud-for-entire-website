@extends('includes.header1')
@section('content-wrapper')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Syllabus List</h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Class Time</th>
              <th>Estimate Time</th>
              <th>Course Duration</th>
              <th></th>
              @can('syllabuss.update',Auth::user()) <th>Update</th> @endcan
              @can('syllabuss.delete',Auth::user()) <th>Delete</th> @endcan
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>{{$syllabus->id}}</td>
              <td>{{$syllabus->syllabusname}}</td>
              <td>{{$syllabus->classtime}}</td>
              <td>{{$syllabus->estimatetime}}</td>
              <td>{{$syllabus->courseduration}}</td>
              <td>
                <a href="{{Route('section.form',$syllabus->id)}}">
                  <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                    {{ __('Add Section') }}
                 </button></a>

                 <a href="{{Route('section.view',$syllabus->id)}}">
                  <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                    {{ __('View Section') }}
                 </button></a>
               </td>
               @can('syllabuss.update',Auth::user())
               <td>
                <a href="/syllabus/update/{{$syllabus->id}}">
                  <i class="fa fa-edit"></i>
                </a>
              </td>
              @endcan

              @can('syllabuss.delete',Auth::user())
              <td>
                <a href="#">
                  <i class="fa fa-trash-o" onclick="myFunction('{{$syllabus->id}}')"></i>
                </a>
              </td> 
              @endcan
            </tr>
          </tbody>
        </table>
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>
</div>
<script>
  function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the syllabus?");
    if (r == true) {
     window.location.href = "/syllabus/delete/"+id;
   } else {
    txt = "You pressed Cancel!";
  }
}
</script>
@endsection