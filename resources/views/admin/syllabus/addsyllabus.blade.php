@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
	<div class="register-box">
		<div class="register-logo">
		@if(!$id)
			<a href="../../index2.html"><b>Add Syllabus</b></a>
		@else
			<a href="../../index2.html"><b>Update Syllabus</b></a>
		@endif
		</div>

		<div class="register-box-body">
			<form action="{{Route('syllabus.add',$course_id)}}" method="POST" enctype="multipart/form-data" id = "addforms">
				@csrf
				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-book form-control-feedback"></span>
					<input id="syllabusname" type="text" class="form-control{{ $errors->has('syllabusname') ? ' is-invalid' : '' }}" name="syllabusname" placeholder="Syllabusname" value="{{(!$syllabusname)?old('syllabusname'):$syllabusname}}" required autofocus>

					@if ($errors->has('syllabusname'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('syllabusname') }}</strong>
					</span>
					@endif
				</div>

				<div syllabusname="Enter the class time" class="form-group has-feedback">
					<span class="glyphicon glyphicon-time form-control-feedback"></span>
					<input id="classtime" type="time" class="form-control{{ $errors->has('classtime') ? ' is-invalid' : '' }}" name="classtime" placeholder="Class Time" value="{{(!$classtime)?old('classtime'):$classtime}}" required autofocus>

					@if ($errors->has('classtime'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('classtime') }}</strong>
					</span>
					@endif
				</div>
				
				<div syllabusname="Enter the estimate time" class="form-group has-feedback">
					<span class="glyphicon glyphicon-time form-control-feedback"></span>
					<input id="estimatetime" type="time" class="form-control{{ $errors->has('estimatetime') ? ' is-invalid' : '' }}" name="estimatetime" placeholder="Estimate Time" value="{{(!$estimatetime)?old('estimatetime'):$estimatetime}}" required autofocus>

					@if ($errors->has('estimatetime'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('estimatetime') }}</strong>
					</span>
					@endif
				</div>


				<div class="form-group has-feedback">
					<span class="glyphicon glyphicon-time form-control-feedback"></span>
					<input id="courseduration" type="text" class="form-control{{ $errors->has('courseduration') ? ' is-invalid' : '' }}" name="courseduration" placeholder="Course Duration in days" value="{{(!$courseduration)?old('courseduration'):$courseduration}}" required>

					@if ($errors->has('courseduration'))
					<span class="invalid-feedback" role="alert">
						<strong>{{ $errors->first('courseduration') }}</strong>
					</span>
					@endif
				</div>


				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">
							@if(!$id)
								{{ __('Add Syllabus') }}
							@else
								{{ __('Update Syllabus') }}
							@endif
						</button>
						<button type="button" class="btn btn-danger btn-block btn-flat" id ="cancel">Cancel</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</body>
@endsection
