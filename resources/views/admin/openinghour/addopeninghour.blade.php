@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="../../index2.html"><b>Add Opening Hours</b></a>
	</div>

	<div class="register-box-body">
		<form action="{{Route('openinghour.add')}}" method="POST" enctype="multipart/form-data" id = "addforms">
			@csrf
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="day" type="text" value="{{(!$day)?old('day'):$day}}" class="form-control{{ $errors->has('day') ? ' is-invalid' : '' }}" name="day" placeholder="Name of the day" required autofocus>

				@if ($errors->has('day'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('day') }}</strong>
				</span>
				@endif
			</div>
			
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="from" type="time" value="{{(!$from)?old('from'):$from}}" class="form-control{{ $errors->has('from') ? ' is-invalid' : '' }}" name="from" placeholder="Name of the from" required autofocus>

				@if ($errors->has('from'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('from') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="to" type="time" value="{{(!$to)?old('to'):$to}}" class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" name="to" placeholder="Name of the to" required autofocus>

				@if ($errors->has('to'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('to') }}</strong>
				</span>
				@endif
			</div>

			<input type="hidden" name="id" value="{{$id}}">
			
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat" style="width: 250px;">
					{{ __('Add Opening Hour') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
