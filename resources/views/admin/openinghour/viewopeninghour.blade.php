@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Opening Hours</h3>
                </div><!-- /.box-header -->
                  <div class="box-tools">
                    @if(count($openinghour) == 6)
                      @can('openinghours.create',Auth::user())
                    <div class="input-group" title="Sorry! You can add only 4 openinghours">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;" disabled>
                           {{ __('Add Opening Hour') }}
                        </button>
                      </div>
                      @endcan
                    @else
                    @can('openinghours.create',Auth::user())
                    <div class="input-group">
                      <a href="{{Route('openinghour.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add openinghour') }}
                        </button></a>
                      </div>
                      @endcan
                    @endif
                    </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Day</th>
                      <th>Day</th>
                      <th>To</th>
                      @can('openinghours.update',Auth::user())
                          <th>Update</th>
                      @endcan
                      @can('openinghours.delete',Auth::user())
                        <th>Delete</th>
                      @endcan
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                      @foreach($openinghour as $openinghours) 
                            <td>{{$openinghours->id}}</td>
                            <td>{{$openinghours->day}}</td>
                            <td>{{$openinghours->from}}</td>
                            <td>{{$openinghours->to}}</td>
                            @can('openinghours.update',Auth::user())
                            <td>
                              <a href="/openinghour/update/{{$openinghours->id}}">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan    
                            @can('openinghours.delete',Auth::user())
                            <td>
                             <a href="#">
                              <i class="fa fa-trash-o" onclick="myFunction('{{$openinghours->id}}')"></i>
                            </a>
                          </td>   
                            @endcan      
                    </tr>
                      @endforeach
                      </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to delete the openinghour?");
    if (r == true) {
         window.location.href = "/openinghour/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection
