@extends('includes.header1')
@section('content-wrapper')
<div class="row">
<div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Teacher's List</h3>
                </div><!-- /.box-header -->
                  <div class="btn-group">
                      <a href="{{Route('generatepdf')}}"><button name="generatepdf" class="btn btn-block btn-success btn-md">Generate PDF</button></a>

                    @can('teachers.create',Auth::user())
                      <a href="{{Route('teacher.form')}}">
                      <button type="submit" class="btn btn-block btn-success btn-md" style="width: 150px;">
                           {{ __('Add Teacher') }}
                        </button></a>

                      @endcan
                  </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Phone</th>
                      @can('teachers.update',Auth::user())<th>Edit</th>@endcan
                      @can('teachers.delete',Auth::user())<th>Status</th>@endcan
                    </tr>
                    </thead>

                    <tbody><tr>
                    	@foreach($user as $users) 
                    		@foreach ($teacher as $teachers) 
                    			@if($users->id == $teachers->user_id)
                    				<td>{{$teachers->id}}</td>
                    				<td>{{$users->name}}</td>
                            <td>{{$users->email}}</td>
                            <td>{{$users->address}}</td>
                    				<td>{{$users->phone}}</td>
                            @can('teachers.update',Auth::user())
                            <td style="width:26px">
                              <a href="/teacher/update/{{$teachers->id}}" title="Click to update">
                                <i class="fa fa-edit"></i>
                              </a>
                            </td>
                            @endcan

                            @can('teachers.delete',Auth::user())
                            <td>
                              @if($teachers->status== 'Active')
                              <div title="Click the button to deactivate teacher">
                              <button class="btn btn-block btn-success btn-md" type="delete" onclick="myFunction('{{$teachers->id}}')" style="width: 100px">
                                  {{$teachers->status}}
                              </button>
                              </div>
                              @else
                                <div title="Click the button to activate teacher">
                              <button class="btn btn-block btn-warning btn-md" type="delete" onclick="myFunction('{{$teachers->id}}')" style="width: 100px">
                                  {{$teachers->status}}
                              </button>
                              </div>
                              @endif
                             
                            </td>
                            @endcan
                    			@endif
                    		@endforeach
                    </tr>
                    	@endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div>
</div>
<script>
function myFunction(id) {
    var txt;
    var r = confirm("Are you sure you want to change the status?");
    if (r == true) {
         window.location.href = "/teacher/delete/"+id;
    } else {
        txt = "You pressed Cancel!";
    }
}
</script>
@endsection