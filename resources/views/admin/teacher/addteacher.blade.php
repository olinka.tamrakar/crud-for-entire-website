@extends('includes.header1')
@section('content-wrapper')
<body class="register-page"> 
<div class="register-box">
	<div class="register-logo">
		<a href="/teacher/form"><b>Add Teacher</b></a>
	</div>

	<div class="register-box-body">
		<form action="/teacher/add" method="POST" enctype="multipart/form-data" id="addforms">
			@csrf
			<b>Personal Information</b>
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Full name" value="{{(!$name)?old('name'):$name}}" required autofocus>

				@if ($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-star form-control-feedback"></span>
				<input id="specialization" type="text" class="form-control{{ $errors->has('specialization') ? ' is-invalid' : '' }}" name="specialization" placeholder="Specialization" value="{{(!$specialization)?old('specialization'):$specialization}}" required>

				@if ($errors->has('specialization'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('specialization') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Describe about the teacher">
				<span class="glyphicon glyphicon-pencil form-control-feedback"></span>
				<textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Describe the event" rows="4" cols="50" required autofocus>{{$description}}
				</textarea>

				@if ($errors->has('description'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('description') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password"  required>

				@if ($errors->has('password'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('password') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback" title="Upload teacher's photo">
				<input type="file" name="file" id="file" required autofocus>
				@if ($errors->has('file'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('file') }}</strong>
				</span>
				@endif
			</div>
	
			<b>Contact Information</b>
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-home form-control-feedback"></span>
				<input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Full Address" value="{{(!$address)?old('address'):$address}}" required autofocus>

				@if ($errors->has('address'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('address') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-phone form-control-feedback"></span>
				<input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Phone" value="{{(!$phone)?old('phone'):$phone}}" required autofocus>

				@if ($errors->has('phone'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('phone') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{(!$email)?old('email'):$email}}" required>

				@if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span>
				@endif
			</div>
			
			<b>Experiences</b>
			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-thumbs-up form-control-feedback"></span>
				<input id="experiences" type="text" class="form-control{{ $errors->has('experiences') ? ' is-invalid' : '' }}" name="experiences" placeholder="Mention teacher's experiences" value="{{(!$experiences)?old('experiences'):$experiences}}" required>

				@if ($errors->has('experiences'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('experiences') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-book form-control-feedback"></span>
				<input id="subjects" type="text" class="form-control{{ $errors->has('subjects') ? ' is-invalid' : '' }}" name="subjects" placeholder="Subjects in number" value="{{(!$subjects)?old('subjects'):$subjects}}" required autofocus>

				@if ($errors->has('subjects'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('subjects') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
				<input id="followers" type="text" class="form-control{{ $errors->has('followers') ? ' is-invalid' : '' }}" name="followers" placeholder="Followers in number" value="{{(!$followers)?old('followers'):$followers}}" required autofocus>

				@if ($errors->has('followers'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('followers') }}</strong>
				</span>
				@endif
			</div>


			<div class="form-group has-feedback">
				<span class="glyphicon glyphicon-check form-control-feedback"></span>
				<input id="classes" type="text" class="form-control{{ $errors->has('classes') ? ' is-invalid' : '' }}" name="classes" placeholder="Classes in number" value="{{(!$classes)?old('classes'):$classes}}" required autofocus>

				@if ($errors->has('classes'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('classes') }}</strong>
				</span>
				@endif
			</div>

			<div class="form-group has-feedback">
				Select the courses <br>
					@foreach($courses as $course)
						<input id="courses" type="checkbox" 
							value="{{$course->id}}"  
							class="form-control{{ $errors->has('courses') ? ' is-invalid' : '' }}" name="courses[]" 
						required autofocus> {{$course->name}}
					@endforeach
				
				@if ($errors->has('courses'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('courses') }}</strong>
				</span>
				@endif
			</div>
			
			<input type="hidden" name="id" value="{{$id}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<input type="hidden" name="role" value="Teacher">
			<input type="hidden" name="status" value="Active">
			<div class="row">
			<div class="col-xs-4">
				<button type="submit" class="btn btn-primary btn-block btn-flat">
					{{ __('Register') }}
				</button>

				<button onclick ="goback()" class="btn btn-primary btn-block btn-flat">
					{{ __('Cancel') }}
				</button>
			</div>
		</div>
		</form>
	</div>	
</div>		
</body>
@endsection
