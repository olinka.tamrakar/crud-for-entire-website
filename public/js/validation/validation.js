$(function()
{
	$.validator.setDefaults({
		errorClass: 'help-block',
		highlight: function(element) {
			$(element)
			.closest('.form-group')
			.addClass('has-error');
		},
		unhighlight: function(element) {
			$(element)
			.closest('.form-group')
			.removeClass('has-error');
		},
	});
	$.validator.addMethod('strongPassword',function(value,element)
	{
		return this.optional(element) 
      || value.length >= 6
      && /\d/.test(value)
      && /[a-z]/i.test(value);
	},'Your password must be at least 6 characters long and contain at least one number and one char')

	$.validator.addMethod('phone', function(value, element) 
	{
		return this.optional(element) 
		   ||/^(9841)([0-9]{6})$/.test(value)
           ||/^(9803)([0-9]{6})$/.test(value)
           ||/^(9848)([0-9]{6})$/.test(value)
           ||/^(9849)([0-9]{6})$/.test(value)
           ||/^(98510)([0-9]{5})$/.test(value)
           ||/^(98510)([0-9]{5})$/.test(value)
           ||/^(98510)([0-9]{5})$/.test(value)
           ||/^(98010)([0-9]{5})$/.test(value)
           && value.length == 10;
	},'Enter Valid  phone number');

	$("#addforms").validate(
	{
		rules: {
			title:
			{
				required: true,
			},

			category:
			{
				required:true,
			},

			description:
			{
				required:true,
				
			},
			
			price:
			{
				required:true,
				digits:true,
				nowhitespace: true,
			},

			name: 
			{
				required: true,
			},

			features:
			{
				required:true,
			},

			classtime:{
				required:true,
			},

			courseduration:{
				required:true,
				digits:true,
			},

			'course[]':{
				required:true,
				minlength:2,
			},

			estimatetime:{
				required:true,
			},

			syllabusname:{
				required:true,
			},

			email:{
				required:true,
				email:true,
				nowhitespace: true,
			},

			topics:{
				required:true,	
			},

			host:{
				required:true,
				lettersonly: true
			},

			location:{
				required:true,
			},

			starttime:{
				required:true,
			},

			startdate:{
				required:true,
			},

			enddate:{
				required:true,
			},

			website:{
				required:true,
				nowhitespace: true,
				url:true,
			},

			keynotespeakers:{
				required:true,
			},

			specialization:{
				required:true,
			},

			experiences:{
				required:true,
			},

			subjects:{
				nowhitespace: true,
				required:true,
				digits:true,
			},

			followers:{
				required:true,
				nowhitespace: true,
				digits:true,
			},

			classes:{
				required:true,
				nowhitespace: true,
				digits:true,
			},

			password: {
				nowhitespace: true,
				required: true,
				strongPassword: true,
			},

			oldpassword:{
				nowhitespace: true,
				required:true,
			},

			newpassword:{
				nowhitespace: true,
				required:true,
				strongPassword: true,
			},

			confirmpassword:{
				nowhitespace: true,
				required:true,
				equalTo:'#newpassword',
			},

			phone: {		
				nowhitespace: true,
				digits: true,
				phone: true,
			},
			number: {		
				nowhitespace: true,
				digits: true,
				phone: true,
			},
			videocall: {		
				nowhitespace: true,
				digits: true,
			},

			address: {
				required: true,						
			},

			file:{
				required:true,
			}
		},

		messages: 
		{
      		email: 
      		{
        		required: 'Please enter an email address.',
        		email: 'Please enter a <em>valid</em> email address.',//Validation failed 
      		}

    	}
	});
});
$('#cancel').click(function() {
   if(confirm("Are you sure you want to cancel the submission?"))
   {
       history.go(-1);
   }
   return false;
});