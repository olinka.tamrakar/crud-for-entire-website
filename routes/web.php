<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Notifications\Verifyuser;

Route::get('add-to-log', 'HomeController@myTestAddToLog');
Route::get('/logActivity', 'HomeController@logActivity')->name('logactivity');

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('goback',function()
{
	return redirect()->back();
})->name('back');

Route::get('/back',function()
{
	return view ('admin.back');
});

Route::any('/welcome',function()
{
	return view('welcome');
});

//Gates for the users
Route::get('adminonly',function()
{
	if (Gate::allows('admin-only', Auth::user())) 
	{
		return view('header1');
	}
	else
	{
		return false;
	}
});

Route::get('teacheronly',function()
{
	if (Gate::allows('teacheronly', Auth::user())) 
	{
		return view('header1');
	}
	else
	{
		return false;
	}
});

Route::get('instituteonly',function()
{
	if (Gate::allows('institute-only', Auth::user())) 
	{
		return view('header1');
	}
	else
	{
		return false;
	}
});

Route::get('editoronly',function()
{
	if (Gate::allows('editor-only', Auth::user())) 
	{
		return view('header1');
	}
	else
	{
		return false;
	}
});


//For header and footer part
Route::get('/header',function()
{
	return view('includes.header1');
});



Auth::routes();

Route::get('/profile',function()
{
	return view('profile');
})->name('profile');

Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'namespace' => 'Web' ], function () 
{
	//Dashboard
	Route::get('/album/{id}','GalleryCategoryController@display')->name('album.display');

	//Dashboard
	Route::get('/','DashboardController@index');
	Route::get('/teacherdashboard','DashboardController@teacher')->middleware('teacher');
	Route::get('/institutedashboard','DashboardController@institute')->middleware('institute');
	Route::get('/editordashboard','DashboardController@editor')->middleware('editor');
	// Route::get('/studentdashboard','DashboardController@student')->middleware('student');

	//Opening Hour
	Route::group(['prefix' => 'openinghour'],function()
	{
		Route::get('/form','OpeninghourController@add')->name('openinghour.form');
		Route::post('/add','OpeninghourController@add')->name('openinghour.add');
		Route::get('/view','OpeninghourController@view')->name('openinghour.view');
		Route::get('/update/{id}','OpeninghourController@update')->name('openinghour.update');
		Route::get('/delete/{id}','OpeninghourController@delete')->name('openinghour.delete');
	});

	//Index Page
	Route::group(['prefix' => 'indexpage'],function()
	{
		Route::get('/form','IndexpageController@add')->name('indexpage.form');
		Route::post('/add','IndexpageController@add')->name('indexpage.add');
		Route::get('/view','IndexpageController@view')->name('indexpage.view');
		Route::get('/update/{id}','IndexpageController@update')->name('indexpage.update');
	});

	//Links
	Route::group(['prefix' => 'link'],function()
	{
		Route::get('/form','LinkController@add')->name('link.form');
		Route::post('/add','LinkController@add')->name('link.add');
		Route::get('/view','LinkController@view')->name('link.view');
		Route::get('/update/{id}','LinkController@update')->name('link.update');
		Route::get('/delete/{id}','LinkController@delete')->name('link.delete');
	});

	//Features
	Route::group(['prefix' => 'feature'],function()
	{
		Route::get('/form','FeatureController@add')->name('feature.form');
		Route::post('/add','FeatureController@add')->name('feature.add');
		Route::get('/view','FeatureController@view')->name('feature.view');
		Route::get('/update/{id}','FeatureController@update')->name('feature.update');
		Route::get('/delete/{id}','FeatureController@delete')->name('feature.delete');
	});

	//Why Choose Us
	Route::group(['prefix' => 'whychooseus'],function()
	{
		Route::get('/form','WhychooseusController@add')->name('whychooseus.form');
		Route::post('/add','WhychooseusController@add')->name('whychooseus.add');
		Route::get('/view','WhychooseusController@view')->name('whychooseus.view');
		Route::get('/update/{id}','WhychooseusController@update')->name('whychooseus.update');
		Route::get('/delete/{id}','WhychooseusController@delete')->name('whychooseus.delete');
	});

	//Our Team
	Route::group(['prefix' => 'ourteam'],function()
	{
		Route::get('/form','OurteamController@add')->name('ourteam.form');
		Route::post('/add','OurteamController@add')->name('ourteam.add');
		Route::get('/view','OurteamController@view')->name('ourteam.view');
		Route::get('/update/{id}','OurteamController@update')->name('ourteam.update');
		Route::get('/delete/{id}','OurteamController@delete')->name('ourteam.delete');
	});
	
	//Verify Email
	Route::get('verify/{token}','VerificationController@verify')->name('verify');
	Route::post('/logactivity','FilterController@filter')->name('filter');

	//For Admin
	Route::get('/admin','AdminController@index');
	Route::post('adminlogin','AdminController@check');

	//Users
	Route::group(['prefix' => 'user'], function () 
	{
		Route::get('/view','UserController@index')->name('user.view');
	});

	//FAQs
	Route::group(['prefix' => 'faq'], function () 
	{
		Route::get('/view','FaqController@view')->name('faq.view');
		Route::get('form','FaqController@add')->name('faq.form');
		Route::post('/add','FaqController@add')->name('faq.add');
		Route::any('/update/{id}','FaqController@update')->name('faq.update');
		Route::get('/delete/{id}','FaqController@delete')->name('faq.delete');
	});

	//Roles
	Route::group(['prefix' => 'role'], function () 
	{
		Route::get('/view','RoleController@index')->name('role.view');
		Route::get('form','RoleController@create')->name('role.form');
		Route::post('/add','RoleController@create')->name('role.add');
		Route::any('/update/{id}','RoleController@update')->name('role.update');
		Route::get('/delete/{id}','RoleController@delete')->name('role.delete');
	});

	//Permissions
	Route::group(['prefix' => 'permission'], function () 
	{
		Route::get('/view','PermissionController@index')->name('permission.view');
		Route::get('form','PermissionController@create')->name('permission.form');
		Route::post('/add','PermissionController@create')->name('permission.add');
		Route::any('/update/{id}','PermissionController@update')->name('permission.update');
		Route::get('/delete/{id}','PermissionController@delete')->name('permission.delete');
	});

	//Teacher CRUD
	Route::group(['prefix' => 'teacher'], function () 
	{
		Route::get('form','TeacherController@add')->name('teacher.form');
		Route::post('/add','TeacherController@add')->name('teacher.add');
		Route::get('/view','TeacherController@show')->name('teacher.view');
		Route::any('/update/{id}','TeacherController@update')->name('teacher.update');
		Route::get('/delete/{id}','TeacherController@delete')->name('teacher.delete');
	});

	//Editor CRUD
	Route::group(['prefix' => 'editor'], function () 
	{
		Route::get('form','EditorController@add')->name('editor.form');
		Route::post('/add','EditorController@add')->name('editor.add');
		Route::get('/view','EditorController@view')->name('editor.view');
		Route::any('/update/{id}','EditorController@update')->name('editor.update');
		Route::get('/delete/{id}','EditorController@delete')->name('editor.delete');
	});

	//Course CRUD
	Route::group(['prefix' => 'course'], function () 
	{
		Route::get('/form','CourseController@add')->name('course.form');
		Route::post('/add','CourseController@add')->name('course.add');
		Route::post('/assigncourse','CourseController@assigncourse')->name('course.assigncourses');
		Route::any('/update/{id}','CourseController@update')->name('course.update');
		Route::get('/delete/{id}','CourseController@delete')->name('course.delete');
		Route::get('/view','CourseController@view')->name('course.view');
	});

	//Syllabus CRUD
	Route::group(['prefix' => 'syllabus'], function () 
	{
		Route::get('/form/{id}','SyllabusController@add')->name('syllabus.form');
		Route::post('/add/{id}','SyllabusController@add')->name('syllabus.add');
		Route::any('/update/{id}','SyllabusController@update')->name('syllabus.update');
		Route::get('/delete/{id}','SyllabusController@delete')->name('syllabus.delete');
		Route::get('/view/{id}','SyllabusController@view')->name('syllabus.view');
	});

	//Section CRUD
	Route::group(['prefix' => 'section'], function () 
	{
		Route::get('/form/{id}','SectionController@add')->name('section.form');
		Route::post('/add/{id}','SectionController@add')->name('section.add');
		Route::any('/update/{id}','SectionController@update')->name('section.update');
		Route::get('/delete/{id}','SectionController@delete')->name('section.delete');
		Route::get('/view/{id}','SectionController@view')->name('section.view');
	});

	//Event CRUD
	Route::group(['prefix' => 'event'], function () 
	{
		Route::get('/form','EventController@add')->name('event.form');
		Route::post('/add','EventController@add')->name('event.add');
		Route::get('/view','EventController@view')->name('event.view');
		Route::any('/update/{id}','EventController@update')->name('event.update');
		Route::get('/delete/{id}','EventController@delete')->name('event.delete');
	});

	//Blog CRUD
	Route::group(['prefix' => 'blog'], function () 
	{
		Route::get('/form','BlogController@add')->name('blog.form');
		Route::post('/add','BlogController@add')->name('blog.add');
		Route::get('/view','BlogController@view')->name('blog.view');
		Route::any('/update/{id}','BlogController@update')->name('blog.update');
		Route::get('/delete/{id}','BlogController@delete')->name('blog.');
	});

	//Pageimage CRUD
	Route::group(['prefix' => 'pageimage'], function () 
	{
		Route::get('/form','PageimageController@add')->name('pageimage.form');
		Route::post('/add','PageimageController@add')->name('pageimage.add');
		Route::get('/view','PageimageController@view')->name('pageimage.view');
		Route::any('/update/{id}','PageimageController@update')->name('pageimage.update');
		Route::get('/delete/{id}','PageimageController@delete')->name('pageimage.');
	});

	//Slider CRUD
	Route::group(['prefix' => 'slider'], function () 
	{
		Route::get('/form','SliderController@add')->name('slider.form');
		Route::post('/add','SliderController@add')->name('slider.add');
		Route::get('/view','SliderController@view')->name('slider.view');
		Route::get('/update/{id}','SliderController@update')->name('slider.update');
		Route::get('/delete/{id}','SliderController@delete')->name('slider.delete');
	});

	//Gallery CRUD
	Route::group(['prefix' => 'gallery'], function () 
	{
		Route::get('/form','GalleryController@add')->name('gallery.form');
		Route::post('/add','GalleryController@add')->name('gallery.add');
		Route::get('/view','GalleryController@view')->name('gallery.view');
		Route::get('/update/{id}','GalleryController@update')->name('gallery.update');
		Route::get('/delete/{id}','GalleryController@delete')->name('gallery.delete');
	});

	//Gallery Category CRUD
	Route::group(['prefix' => 'gallerycategory'], function () 
	{
		Route::get('/form','GallerycategoryController@add')->name('gallerycategory.form');
		Route::post('/add','GallerycategoryController@add')->name('gallerycategory.add');
		Route::get('/view','GallerycategoryController@view')->name('gallerycategory.view');
		Route::get('/update/{id}','GallerycategoryController@update')->name('gallerycategory.update');
		Route::get('/delete/{id}','GallerycategoryController@delete')->name('gallerycategory.delete');
	});

	//Coursecategory CRUD
	Route::group(['prefix' => 'coursecategory'], function () 
	{
		Route::get('/form','CoursecategoryController@add')->name('coursecategory.form');
		Route::post('/add','CoursecategoryController@add')->name('coursecategory.add');
		Route::get('/view','CoursecategoryController@view')->name('coursecategory.view');
		Route::get('/update/{id}','CoursecategoryController@update')->name('coursecategory.update');
		Route::get('/delete/{id}','CoursecategoryController@delete')->name('coursecategory.delete');
	});

	//Blogcategory CRUD
	Route::group(['prefix' => 'blogcategory'], function () 
	{
		Route::get('/form','BlogcategoryController@add')->name('blogcategory.form');
		Route::post('/add','BlogcategoryController@add')->name('blogcategory.add');
		Route::get('/view','BlogcategoryController@view')->name('blogcategory.view');
		Route::get('/update/{id}','BlogcategoryController@update')->name('blogcategory.update');
		Route::get('/delete/{id}','BlogcategoryController@delete')->name('blogcategory.delete');
	});

	//Contact CRUD
	Route::group(['prefix' => 'contact'], function () 
	{
		Route::get('/form','ContactController@add')->name('contact.form');
		Route::post('/add','ContactController@add')->name('contact.add');
		Route::get('/view','ContactController@view')->name('contact.view');
		Route::get('/update/{id}','ContactController@update')->name('contact.update');
		Route::get('/delete/{id}','ContactController@delete')->name('contact.delete');
	});

	//Mission CRUD
	Route::group(['prefix' => 'mission'], function () 
	{
		Route::get('/form','MissionController@add')->name('mission.form');
		Route::post('/add','MissionController@add')->name('mission.add');
		Route::get('/view','MissionController@view')->name('mission.view');
		Route::get('/update/{id}','MissionController@update')->name('mission.update');
		Route::get('/delete/{id}','MissionController@delete')->name('mission.delete');
	});

	//Whyus CRUD
	Route::group(['prefix' => 'whyus'], function () 
	{
		Route::get('/form','WhyusController@add')->name('whyus.form');
		Route::post('/add','WhyusController@add')->name('whyus.add');
		Route::get('/view','WhyusController@view')->name('whyus.view');
		Route::get('/update/{id}','WhyusController@update')->name('whyus.update');
		Route::get('/delete/{id}','WhyusController@delete')->name('whyus.delete');
	});

	//Workflow CRUD
	Route::group(['prefix' => 'workflow'], function () 
	{
		Route::get('/form','WorkflowController@add')->name('workflow.form');
		Route::post('/add','WorkflowController@add')->name('workflow.add');
		Route::get('/view','WorkflowController@view')->name('workflow.view');
		Route::get('/update/{id}','WorkflowController@update')->name('workflow.update');
		Route::get('/delete/{id}','WorkflowController@delete')->name('workflow.delete');
	});

	//Institute CRUD
	Route::group(['prefix' => 'institute'], function () 
	{
		Route::get('/form','InstituteController@add')->name('institute.form');
		Route::post('/add','InstituteController@add')->name('institute.add');
		Route::get('/view','InstituteController@view')->name('institute.view');
		Route::get('/update/{id}','InstituteController@update')->name('institute.update');
		Route::get('/delete/{id}','InstituteController@delete')->name('institute.delete');

		Route::get('/assigncourse','InstituteController@assigncourse')->name('getassigncourse');
		Route::post('/assigncourse','InstituteController@assigncourse');
		Route::get('/viewcourse','InstituteController@viewcourse')->name('viewcourse');

		Route::get('/assignteacher','InstituteController@assignteacher')->name('getassignteacher');
		Route::post('/assignteacher','InstituteController@assignteacher');
		Route::get('/viewteacher','InstituteController@viewteacher')->name('viewteacher');
	});


	//Settings 
	Route::group(['prefix' => 'settings'],function()
	{

		Route::get('/name','SettingsController@name');
		Route::post('/name','SettingsController@name');	

		Route::get('/password','SettingsController@password');
		Route::post('/password','SettingsController@password');

		Route::get('/emails','SettingsController@emails');
		Route::post('/emails','SettingsController@emails');

		Route::get('/address','SettingsController@address');
		Route::post('/address','SettingsController@address');

		Route::get('/phone','SettingsController@phone');
		Route::post('/phone','SettingsController@phone');		
	});

	//All About
	Route::group(['prefix' => 'allaboutorg'],function()
	{
		Route::get('/form','AllaboutorgController@add')->name('allaboutorg.form');
		Route::post('/add','AllaboutorgController@add')->name('allaboutorg.add');
		Route::get('/view','AllaboutorgController@view')->name('allaboutorg.view');
		Route::get('/update/{id}','AllaboutorgController@update')->name('allaboutorg.update');
		Route::get('/delete/{id}','AllaboutorgController@delete')->name('allaboutorg.delete');
	});


	//For PDF
	Route::get('/getdata','Pdfcontroller@get_data');
	Route::get('/generatepdf','Pdfcontroller@generate')->name('generatepdf');



	//For Frontend
	Route::group([ 'namespace' => 'Frontend' ], function () 
	{
		//Teacher CRUD
		Route::group(['prefix' => 'student'], function () 
		{
			Route::any('register','Studentcontroller@register')->name('student.register');
			Route::any('login','Studentcontroller@login')->name('student.login');
			Route::post('enroll','Studentcontroller@enroll')->name('student.enroll');
			// Route::post('/add','Studentcontroller@add')->name('student.add');
			// Route::get('/view','Studentcontroller@show')->name('student.view');
			// Route::any('/update/{id}','Studentcontroller@update')->name('student.update');
			// Route::get('/delete/{id}','Studentcontroller@delete')->name('student.delete');
		});

		//For Views
		Route::get('/about','ViewController@about')->name('frontend.about');
		Route::get('/blog','ViewController@blogs')->name('frontend.blogs');
		Route::get('/blogs-detail/{id}','ViewController@blogdetail')->name('frontend.blogs-detail');
		Route::get('/contact','ViewController@contact')->name('frontend.contact');
		Route::get('/message','MessageController@view')->name('message.view');

		Route::get('/course-detail/{id}','ViewController@coursedetail')->name('frontend.course-detail');
		Route::get('/course','ViewController@course')->name('frontend.course');
		Route::get('/event-detail/{id}','ViewController@eventdetail')->name('frontend.event-detail');
		Route::get('/events','ViewController@events')->name('frontend.events');
		Route::get('/faq','ViewController@faq')->name('frontend.faq');
		Route::get('/gallery-detail/{id}','ViewController@gallerydetail')->name('frontend.gallery-detail');
		Route::get('/gallery','ViewController@gallery')->name('frontend.gallery');
		Route::get('/index','ViewController@index')->name('frontend.index');
		Route::get('/teachers-detail','ViewController@teachersdetail')->name('frontend.teachers-detail');
		Route::get('/teachers','ViewController@teachers')->name('frontend.teachers');
		Route::get('/team','ViewController@team')->name('frontend.team');

		Route::get('/register','ViewController@register')->name('frontend.register');

		//For Message
		Route::post('/message','MessageController@send')->name('frontend.message');

		//For Search
		Route::post('course/search','ViewController@course')->name('course.search');
		Route::post('teacher/search','ViewController@teachers')->name('teacher.search');
		Route::post('event/search','ViewController@events')->name('event.search');
		Route::post('blog/search','ViewController@blogs')->name('blog.search');
		Route::get('category/search/{id}','ViewController@course')->name('category.search');
	});
});

// Route::get('/index','function()
// {
// 	return view('frontend.index');
// }');










